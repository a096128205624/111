package com.example.a1006;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class updatepassword extends AppCompatActivity {
    EditText oldpass,newpass;
    TextView tsend;
    ArrayList<forgetpasswordvalue> allaccount=new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updatepassword);
        Bundle bundle = getIntent().getExtras();
        oldpass=findViewById(R.id.oldpassword);
        newpass=findViewById(R.id.newpasswrod);
        tsend=findViewById(R.id.bsend);
        Global global=(Global)this.getApplicationContext();
        String account=global.accountname;
        String password=global.password;
        Mysqlqrcode mysqlqrcode=new Mysqlqrcode();
        tsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println(oldpass.getText().toString()+"/"+password);
                if(!newpass.getText().toString().equals("")){
                    if(password.equals(oldpass.getText().toString())){
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                mysqlqrcode.updatePassword(account,newpass.getText().toString());
                                global.setPass(newpass.getText().toString());
                                Looper.prepare();;
                                Toast.makeText(updatepassword.this, "密碼已修改", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(updatepassword.this, account.class);
                                startActivity(intent);
                                Looper.loop();
                            }
                        }).start();
                    }
                    else{
                        Toast.makeText(updatepassword.this, "舊密碼錯誤", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(updatepassword.this, "密碼不可為空的", Toast.LENGTH_SHORT).show();
                }

            }
        });



    }

}