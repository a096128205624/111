package com.example.a1006;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class calendar extends AppCompatActivity {
    ArrayList<historydata> historydataArrayList=new ArrayList<historydata>();

    ListView listViewhistroy;
    boolean end=false;
    Calendar c = Calendar.getInstance();
    int nyear = c.get(Calendar.YEAR);
    int nmonth = c.get(Calendar.MONTH)+1;
    int nday = c.get(Calendar.DAY_OF_MONTH);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendarlayout);
        List<EventDay> events = new ArrayList<>();
        Global globalaccountname=(Global)getApplicationContext();
        String accountname=globalaccountname.getAccountname();
        Calendar c=Calendar.getInstance();
        ArrayList<historydata> historydatabuffer=new ArrayList<historydata>();
        listViewhistroy=findViewById(R.id.history);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Mysqlqrcode con = new Mysqlqrcode();
                historydataArrayList= con.gethistoryData(accountname);
                for(int i=0;i<historydataArrayList.size();i++){
                    String nowday[]=historydataArrayList.get(i).date.split("\\/");
                    if(historydataArrayList.get(i).type.equals("expend")){
                        c.set(Integer.valueOf(nowday[0]),Integer.valueOf(nowday[1]),Integer.valueOf(nowday[2]));

                        events.add(new EventDay(c, R.drawable.button_pay_background));
                    }
                    else if(historydataArrayList.get(i).type.equals("income")){
                        c.set(Integer.valueOf(nowday[0]),Integer.valueOf(nowday[1]),Integer.valueOf(nowday[2]));
                        System.out.println(nowday[0]+nowday[1]+nowday[2]);
                        events.add(new EventDay(c, R.drawable.button_income_background));
                    }
                    if(nowday[0].equals(String.valueOf(nyear)) && nowday[1].equals(String.valueOf(nmonth))&& nowday[2].equals(String.valueOf(nday))){
                        historydatabuffer.add(historydataArrayList.get(i));
                    }
                }
                end=true;
                Collections.sort(historydatabuffer, new historysort());
            }
        }).start();

        while (end!=true){
            BaseAdapter adapter=new BaseAdapter() {
                @Override
                public int getCount() {
                    return historydatabuffer.size();
                }

                @Override
                public Object getItem(int i) {
                    return i;
                }

                @Override
                public long getItemId(int i) {
                    return i;
                }

                @Override
                public View getView(int i, View view, ViewGroup viewGroup) {
                    View layout=View.inflate(calendar.this,R.layout.expenddatalayout,null);
                    TextView subcategory=layout.findViewById(R.id.subcategory);
                    TextView money=layout.findViewById(R.id.money);
                    subcategory.setText(historydatabuffer.get(i).maincategory);
                    if(historydatabuffer.get(i).type.equals("expend")){
                        money.setText(" - "+String.valueOf(historydatabuffer.get(i).money)+" $");
                        money.setTextColor(Color.RED);
                    }
                    else if(historydatabuffer.get(i).type.equals("income")){
                        money.setText(" + "+String.valueOf(historydatabuffer.get(i).money)+" $");
                        money.setTextColor(Color.GREEN);
                    }
                    return layout;

                }
            };
            listViewhistroy.setAdapter(adapter);
        }
        CalendarView calendarView = (CalendarView) findViewById(R.id.calendarView);
        calendarView.setEvents(events);
        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                historydatabuffer.clear();
                Calendar clickedDayCalendar = eventDay.getCalendar();
                nyear=clickedDayCalendar.get(Calendar.YEAR);
                nmonth=clickedDayCalendar.get(Calendar.MONTH)+1;
                nday=clickedDayCalendar.get(Calendar.DATE);
                for(int i=0;i<historydataArrayList.size();i++){
                    String nowday[]=historydataArrayList.get(i).date.split("\\/");
                    System.out.println(nowday[0]+nowday[1]+nowday[2]);
                    if(nowday[0].equals(String.valueOf(nyear)) && nowday[1].equals(String.valueOf(nmonth))&& nowday[2].equals(String.valueOf(nday))){
                        historydatabuffer.add(historydataArrayList.get(i));
                    }
                }
                Collections.sort(historydatabuffer, new historysort());
                BaseAdapter adapter=new BaseAdapter() {
                    @Override
                    public int getCount() {
                        return historydatabuffer.size();
                    }
                    @Override
                    public Object getItem(int i) {
                        return i;
                    }@Override

                    public long getItemId(int i) {
                        return i;
                    }
                    @Override
                    public View getView(int i, View view, ViewGroup viewGroup) {
                        View layout=View.inflate(calendar.this,R.layout.expenddatalayout,null);
                        TextView subcategory=layout.findViewById(R.id.subcategory);
                        TextView money=layout.findViewById(R.id.money);
                        subcategory.setText(historydatabuffer.get(i).maincategory);
                        if(historydatabuffer.get(i).type.equals("expend")){
                            money.setText(" - "+String.valueOf(historydatabuffer.get(i).money)+" $");
                            money.setTextColor(Color.RED);
                        }
                        else if(historydatabuffer.get(i).type.equals("income")){
                            money.setText(" + "+String.valueOf(historydatabuffer.get(i).money)+" $");
                            money.setTextColor(Color.BLUE);
                        }
                        return layout;

                    }
                };
                listViewhistroy.setAdapter(adapter);
            }
        });


    }
}