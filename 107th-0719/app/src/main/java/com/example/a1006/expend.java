package com.example.a1006;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.provider.MediaStore;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link expend#newInstance} factory method to
 * create an instance of this fragment.
 */
public class expend extends Fragment{

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public expend() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment tab1.
     */
    // TODO: Rename and change types and number of parameters
    public static expend newInstance(String param1, String param2) {
        expend fragment = new expend();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }
    public static final String TAG = expend.class.getSimpleName()+"My";

    private String mPath = "";//設置高畫質的照片位址
    public static final int CAMERA_PERMISSION = 100;//檢測相機權限用
    public static final int REQUEST_HIGH_IMAGE = 101;//檢測高畫質相機回傳
    public static final int REQUEST_SELECT = 102;//檢測低畫質相機回傳
    TextView tdate,ttime;
    private  Spinner spinner;
    EditText code,money,detail,codee;
    String money1,detail1,time;
    Button enter,add,delete;
    String Maincategory,Subcategory;
    public int pos;
    private  Spinner spinner1;
    private String[] lunch = {"行車交通", "休閒娛樂", "醫療保險", "食品酒水", "居家繳費","購物","教育","家具","其他"};
    private String[] lunch1 = {"公車", "高鐵", "火車", "捷運","計程車/Uber","機票","船票"};
    private String[][] lunch2 = {{"公車", "高鐵", "火車", "捷運","計程車/Uber","機票","船票"},{"無"},{"無"},{"無"},{"水費", "電費", "電話費", "瓦斯費","房租費"},{"無"},{"無"},{"無"},{"無"}};
    private ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>();
    private ArrayList<String> inSideList = new ArrayList<String>();
    ImageView photo,qrcodebt;
    DatePickerDialog.OnDateSetListener dateSetListener;
    TimePickerDialog.OnTimeSetListener timeSetListener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view1=inflater.inflate(R.layout.addview,container,false);
        View view=inflater.inflate(R.layout.expend, container, false);
        ttime=view.findViewById(R.id.time);
        tdate=view.findViewById(R.id.date);
        spinner = view.findViewById(R.id.spinner);
        spinner1 = view.findViewById(R.id.spinner2);
        enter=view.findViewById(R.id.button4);
        photo=view.findViewById(R.id.photo);
        add=view.findViewById(R.id.button9);
        delete=view.findViewById(R.id.button10);
        qrcodebt=view.findViewById(R.id.qrcodebt);
        Calendar cal=Calendar.getInstance();
        int year=cal.get(Calendar.YEAR);
        int month=cal.get(Calendar.MONTH);
        int day=cal.get(Calendar.DATE);
        int hour=cal.get(Calendar.HOUR_OF_DAY);
        int min=cal.get(Calendar.MINUTE);
        code=view.findViewById(R.id.invoice2);
        codee=view.findViewById(R.id.invoice);
        money=view.findViewById(R.id.money);
        detail=view.findViewById(R.id.detail);
        detail.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        detail.setGravity(Gravity.TOP);

        Global global=(Global)getActivity().getApplicationContext();
        if(global.code!=null){
            String codenumber=global.code.substring(2);
            String codeenglish=global.code.substring(0,2);
            codee.setText(codeenglish);
            code.setText(codenumber);
            money.setText(String.valueOf(global.money));
            detail.setText(global.detail);
        }
        list.add(inSideList);
        for (int i=0;i<lunch2.length;i++){
            for(int j=0;j<lunch2[i].length;j++){
                list.get(i).add(lunch2[i][j]);
            }
            list.add(new ArrayList<>());
        }

        for (int i=0;i<list.size();i++){
            for(int j=0;j<list.get(i).size();j++){
               // System.out.print(i+" "+j);
                //System.out.print(list.get(i).get(j)+" ");
            }
           // System.out.println("");
        }

        codee.setTransformationMethod(new AllCapTransformationMethod(true));
        ArrayAdapter<String> lunchList = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, lunch);
                lunchList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(lunchList);
                spinner.setOnItemSelectedListener(selectListener);
                ArrayAdapter<String> lunch1List = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, lunch1);
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                spinner1.setOnItemSelectedListener(selectListener1);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("新增分類");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newstring = editText.getText().toString();
                        for(int i=0;i<list.get(pos).size();i++){
                           // System.out.println(pos);
                            if(newstring.equals(list.get(pos).get(i))){
                                Toast.makeText(getActivity(),"此類別已存在",Toast.LENGTH_LONG).show();
                                editText.setText("");
                                return;
                            }
                        }
                        if(!newstring.equals("")){
                            list.get(pos).add((newstring));
                            spinner1.setSelection(list.get(pos).size()-1);
                            Toast.makeText(getActivity(),"新增成功",Toast.LENGTH_LONG).show();
                            editText.setText("");
                        }

                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("刪除分類");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        boolean check=true;
                        String newstring = editText.getText().toString();
                        if(newstring.equals("無")){
                            Toast.makeText(getActivity(),"沒有類別可以刪除",Toast.LENGTH_LONG).show();
                        }else {
                            for(int i=0;i< list.get(pos).size();i++){
                                if(newstring.equals(list.get(pos).get(i))){
                                    if(!newstring.equals("")){
                                        check=false;
                                        list.get(pos).remove((i));
                                        if(i==0){
                                            spinner1.setSelection(1);
                                        }else {
                                            spinner1.setSelection(0);
                                        }
                                        Toast.makeText(getActivity(),"刪除成功",Toast.LENGTH_LONG).show();
                                        editText.setText("");
                                    }
                                }
                            }
                            if(check==true){
                                Toast.makeText(getActivity(),"查無此類別名稱",Toast.LENGTH_LONG).show();
                                editText.setText("");
                            }
                        }
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });


        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED)
            requestPermissions(new String[]{Manifest.permission.CAMERA},CAMERA_PERMISSION);
        //相機
        final String[] action = {"拍照","從相簿選擇","關閉"};
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialog_list = new AlertDialog.Builder(getActivity());
                dialog_list.setItems(action, new DialogInterface.OnClickListener(){
                    @Override
                    //只要你在onClick處理事件內，使用which參數，就可以知道按下陣列裡的哪一個了
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        if(action[which].equals("拍照")){
                            Intent highIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                            //取得相片檔案的URI位址及設定檔案名稱
                            File imageFile = getImageFile();
                            if (imageFile == null) return;
                            //取得相片檔案的URI位址
                            Uri imageUri = FileProvider.getUriForFile(
                                    getActivity(),
                                    "com.example.myapplication1.CameraEx",//記得要跟AndroidManifest.xml中的authorities 一致
                                    imageFile
                            );
                            highIntent.putExtra(MediaStore.EXTRA_OUTPUT,imageUri);
                            startActivityForResult(highIntent,REQUEST_HIGH_IMAGE);//開啟相機
                        }
                        else if(action[which].equals("從相簿選擇")){
                            Intent intent = new Intent();
                            /* 開啟Pictures畫面Type設定為image */
                            intent.setType("image/*");
                            /* 使用Intent.ACTION_GET_CONTENT這個Action */
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            /* 取得相片後返回本畫面 */
                            startActivityForResult(intent, REQUEST_SELECT);
                        }
                    }
                    private File getImageFile()  {
                        String time = new SimpleDateFormat("yyMMdd").format(new Date());
                        String fileName = time+"_";
                        File dir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                        try {
                            //給予檔案命名及檔案格式
                            File imageFile = File.createTempFile(fileName,".jpg",dir);
                            //給予全域變數中的照片檔案位置，方便後面取得
                            mPath = imageFile.getAbsolutePath();
                            return imageFile;
                        } catch (IOException e) {
                            return null;
                        }
                    }
                });
                dialog_list.show();
            }
        });
        //設定現在日期 String.format("%02d", day)
        String date = year + "/" + (month+1) + "/" + day;
        tdate.setText(date);
        //設定現在時間 (((hour+8)%24))
        String time = (hour) + ":" + String.format("%02d", min);
        ttime.setText(time);


        //日期選擇
         tdate.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Calendar cal=Calendar.getInstance();
                 int year=cal.get(Calendar.YEAR);
                 int month=cal.get(Calendar.MONTH);
                 int day=cal.get(Calendar.DATE);


                 dateSetListener=new DatePickerDialog.OnDateSetListener() {
                     @Override
                     public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                         String date = year + "/" + String.format("%02d", (month+1)) + "/" + String.format("%02d", day);
                         tdate.setText(date);
                     }
                 };

                 DatePickerDialog dialog=new DatePickerDialog(getActivity(), dateSetListener, year,month,day);
                 dialog.show();


             }
         });
         //時間選擇
         ttime.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Calendar cal=Calendar.getInstance();
                 int hour=cal.get(Calendar.HOUR_OF_DAY);
                 int min=cal.get(Calendar.MINUTE);

                 timeSetListener=new TimePickerDialog.OnTimeSetListener() {
                     @Override
                     public void onTimeSet(TimePicker timePicker, int hour, int min) {
                         Log.d("onTimeSet: hh/mm: ", +hour + "/" + min);
                         String time = (hour) + ":" + String.format("%02d", min);
                         ttime.setText(time);
                     }
                 };

                 TimePickerDialog dialog=new TimePickerDialog(
                         getActivity(),
                         timeSetListener,
                         hour,min, DateFormat.is24HourFormat(getActivity())
                 );
                 dialog.show();

             }

         });
        qrcodebt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), qrcode.class);
                startActivity(intent);
            }
        });
        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                money1=money.getText().toString();
                detail1=detail.getText().toString();
                if (money1.length()>1&&money1.startsWith("0")){
                    money1=money1.replaceFirst("0","");
                }

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Mysqlqrcode con = new Mysqlqrcode();
                        int q= con.insertData(global.code, money1,Maincategory,Subcategory,detail1,date,time,global.accountname);
                        if(q==0){
                            Looper.prepare();
                            Toast toast=Toast.makeText(getActivity(), "輸入成功", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER,0,0);
                            toast.show();
                            Looper.loop();
                            code.setText(null);codee.setText(null);
                            money.setText(null);detail.setText(null);
                        }else {
                            Looper.prepare();
                            Toast toast=Toast.makeText(getActivity(), "輸入失敗", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER,0,0);
                            toast.show();
                            Looper.loop();
                        }
                    }
                }).start();

            }
        });

        return view;

    }


    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode==KeyEvent.KEYCODE_BACK){
            Intent intent = new Intent(getActivity(), account.class);
            startActivity(intent);
        }
        return false;
    }

    private AdapterView.OnItemSelectedListener selectListener = new AdapterView.OnItemSelectedListener(){

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            pos = spinner.getSelectedItemPosition();
            if(lunch[pos]==lunch[0]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(0));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=(String) spinner.getSelectedItem();
            }else if (lunch[pos]==lunch[1]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(1));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=(String) spinner.getSelectedItem();

            }else if (lunch[pos]==lunch[2]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(2));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=lunch[pos];
            }else if (lunch[pos]==lunch[3]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(3));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=lunch[pos];
            }else if (lunch[pos]==lunch[4]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(4));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=lunch[pos];
            }else if (lunch[pos]==lunch[5]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(5));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=lunch[pos];
            }else if (lunch[pos]==lunch[6]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(6));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=lunch[pos];
            }else if (lunch[pos]==lunch[7]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(7));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=lunch[pos];
            }else if (lunch[pos]==lunch[8]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(8));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=lunch[pos];
            }else if (lunch[pos]==lunch[9]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(9));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=lunch[pos];
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    private AdapterView.OnItemSelectedListener selectListener1 = new AdapterView.OnItemSelectedListener(){

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            Subcategory= (String) spinner1.getSelectedItem();
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        photo = getActivity().findViewById(R.id.photo);
        if (requestCode == REQUEST_HIGH_IMAGE && resultCode == -1) {

            new Thread(() -> {
                //在BitmapFactory中以檔案URI路徑取得相片檔案，並處理為AtomicReference<Bitmap>，方便後續旋轉圖片
                AtomicReference<Bitmap> getHighImage = new AtomicReference<>(BitmapFactory.decodeFile(mPath));
                Matrix matrix = new Matrix();
                matrix.setRotate(0f);//轉90度
                Bitmap bit = Bitmap.createBitmap(getHighImage.get()
                        , 0, 0
                        , getHighImage.get().getWidth()
                        , getHighImage.get().getHeight()
                        , matrix, true);
                getHighImage.set(bit);

                getActivity().runOnUiThread(() -> {
                    //以Glide設置圖片(因為旋轉圖片屬於耗時處理，故會LAG一下，且必須使用Thread執行緒)
                    Glide.with(getActivity()).load(getHighImage.get()).centerCrop().into(photo);
                });

            }).start();
        } else if (requestCode == REQUEST_SELECT && resultCode == -1) {
            Uri uri = data.getData();
            Log.e("uri", uri.toString());
            ContentResolver cr = getActivity().getContentResolver();
            try {
                Bitmap bitmap = BitmapFactory.decodeStream(cr.openInputStream(uri));
                /* 將Bitmap設定到ImageView */
                photo.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                Log.e("Exception", e.getMessage(), e);
            }
        } else {
            Toast.makeText(getActivity(), "未作任何動作", Toast.LENGTH_SHORT).show();
        }
    }
}

