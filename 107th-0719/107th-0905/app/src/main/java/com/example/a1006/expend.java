package com.example.a1006;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.provider.MediaStore;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link expend#newInstance} factory method to
 * create an instance of this fragment.
 */
public class expend extends Fragment{

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public expend() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment tab1.
     */
    // TODO: Rename and change types and number of parameters
    public static expend newInstance(String param1, String param2) {
        expend fragment = new expend();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }
    public static final String TAG = expend.class.getSimpleName()+"My";

    private String mPath = "";//設置高畫質的照片位址
    public static final int CAMERA_PERMISSION = 100;//檢測相機權限用
    public static final int REQUEST_HIGH_IMAGE = 101;//檢測高畫質相機回傳
    public static final int REQUEST_SELECT = 102;//檢測低畫質相機回傳
    TextView tdate,ttime;
    private  Spinner spinner;
    EditText code,money,detail,codee;
    Button enter,add,delete;
    String Maincategory,moneybuffer, codebuffer,detailbuffer;
    boolean end=false;
    ArrayList<String> Industrycode=new ArrayList<String>();
    ArrayList<String> category=new ArrayList<String>();
    ImageView photo,qrcodebt;
    DatePickerDialog.OnDateSetListener dateSetListener;
    TimePickerDialog.OnTimeSetListener timeSetListener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view1=inflater.inflate(R.layout.addview,container,false);
        View view=inflater.inflate(R.layout.expend, container, false);
        ttime=view.findViewById(R.id.time);
        tdate=view.findViewById(R.id.date);
        spinner = view.findViewById(R.id.spinner);
        enter=view.findViewById(R.id.button4);
        photo=view.findViewById(R.id.photo);
        add=view.findViewById(R.id.button9);
        delete=view.findViewById(R.id.button10);
        qrcodebt=view.findViewById(R.id.qrcodebt);
        Calendar cal=Calendar.getInstance();
        int year=cal.get(Calendar.YEAR);
        int month=cal.get(Calendar.MONTH);
        int day=cal.get(Calendar.DATE);
        int hour=cal.get(Calendar.HOUR_OF_DAY);
        int min=cal.get(Calendar.MINUTE);
        code=view.findViewById(R.id.invoice2);
        codee=view.findViewById(R.id.invoice);
        money=view.findViewById(R.id.money);
        detail=view.findViewById(R.id.detail);
        detail.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        detail.setGravity(Gravity.TOP);
        Global global=(Global)getActivity().getApplicationContext();
        ArrayList<modeexdata> exhistorydataArrayList=global.getExhistorydataArrayList();

        //設定現在日期 String.format("%02d", day)
        final String date = year + "/" + (month+1) + "/" + day;
        tdate.setText(date);
        //設定現在時間 (((hour+8)%24))
        String time = (hour) + ":" + String.format("%02d", min);
        ttime.setText(time);


        new Thread(new Runnable() {
            @Override
            public void run() {
                Mysqlqrcode con = new Mysqlqrcode();
                category= con.getExpendcategory(global.accountname);
                System.out.println(category.size());
                end=true;
            }
        }).start();
        while (end!=true){
            ArrayAdapter<String> lunchList = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, category);
            lunchList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(lunchList);
        }
        end=false;
        if (global.positionEX!=null){
            System.out.println("position"+ global.positionEX);
            money.setText(exhistorydataArrayList.get(Integer.parseInt(global.positionEX)).moneyEx);
            for(int i=0;i<category.size();i++){
                if(exhistorydataArrayList.get(Integer.parseInt(global.positionEX)).maincategoryEx.equals(category.get(i))){
                    spinner.setSelection(i);
                }
            }
        }
        if(global.code!=null){
            String codenumber=global.code.substring(2);
            String codeenglish=global.code.substring(0,2);
            codee.setText(codeenglish);
            code.setText(codenumber);
            money.setText(String.valueOf(global.money));
            detail.setText(global.detail);
            String datearr[]=global.date.split("\\/");

            if(datearr[1].substring(0,1).equals("0")){
                datearr[1]=datearr[1].substring(1);
            }
            if(datearr[2].substring(0,1).equals("0")){
                datearr[2]=datearr[2].substring(1);
            }

            String datebuffer = String.valueOf(Integer.valueOf(datearr[0])+1911 )+ "/"+ datearr[1] + "/" +datearr[2];
            tdate.setText(datebuffer);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Mysqlqrcode con = new Mysqlqrcode();
                    Industrycode= con.getIndustrycode(global.Uniformnumbers);
                    System.out.println("j:"+Industrycode.size());
                    end=true;
                }
            }).start();
            while (end!=true){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            for(int i=0;i<Industrycode.size();i++){
                System.out.println("i:"+Industrycode.get(i));
                if(Industrycode.get(i).equals("56") || Industrycode.get(i).equals("472")){
                    spinner.setSelection(0);
                }
                else if(Industrycode.get(i).equals("463")){
                    spinner.setSelection(1);
                }
                else if(Industrycode.get(i).equals("932") || Industrycode.get(i).equals("55")){
                    spinner.setSelection(2);
                }
                else if(Industrycode.get(i).equals("471") || Industrycode.get(i).equals("473")){
                    spinner.setSelection(3);
                }
                else if(Industrycode.get(i).equals("475") ){
                    spinner.setSelection(4);
                }
                else if(Industrycode.get(i).equals("476") ){
                    spinner.setSelection(5);
                }
                else if(Industrycode.get(i).equals("474") ){
                    spinner.setSelection(6);
                }

            }
        }
        codee.setTransformationMethod(new AllCapTransformationMethod(true));


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("新增分類");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newstring = editText.getText().toString();
                        for(int i=0;i<category.size();i++){
                            if(newstring.equals(category.get(i))){
                                Toast.makeText(getActivity(),"此類別已存在",Toast.LENGTH_LONG).show();
                                editText.setText("");
                                return;
                            }
                        }
                        if(!newstring.equals("")){
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Mysqlqrcode con = new Mysqlqrcode();
                                    con.insertExpendcategory(newstring,global.accountname);
                                }
                            }).start();
                            category.add(newstring);
                            Toast.makeText(getActivity(),"新增成功",Toast.LENGTH_LONG).show();
                            editText.setText("");
                        }

                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("刪除分類");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        boolean check=true;
                        String newstring = editText.getText().toString();
                            for(int i=0;i< category.size();i++){
                                if(newstring.equals(category.get(i))){
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Mysqlqrcode con = new Mysqlqrcode();
                                            con.deletecategory(newstring,global.accountname);
                                        }
                                    }).start();
                                    check=false;
                                    category.remove((i));
                                    if(i==0){
                                        spinner.setSelection(1);
                                    }else {
                                        spinner.setSelection(0);
                                    }
                                    Toast.makeText(getActivity(),"刪除成功",Toast.LENGTH_LONG).show();
                                    editText.setText("");
                                }
                          }
                        if(check==true){
                            Toast.makeText(getActivity(),"查無此類別名稱",Toast.LENGTH_LONG).show();
                            editText.setText("");
                        }
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });

        //日期選擇
         tdate.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Calendar cal=Calendar.getInstance();
                 int year=cal.get(Calendar.YEAR);
                 int month=cal.get(Calendar.MONTH);
                 int day=cal.get(Calendar.DATE);


                 dateSetListener=new DatePickerDialog.OnDateSetListener() {
                     @Override
                     public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                         String date = year + "/" + (month+1) + "/" + day;
                         tdate.setText(date);
                     }
                 };

                 DatePickerDialog dialog=new DatePickerDialog(getActivity(), dateSetListener, year,month,day);
                 dialog.show();


             }
         });
         //時間選擇
         ttime.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Calendar cal=Calendar.getInstance();
                 int hour=cal.get(Calendar.HOUR_OF_DAY);
                 int min=cal.get(Calendar.MINUTE);

                 timeSetListener=new TimePickerDialog.OnTimeSetListener() {
                     @Override
                     public void onTimeSet(TimePicker timePicker, int hour, int min) {
                         Log.d("onTimeSet: hh/mm: ", +hour + "/" + min);
                         String time = (hour) + ":" + String.format("%02d", min);
                         ttime.setText(time);
                     }
                 };

                 TimePickerDialog dialog=new TimePickerDialog(
                         getActivity(),
                         timeSetListener,
                         hour,min, DateFormat.is24HourFormat(getActivity())
                 );
                 dialog.show();

             }

         });
        qrcodebt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), qrcode.class);
                startActivity(intent);
            }
        });
        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String datebuffer=tdate.getText().toString();
                String timebuffer=ttime.getText().toString();
               moneybuffer=money.getText().toString();
               detailbuffer=detail.getText().toString();
               Maincategory=(String) spinner.getSelectedItem();
               codebuffer=(String.valueOf(codee.getText())+String.valueOf(code.getText()));
                if (moneybuffer.length()>1&&moneybuffer.startsWith("0")){
                    moneybuffer=moneybuffer.replaceFirst("0","");
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Mysqlqrcode con = new Mysqlqrcode();
                        int q=con.insertExpendData(codebuffer, moneybuffer,Maincategory,detailbuffer,datebuffer,timebuffer,global.accountname);
                        if(q==0){
                            Looper.prepare();
                            Toast toast=Toast.makeText(getActivity(), "輸入成功", Toast.LENGTH_LONG);
                            toast.show();
                            Looper.loop();

                        }else {
                            Looper.prepare();
                            Toast toast=Toast.makeText(getActivity(), "輸入失敗", Toast.LENGTH_LONG);
                            toast.show();
                            Looper.loop();
                        }
                        end=true;
                    }
                }).start();
                tdate.setText(date);
                ttime.setText(time);
                code.setText(null);codee.setText(null);
                money.setText(null);detail.setText(null);
            }
        });
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED)
            requestPermissions(new String[]{Manifest.permission.CAMERA},CAMERA_PERMISSION);
        //相機
        final String[] action = {"拍照","從相簿選擇","關閉"};
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialog_list = new AlertDialog.Builder(getActivity());
                dialog_list.setItems(action, new DialogInterface.OnClickListener(){
                    @Override
                    //只要你在onClick處理事件內，使用which參數，就可以知道按下陣列裡的哪一個了
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        if(action[which].equals("拍照")){
                            Intent highIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                            //取得相片檔案的URI位址及設定檔案名稱
                            File imageFile = getImageFile();
                            if (imageFile == null) return;
                            //取得相片檔案的URI位址
                            Uri imageUri = FileProvider.getUriForFile(
                                    getActivity(),
                                    "com.example.myapplication1.CameraEx",//記得要跟AndroidManifest.xml中的authorities 一致
                                    imageFile
                            );
                            highIntent.putExtra(MediaStore.EXTRA_OUTPUT,imageUri);
                            startActivityForResult(highIntent,REQUEST_HIGH_IMAGE);//開啟相機
                        }
                        else if(action[which].equals("從相簿選擇")){
                            Intent intent = new Intent();
                            /* 開啟Pictures畫面Type設定為image */
                            intent.setType("image/*");
                            /* 使用Intent.ACTION_GET_CONTENT這個Action */
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            /* 取得相片後返回本畫面 */
                            startActivityForResult(intent, REQUEST_SELECT);
                        }
                    }
                    private File getImageFile()  {
                        String time = new SimpleDateFormat("yyMMdd").format(new Date());
                        String fileName = time+"_";
                        File dir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                        try {
                            //給予檔案命名及檔案格式
                            File imageFile = File.createTempFile(fileName,".jpg",dir);
                            //給予全域變數中的照片檔案位置，方便後面取得
                            mPath = imageFile.getAbsolutePath();
                            return imageFile;
                        } catch (IOException e) {
                            return null;
                        }
                    }
                });
                dialog_list.show();
            }
        });

        return view;

    }


    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode==KeyEvent.KEYCODE_BACK){
            Intent intent = new Intent(getActivity(), account.class);
            startActivity(intent);
        }
        return false;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        photo = getActivity().findViewById(R.id.photo);
        if (requestCode == REQUEST_HIGH_IMAGE && resultCode == -1) {

            new Thread(() -> {
                //在BitmapFactory中以檔案URI路徑取得相片檔案，並處理為AtomicReference<Bitmap>，方便後續旋轉圖片
                AtomicReference<Bitmap> getHighImage = new AtomicReference<>(BitmapFactory.decodeFile(mPath));
                Matrix matrix = new Matrix();
                matrix.setRotate(0f);//轉90度
                Bitmap bit = Bitmap.createBitmap(getHighImage.get()
                        , 0, 0
                        , getHighImage.get().getWidth()
                        , getHighImage.get().getHeight()
                        , matrix, true);
                getHighImage.set(bit);

                getActivity().runOnUiThread(() -> {
                    //以Glide設置圖片(因為旋轉圖片屬於耗時處理，故會LAG一下，且必須使用Thread執行緒)
                    Glide.with(getActivity()).load(getHighImage.get()).centerCrop().into(photo);
                });

            }).start();
        } else if (requestCode == REQUEST_SELECT && resultCode == -1) {
            Uri uri = data.getData();
            Log.e("uri", uri.toString());
            ContentResolver cr = getActivity().getContentResolver();
            try {
                Bitmap bitmap = BitmapFactory.decodeStream(cr.openInputStream(uri));
                /* 將Bitmap設定到ImageView */
                photo.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                Log.e("Exception", e.getMessage(), e);
            }
        } else {
            Toast.makeText(getActivity(), "未作任何動作", Toast.LENGTH_SHORT).show();
        }
    }
}

