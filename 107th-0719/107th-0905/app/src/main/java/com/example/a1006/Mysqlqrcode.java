package com.example.a1006;

import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import static android.os.SystemClock.sleep;

public class Mysqlqrcode  {
    String mysql_ip = "120.105.161.106";
    int mysql_port = 3306; // Port 預設為 3306
    String db_name = "107th";
    String db_user = "root";
    String db_password = "th107";

    public void run() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Log.v("DB","加載驅動成功");
        }catch( ClassNotFoundException e) {
            Log.e("DB","加載驅動失敗");
            return;
        }

        // 連接資料庫
        try {
            String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+db_name+"?useUnicode=true&characterEncoding=UTF-8";
            Connection con = DriverManager.getConnection(url,db_user,db_password);
            Log.v("DB","遠端連接成功");
        }catch(SQLException e) {
            Log.e("DB","遠端連接失敗");
            Log.e("DB", e.toString());
        }
    }
    public ArrayList<forgetpasswordvalue> getAllAccount(){
        ArrayList<forgetpasswordvalue> av =new ArrayList<forgetpasswordvalue>();
        try {
            String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+db_name+"?useUnicode=true&characterEncoding=UTF-8";
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(url, db_user, db_password);

            String sql = "SELECT * FROM `members`";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next())
            {
                String account =rs.getString("account");
                String mail=rs.getString("mail");
                av.add(new forgetpasswordvalue(account,mail));
            }
            st.close();
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
        return av;
    }
    public ArrayList<store> getData(String address) {
        ArrayList<store> arrayList=new ArrayList<store>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Log.v("DB","加載驅動成功");
            try {
                String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+db_name+"?useUnicode=true&characterEncoding=UTF-8";
                Connection con = DriverManager.getConnection(url, db_user, db_password);
                Log.v("DB",address);
                String str="%"+address+"%";
                String sql = "Select * FROM store where address LIKE '"+str+"'";

                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sql);

                while (rs.next())
                {
                    String name = rs.getString("name");
                    String add = rs.getString("address");
                    float lat =rs.getFloat("lat");
                    float lng=rs.getFloat("lng");
                    String time=rs.getString("tradetime");
                    String phone=rs.getString("phone");
                    store sto=new store(name,add,lat,lng,0,time,phone);

                    arrayList.add(sto);
                }


                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }catch( ClassNotFoundException e) {
            Log.e("DB","加載驅動失敗");
        }
        return arrayList;

    }
    public String getassets(String account){
        String assets=null;
        try {
            String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+db_name+"?useUnicode=true&characterEncoding=UTF-8";
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(url, db_user, db_password);
            String sql = "SELECT * FROM members WHERE account = '"+account+"' ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                assets=rs.getString("assets");
                if(assets.equals("NULL")){
                    assets="0";
                }
            }
            st.close();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            Log.e("DB", "寫入資料失敗");
            Log.e("DB", e.toString());
        }
        return assets;
    }
    public void insertAssets(String account,String assets)  {
        try {
            String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+db_name+"?useUnicode=true&characterEncoding=UTF-8";
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(url, db_user, db_password);
            String sql = "UPDATE members SET assets='"+assets+"' WHERE account='"+account+"'";
            Statement st = con.createStatement();
            st.executeUpdate(sql);
            st.close();
            Log.v("DB", "寫入資料完成：" );

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            Log.e("DB", "寫入資料失敗");
            Log.e("DB", e.toString());
        }
    }
    public void insertAccount(String account,String password,String email)  {
        try {
            String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+db_name+"?useUnicode=true&characterEncoding=UTF-8";
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(url, db_user, db_password);
            String sql = "INSERT INTO members "+"VALUES('"+account+"','使用者','"+password+"','"+email+"',NULL,'X')";
            Statement st = con.createStatement();
            st.executeUpdate(sql);
            st.close();
            Log.v("DB", "寫入資料完成：" );

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            Log.e("DB", "寫入資料失敗");
            Log.e("DB", e.toString());
        }
    }
    public AccountValue getAccount(String account){
        AccountValue av =new AccountValue();
        System.out.println(account);
        try {
            String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+db_name+"?useUnicode=true&characterEncoding=UTF-8";
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(url, db_user, db_password);

            String sql = "SELECT * FROM `members` WHERE account = '"+account+"' ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                av.setAccount(rs.getString("account"),rs.getString("passwd"),rs.getString("state"));
                System.out.println(av.account);
            }
            else{
                av.setAccount("noaccount","nopasswd","X");
            }
            st.close();


        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
        return av;
    }
    int insertExpendData(String code,String money,String maincategory,String detail1,String date,String time,String account) {
        int q=0;
        try {
            String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+account+"?useUnicode=true&characterEncoding=UTF-8";
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(url, db_user, db_password);
            String sql ="INSERT INTO expend (code,money,maincategory,productname,date,time) values " + "('"+code+"','"+money+"','"+maincategory+"','"+detail1+"','"+date+"','"+time+"')";//`code`,`date`,`randomcode`,`productname`
            Statement st = con.createStatement();
            st.executeUpdate(sql);
            st.close();
            Log.v("DB", "寫入資料完成：" + date);
        } catch (SQLException  e) {
            e.printStackTrace();
            Log.e("DB", "寫入資料失敗");
            q=1;
            Log.e("DB", e.toString());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return q;
    }
    int insertIncomeData(String money, String maincategory, String detail1, String date, String time,String account) {
        int q=0;
        try {
            String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+account+"?useUnicode=true&characterEncoding=UTF-8";
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(url, db_user, db_password);
            String sql ="INSERT INTO income (money,maincategory,productname,date,time) values " + "('"+money+"','"+maincategory+"','"+detail1+"','"+date+"','"+time+"')";//`code`,`date`,`randomcode`,`productname`
            Statement st = con.createStatement();
            st.executeUpdate(sql);
            st.close();
            Log.v("DB", "寫入資料完成：" + date);
        } catch (SQLException  e) {
            e.printStackTrace();
            Log.e("DB", "寫入資料失敗");
            q=1;
            Log.e("DB", e.toString());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return q;
    }
    int insertmodleData(String title,String maincategory,String money,String account,String type) {
        int q=0;
        try {
            String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+account+"?useUnicode=true&characterEncoding=UTF-8";
            if(type.equals("支出")){
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = DriverManager.getConnection(url, db_user, db_password);
                String sql ="INSERT INTO modle (title,maincategory,money,type) values " + "('"+title+"','"+maincategory+"','"+money+"','expend')";
                Statement st = con.createStatement();
                st.executeUpdate(sql);
                st.close();
                Log.v("DB", "寫入資料完成：" );
            }else if(type.equals("收入")){
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = DriverManager.getConnection(url, db_user, db_password);
                String sql ="INSERT INTO modle (title,maincategory,money,type) values " + "('"+title+"','"+maincategory+"','"+money+"','income')";
                Statement st = con.createStatement();
                st.executeUpdate(sql);
                st.close();
                Log.v("DB", "寫入資料完成：" );
            }

        } catch (SQLException  e) {
            e.printStackTrace();
            Log.e("DB", "寫入資料失敗");
            q=1;
            Log.e("DB", e.toString());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return q;
    }
    public ArrayList<historydata> gethistoryData(String account) {
        ArrayList<historydata> historydataArrayListarrayList=new ArrayList<historydata>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Log.v("DB","加載驅動成功");
            try {
                String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+account+"?useUnicode=true&characterEncoding=UTF-8";
                Connection con = DriverManager.getConnection(url, db_user, db_password);
                String sql = "SELECT * FROM `expend`";
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sql);

                while (rs.next())
                {
                    String maincategory =rs.getString("maincategory");
                    String productname=rs.getString("productname");
                    String date=rs.getString("date");
                    String time=rs.getString("time");
                    int money=rs.getInt("money");
                    historydata hdata=new historydata(maincategory,productname,date,time,money,"expend");
                    historydataArrayListarrayList.add(hdata);
                }
                sql = "SELECT * FROM `income`";
                rs = st.executeQuery(sql);

                while (rs.next())
                {
                    String maincategory = rs.getString("maincategory");
                    String productname=rs.getString("productname");
                    String date=rs.getString("date");
                    String time=rs.getString("time");
                    int money=rs.getInt("money");
                    historydata hdata=new historydata(maincategory,productname,date,time,money,"income");
                    historydataArrayListarrayList.add(hdata);
                }


                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }catch( ClassNotFoundException e) {
            Log.e("DB","加載驅動失敗");
        }
        return historydataArrayListarrayList;
    }
    public ArrayList<modeexdata> getModeExData(String account ) {
        ArrayList<modeexdata> historydataArrayList=new ArrayList<modeexdata>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Log.v("DB","加載驅動成功");
            try {
                String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+account+"?useUnicode=true&characterEncoding=UTF-8";
                Connection con = DriverManager.getConnection(url, db_user, db_password);
                String sql = "SELECT * FROM `modle`";
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sql);

                while (rs.next())
                {
                    String titleEx =rs.getString("title");
                    String maincategoryEx=rs.getString("maincategory");
                    String moneyEx=rs.getString("money");
                    String type=rs.getString("type");
                    modeexdata hdata=new modeexdata(titleEx,maincategoryEx,moneyEx,type);
                    historydataArrayList.add(hdata);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }catch( ClassNotFoundException e) {
            Log.e("DB","加載驅動失敗");
        }
        return historydataArrayList;
    }
    public ArrayList<invoicedata> getInvoiceData(String account) {
        ArrayList<invoicedata> invoicedataArrayList=new ArrayList<invoicedata>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Log.v("DB","加載驅動成功");
            try {
                String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+account+"?useUnicode=true&characterEncoding=UTF-8";
                Connection con = DriverManager.getConnection(url, db_user, db_password);
                String sql = "SELECT * FROM `expend`";
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sql);

                while (rs.next())
                {
                    String code =rs.getString("code");
                    String money=rs.getString("money");
                    String date=rs.getString("date");
                    invoicedata idata=new invoicedata(code,date,money);
                    invoicedataArrayList.add(idata);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }catch( ClassNotFoundException e) {
            Log.e("DB","加載驅動失敗");
        }
        return invoicedataArrayList;
    }
    public ArrayList<awardsnumber> getawardsnumber(String month) {
        ArrayList<awardsnumber> awardsnumberArrayList=new ArrayList<awardsnumber>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Log.v("DB","加載驅動成功");
            try {
                String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+db_name+"?useUnicode=true&characterEncoding=UTF-8";
                Connection con = DriverManager.getConnection(url, db_user, db_password);
                System.out.println(month);
                String sql = "SELECT * FROM `invoice`  WHERE month = '"+month+"' ";
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sql);

                while (rs.next())
                {
                    String number =rs.getString("number");
                    String awards=rs.getString("awards");
                    awardsnumber anumbers=new awardsnumber(number,awards);
                    awardsnumberArrayList.add(anumbers);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }catch( ClassNotFoundException e) {
            Log.e("DB","加載驅動失敗");
        }
        return awardsnumberArrayList;
    }
    public ArrayList<String> getIndustrycode(String Uniformnumbers){
        ArrayList<String> industrycode=new ArrayList<String>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Log.v("DB","加載驅動成功");
            try {
                String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+db_name+"?useUnicode=true&characterEncoding=UTF-8";
                Connection con = DriverManager.getConnection(url, db_user, db_password);
                String sql = "SELECT * FROM `uniformnumbers`  WHERE Uniformnumbers = '"+Uniformnumbers+"' ";
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sql);

                while (rs.next())
                {
                    String Industrycode =rs.getString("Industrycode");
                    String buffer=Industrycode.substring(0,1);
                    if(buffer.equals("5")){
                        Industrycode=Industrycode.substring(0,2);
                    }
                    else{
                        Industrycode=Industrycode.substring(0,3);
                    }
                    industrycode.add(Industrycode);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }catch( ClassNotFoundException e) {
            Log.e("DB","加載驅動失敗");
        }
        return industrycode;
    }
    public ArrayList<String> getExpendcategory(String account){
        ArrayList<String> category=new ArrayList<String>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Log.v("DB","加載驅動成功");
            try {
                String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+account+"?useUnicode=true&characterEncoding=UTF-8";
                Connection con = DriverManager.getConnection(url, db_user, db_password);
                String sql = "SELECT * FROM `category`  WHERE type = 'expend'";
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sql);

                while (rs.next())
                {
                    String maincategory =rs.getString("maincategory");
                    category.add(maincategory);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }catch( ClassNotFoundException e) {
            Log.e("DB","加載驅動失敗");
        }
        return category;
    }
    public ArrayList<String> getIncomecategory(String account){
        ArrayList<String> category=new ArrayList<String>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Log.v("DB","加載驅動成功");
            try {
                String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+account+"?useUnicode=true&characterEncoding=UTF-8";
                Connection con = DriverManager.getConnection(url, db_user, db_password);
                String sql = "SELECT * FROM `category`  WHERE type = 'income'";
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sql);

                while (rs.next())
                {
                    String maincategory =rs.getString("maincategory");
                    category.add(maincategory);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }catch( ClassNotFoundException e) {
            Log.e("DB","加載驅動失敗");
        }
        return category;
    }
    public void insertExpendcategory(String category,String account){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Log.v("DB","加載驅動成功");
            try {
                String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+account+"?useUnicode=true&characterEncoding=UTF-8";
                Connection con = DriverManager.getConnection(url, db_user, db_password);
                String sql = "INSERT INTO category "+"VALUES('"+category+"','expend')";
                Statement st = con.createStatement();
                st.executeUpdate(sql);
                st.close();
                Log.v("DB", "寫入資料完成：" );
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }catch( ClassNotFoundException e) {
            Log.e("DB","加載驅動失敗");
        }

    }
    public void insertIncomecategory(String category,String account){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Log.v("DB","加載驅動成功");
            try {
                String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+account+"?useUnicode=true&characterEncoding=UTF-8";
                Connection con = DriverManager.getConnection(url, db_user, db_password);
                String sql = "INSERT INTO category "+"VALUES('"+category+"','income')";
                Statement st = con.createStatement();
                st.executeUpdate(sql);
                st.close();
                Log.v("DB", "寫入資料完成：" );
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }catch( ClassNotFoundException e) {
            Log.e("DB","加載驅動失敗");
        }

    }
    public void deletecategory(String category,String account){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Log.v("DB","加載驅動成功");
            try {
                String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+account+"?useUnicode=true&characterEncoding=UTF-8";
                Connection con = DriverManager.getConnection(url, db_user, db_password);
                String sql = "DELETE FROM category " + " WHERE maincategory = '"+category+"'";
                Statement st = con.createStatement();
                st.executeUpdate(sql);
                st.close();
                Log.v("DB", "寫入資料完成：" );
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }catch( ClassNotFoundException e) {
            Log.e("DB","加載驅動失敗");
        }
    }
    public void deletemode(String title,String account){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Log.v("DB","加載驅動成功");
            try {
                String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+account+"?useUnicode=true&characterEncoding=UTF-8";
                Connection con = DriverManager.getConnection(url, db_user, db_password);
                String sql = "DELETE FROM modle " + " WHERE title = '"+title+"'";
                Statement st = con.createStatement();
                st.executeUpdate(sql);
                st.close();
                Log.v("DB", "寫入資料完成：" );
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }catch( ClassNotFoundException e) {
            Log.e("DB","加載驅動失敗");
        }
    }

}
