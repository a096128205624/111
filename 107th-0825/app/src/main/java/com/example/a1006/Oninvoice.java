package com.example.a1006;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Oninvoice extends AppCompatActivity {
    public String reciprocal(String nowtime,int n,String year)  {
        String timebuffer[]=nowtime.split("\\/");
        System.out.println("nowtime:"+nowtime);
        System.out.println("year:"+year);
        String daylinetime=null;
        if(n==0){
            daylinetime=year+"/07/05";
        }
        else if(n==1){
            daylinetime=year+"/09/05";
        }
        else if(n==2){
            daylinetime=year+"/11/05";
        }
        else if(n==3){
            daylinetime=String.valueOf((Integer.valueOf(year)+1))+"/01/05";
        }
        else if(n==4){
            daylinetime=String.valueOf((Integer.valueOf(year)+1))+"/03/05";
        }
        else if(n==5){
            daylinetime=String.valueOf((Integer.valueOf(year)+1))+"/05/05";
        }SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        Date dt1 = null;
        try {
            dt1 = sdf.parse(nowtime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date dt2 = null;
        try {
            dt2 = sdf.parse(daylinetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(nowtime+"/"+daylinetime);
        Long ut1=dt1.getTime();
        Long ut2=dt2.getTime();
        System.out.println("ut2:"+ut2);

//相減獲得兩個時間差距的毫秒

        Long timeP=ut2-ut1;
        Long day=timeP/(1000*60*60*24);//日差
        System.out.println(day);
        if(day>0 && day<120){
            return Long.toString(day);
        }
        else if(day>120){
            return "未開獎";
        }
        else{
            return "已過期";
        }
    }
    TextView time,summoney,getmoney,reciprocal;
    private DrawerLayout drawerLayout;
    Calendar c = Calendar.getInstance();
    int nowyear = c.get(Calendar.YEAR);
    int nowmonth = c.get(Calendar.MONTH)+1;
    int nowdate = c.get(Calendar.DATE);
    int n;
    int countyear=2021;
    int year=110+(nowyear-countyear);
    int sum=0;
    String timebuffer=nowyear+"/"+String.format("%02d",nowmonth)+"/"+nowdate;
    int sumgetmoney=0;
    ListView history;
    ArrayList<awardsnumber> awardsnumberArrayList=new ArrayList<awardsnumber>();
    ArrayList<invoicedata> invoicedataArrayList=new ArrayList<invoicedata>();
    boolean end=false;
    boolean findawardsadd=false;
    boolean findawardsminus=false;
    ImageButton addmonth,minusmonth;
    int smallmonth=12;
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.oninvoice);
        Global global=(Global) getApplicationContext();
        String account=global.getAccountname();
//toolbar------------------------------------------------------------------------------
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout=findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        toolbar.setTitleTextColor(Color.WHITE);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        TextView navaccount=headerView.findViewById(R.id.accountname);
        navaccount.setText(account);
//nav---------------------------------------------------------------------------------
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.itemfoodcommend:
                        Intent intent = new Intent(Oninvoice.this, foodcommend.class);
                        startActivity(intent);
                        break;
                    case R.id.itemaccount:
                        intent = new Intent(Oninvoice.this, account.class);
                        startActivity(intent);
                        break;
                    case R.id.itemoninvoice:
                        intent = new Intent(Oninvoice.this, Oninvoice.class);
                        startActivity(intent);
                        break;
                }

                menuItem.setChecked(true);//點選了把它設為選中狀態
                return true;
            }
        });
//time-------------------------------------------------------------------------------
        reciprocal=findViewById(R.id.reciprocal);
        time=findViewById(R.id.time);
        String month[]={"01-02","03-04","05-06","07-08","09-10","11-12"};
        if((nowmonth%2)==0){
            n=(nowmonth/2)-1;
        }
        else{
            n=nowmonth/2;
        }
        time.setText(String.valueOf(year)+" 年 "+month[n]+" 月 ");
        reciprocal.setText(reciprocal(timebuffer,n,String.valueOf(nowyear)));

//history----------------------------------------------------------------------------
        history=findViewById(R.id.history);
        Global globalaccountname=(Global)getApplicationContext();
        String accountname=globalaccountname.getAccountname();
        ArrayList<invoicedata> invoicedatabuffer=new ArrayList<invoicedata>();

        String m[]={"1-2","3-4","5-6","7-8","9-10","11-12"};
        new Thread(new Runnable() {
            @Override
            public void run() {
                Mysqlqrcode con = new Mysqlqrcode();
                invoicedataArrayList= con.getInvoiceData(accountname);
                for(int i=0;i<invoicedataArrayList.size();i++){
                    String nowday[]=invoicedataArrayList.get(i).date.split("\\/");
                    String showmonth[]=m[n].split("\\-");


                    if(nowday[0].equals(String.valueOf(nowyear)) && (nowday[1].equals(showmonth[0]) || nowday[1].equals(showmonth[1]))){
                        invoicedatabuffer.add(invoicedataArrayList.get(i));
                        sum+=Integer.valueOf(invoicedataArrayList.get(i).money);
                    }
                }
                end=true;
                Collections.sort(invoicedatabuffer, new invoicesort());
                System.out.println(invoicedatabuffer.size());
            }
        }).start();

        while (end!=true) {
            summoney=findViewById(R.id.summoney);
            summoney.setText(String.valueOf(sum));
            BaseAdapter adapter = new BaseAdapter() {
                @Override
                public int getCount() {
                    return invoicedatabuffer.size();
                }

                @Override
                public Object getItem(int i) {
                    return i;
                }

                @Override
                public long getItemId(int i) {
                    return i;
                }

                @Override
                public View getView(int i, View view, ViewGroup viewGroup) {
                    View layout = View.inflate(Oninvoice.this, R.layout.invoicedatalayout, null);
                    TextView code = layout.findViewById(R.id.code);
                    TextView state = layout.findViewById(R.id.state);
                    TextView money = layout.findViewById(R.id.money);
                    TextView time = layout.findViewById(R.id.time);
                    code.setText(invoicedatabuffer.get(i).code);
                    money.setText(invoicedatabuffer.get(i).money);
                    time.setText(invoicedatabuffer.get(i).date);

                    return layout;

                }
            };
            history.setAdapter(adapter);
        }
//button-----------------------------------------------------------------------------------
        getmoney=findViewById(R.id.getmoney);
        addmonth=findViewById(R.id.addmonth);
        minusmonth=findViewById(R.id.minusmonth);
        addmonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findawardsadd=false;
                sum=0;
                sumgetmoney=0;
                getmoney.setText("本期無中獎發票");
                invoicedatabuffer.clear();
                n=n+1;
                if(n>5){
                    n=0;
                    year=year+1;
                    nowyear+=1;
                }
                reciprocal.setText(reciprocal(timebuffer,n,String.valueOf(nowyear)));
                String showmonth[]=m[n].split("\\-");
                time.setText(String.valueOf(year)+" 年 "+month[n]+" 月 ");
                for(int i=0;i<invoicedataArrayList.size();i++){
                    String nowday[]=invoicedataArrayList.get(i).date.split("\\/");
                    if(nowday[0].equals(String.valueOf(nowyear))){
                        if((nowday[1].equals(showmonth[0]) || nowday[1].equals(showmonth[1]))){
                            invoicedatabuffer.add(invoicedataArrayList.get(i));
                            sum+=Integer.valueOf(invoicedataArrayList.get(i).money);
                        }
                    }
                }
                Collections.sort(invoicedatabuffer, new invoicesort());
                summoney=findViewById(R.id.summoney);
                summoney.setText(String.valueOf(sum));

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Mysqlqrcode con = new Mysqlqrcode();
                        awardsnumberArrayList= con.getawardsnumber(nowyear+"/"+showmonth[0]);
                        findawardsadd=true;
                    }
                }).start();
                while (findawardsadd!=true){
                    BaseAdapter adapter = new BaseAdapter() {
                        @Override
                        public int getCount() {
                            return invoicedatabuffer.size();
                        }

                        @Override
                        public Object getItem(int i) {
                            return i;
                        }

                        @Override
                        public long getItemId(int i) {
                            return i;
                        }

                        @Override
                        public View getView(int i, View view, ViewGroup viewGroup) {
                            View layout = View.inflate(Oninvoice.this, R.layout.invoicedatalayout, null);
                            TextView code = layout.findViewById(R.id.code);
                            TextView state = layout.findViewById(R.id.state);
                            TextView money = layout.findViewById(R.id.money);
                            TextView time = layout.findViewById(R.id.time);
                            code.setText(invoicedatabuffer.get(i).code);
                            money.setText(invoicedatabuffer.get(i).money);
                            time.setText(invoicedatabuffer.get(i).date);
                            String showmonth[]=m[n].split("\\-");
                            String number=invoicedatabuffer.get(i).code.substring(2);
                            System.out.println("12313:"+awardsnumberArrayList.size());
                            boolean blackjack=false;
                            for(int j=0;j<awardsnumberArrayList.size();j++){
                                number=invoicedatabuffer.get(i).code.substring(2);
                                //特別獎
                                System.out.println(number+"//"+awardsnumberArrayList.get(j).number);
                                if(number.equals(awardsnumberArrayList.get(j).number) && awardsnumberArrayList.get(j).awards.equals("特別獎")){
                                    state.setText("1000萬元");
                                    sumgetmoney+=10000000;
                                    blackjack=true;
                                }
                                //特獎
                                else if(number.equals(awardsnumberArrayList.get(j).number) && awardsnumberArrayList.get(j).awards.equals("特獎")){
                                    state.setText("200萬元");
                                    sumgetmoney+=2000000;
                                    blackjack=true;
                                }
                                //頭獎
                                else if(number.equals(awardsnumberArrayList.get(j).number) && awardsnumberArrayList.get(j).awards.equals("頭獎")){
                                    state.setText("20萬元");
                                    sumgetmoney+=200000;
                                    blackjack=true;
                                }
                                //2~6獎
                                else if( awardsnumberArrayList.get(j).awards.equals("頭獎")){
                                    //6獎
                                    String numberbuffer=number.substring(5);
                                    String awardsnumber=awardsnumberArrayList.get(j).number.substring(5);
                                    if(numberbuffer.equals(awardsnumber)){
                                        state.setText("200元");
                                        sumgetmoney+=200;
                                        blackjack=true;
                                    }
                                    //5獎
                                    numberbuffer=number.substring(4);
                                    awardsnumber=awardsnumberArrayList.get(j).number.substring(4);
                                    if(numberbuffer.equals(awardsnumber)){
                                        state.setText("1千元");
                                        sumgetmoney+=1000;
                                        blackjack=true;
                                    }
                                    //4獎
                                    numberbuffer=number.substring(3);
                                    awardsnumber=awardsnumberArrayList.get(j).number.substring(3);
                                    if(numberbuffer.equals(awardsnumber)){
                                        state.setText("4千元");
                                        sumgetmoney+=4000;
                                        blackjack=true;
                                    }
                                    //3獎
                                    numberbuffer=number.substring(2);
                                    awardsnumber=awardsnumberArrayList.get(j).number.substring(2);
                                    if(numberbuffer.equals(awardsnumber)){
                                        state.setText("1萬元");
                                        sumgetmoney+=10000;
                                        blackjack=true;
                                    }
                                    //2獎
                                    numberbuffer=number.substring(1);
                                    awardsnumber=awardsnumberArrayList.get(j).number.substring(1);
                                    if(numberbuffer.equals(awardsnumber)){
                                        state.setText("4萬元");
                                        sumgetmoney+=40000;
                                        blackjack=true;
                                    }
                                }
                                //增開獎
                                else if( awardsnumberArrayList.get(j).awards.equals("增開獎")){
                                    String numberbuffer=number.substring(5);
                                    if(numberbuffer.equals(awardsnumberArrayList.get(j).number)){
                                        state.setText("200元");
                                        sumgetmoney+=200;
                                        blackjack=true;
                                    }
                                }
                                if(blackjack==false){
                                    state.setText("未中獎");
                                }
                            }

                            if(sumgetmoney==0){
                                getmoney.setText("本期無中獎發票");
                            }
                            else{
                                getmoney.setText(String.valueOf(sumgetmoney));
                            }
                            return layout;
                        }
                    };
                    history.setAdapter(adapter);
                }

            }
        });
        minusmonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findawardsminus=false;
                sum=0;
                sumgetmoney=0;
                getmoney.setText("本期無中獎發票");
                invoicedatabuffer.clear();
                n=n-1;
                if(n<0){
                    n=5;
                    year=year-1;
                    nowyear-=1;
                }
                reciprocal.setText(reciprocal(timebuffer,n,String.valueOf(nowyear)));
                String showmonth[]=m[n].split("\\-");
                time.setText(String.valueOf(year)+" 年 "+month[n]+" 月 ");
                for(int i=0;i<invoicedataArrayList.size();i++){
                    String nowday[]=invoicedataArrayList.get(i).date.split("\\/");
                    if(nowday[0].equals(String.valueOf(nowyear))){
                        if((nowday[1].equals(showmonth[0]) || nowday[1].equals(showmonth[1]))){
                            invoicedatabuffer.add(invoicedataArrayList.get(i));
                            sum+=Integer.valueOf(invoicedataArrayList.get(i).money);
                        }
                    }
                }
                Collections.sort(invoicedatabuffer, new invoicesort());
                summoney=findViewById(R.id.summoney);
                summoney.setText(String.valueOf(sum));

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Mysqlqrcode con = new Mysqlqrcode();
                        awardsnumberArrayList= con.getawardsnumber(nowyear+"/"+showmonth[0]);
                        findawardsminus=true;
                    }
                }).start();
                while (findawardsminus!=true){
                    BaseAdapter adapter = new BaseAdapter() {
                        @Override
                        public int getCount() {
                            return invoicedatabuffer.size();
                        }

                        @Override
                        public Object getItem(int i) {
                            return i;
                        }

                        @Override
                        public long getItemId(int i) {
                            return i;
                        }

                        @Override
                        public View getView(int i, View view, ViewGroup viewGroup) {
                            View layout = View.inflate(Oninvoice.this, R.layout.invoicedatalayout, null);
                            TextView code = layout.findViewById(R.id.code);
                            TextView state = layout.findViewById(R.id.state);
                            TextView money = layout.findViewById(R.id.money);
                            TextView time = layout.findViewById(R.id.time);
                            code.setText(invoicedatabuffer.get(i).code);
                            money.setText(invoicedatabuffer.get(i).money);
                            time.setText(invoicedatabuffer.get(i).date);
                            String showmonth[]=m[n].split("\\-");
                            String number=invoicedatabuffer.get(i).code.substring(2);
                            System.out.println("12313:"+awardsnumberArrayList.size());
                            boolean blackjack=false;
                            for(int j=0;j<awardsnumberArrayList.size();j++){
                                number=invoicedatabuffer.get(i).code.substring(2);
                                //特別獎
                                System.out.println(number+"//"+awardsnumberArrayList.get(j).number);
                                if(number.equals(awardsnumberArrayList.get(j).number) && awardsnumberArrayList.get(j).awards.equals("特別獎")){
                                    state.setText("1000萬元");
                                    sumgetmoney+=10000000;
                                    blackjack=true;
                                }
                                //特獎
                                else if(number.equals(awardsnumberArrayList.get(j).number) && awardsnumberArrayList.get(j).awards.equals("特獎")){
                                    state.setText("200萬元");
                                    sumgetmoney+=2000000;
                                    blackjack=true;
                                }
                                //頭獎
                                else if(number.equals(awardsnumberArrayList.get(j).number) && awardsnumberArrayList.get(j).awards.equals("頭獎")){
                                    state.setText("20萬元");
                                    sumgetmoney+=200000;
                                    blackjack=true;
                                }
                                //2~6獎
                                else if( awardsnumberArrayList.get(j).awards.equals("頭獎")){
                                    //6獎
                                    String numberbuffer=number.substring(5);
                                    String awardsnumber=awardsnumberArrayList.get(j).number.substring(5);
                                    if(numberbuffer.equals(awardsnumber)){
                                        state.setText("200元");
                                        sumgetmoney+=200;
                                        blackjack=true;
                                    }
                                    //5獎
                                    numberbuffer=number.substring(4);
                                    awardsnumber=awardsnumberArrayList.get(j).number.substring(4);
                                    if(numberbuffer.equals(awardsnumber)){
                                        state.setText("1千元");
                                        sumgetmoney+=1000;
                                        blackjack=true;
                                    }
                                    //4獎
                                    numberbuffer=number.substring(3);
                                    awardsnumber=awardsnumberArrayList.get(j).number.substring(3);
                                    if(numberbuffer.equals(awardsnumber)){
                                        state.setText("4千元");
                                        sumgetmoney+=4000;
                                        blackjack=true;
                                    }
                                    //3獎
                                    numberbuffer=number.substring(2);
                                    awardsnumber=awardsnumberArrayList.get(j).number.substring(2);
                                    if(numberbuffer.equals(awardsnumber)){
                                        state.setText("1萬元");
                                        sumgetmoney+=10000;
                                        blackjack=true;
                                    }
                                    //2獎
                                    numberbuffer=number.substring(1);
                                    awardsnumber=awardsnumberArrayList.get(j).number.substring(1);
                                    if(numberbuffer.equals(awardsnumber)){
                                        state.setText("4萬元");
                                        sumgetmoney+=40000;
                                        blackjack=true;
                                    }
                                }
                                //增開獎
                                else if( awardsnumberArrayList.get(j).awards.equals("增開獎")){
                                    String numberbuffer=number.substring(5);
                                    if(numberbuffer.equals(awardsnumberArrayList.get(j).number)){
                                        state.setText("200元");
                                        sumgetmoney+=200;
                                        blackjack=true;
                                    }
                                }
                                if(blackjack==false){
                                    state.setText("未中獎");
                                }
                            }

                            if(sumgetmoney==0){
                                getmoney.setText("本期無中獎發票");
                            }
                            else{
                                getmoney.setText(String.valueOf(sumgetmoney));
                            }
                            return layout;
                        }
                    };
                    history.setAdapter(adapter);
                }

            }
        });
    }
}
