package com.example.a1006;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import static android.os.SystemClock.sleep;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link modle#newInstance} factory method to
 * create an instance of this fragment.
 */
public class modle extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public modle() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment tab3.
     */
    // TODO: Rename and change types and number of parameters
    public static modle newInstance(String param1, String param2) {
        modle fragment = new modle();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    int checkediterstate=0;
    TextView titleExTextview,mainSubCategoryExTextview,moneyExTextview;
    TextView titleInTextview,mainSubCategoryInTextview,moneyInTextview;
    TextView add,editer;
    ListView listViewhEx,listViewhIn;
    TextView delete,revise;
    ArrayList<modeexdata> historydataArrayList=new ArrayList<modeexdata>();
    ArrayList<modeexdata> exhistorydataArrayList=new ArrayList<modeexdata>();
    ArrayList<modeexdata> inhistorydataArrayList=new ArrayList<modeexdata>();
    //Global global;
    boolean end=false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.modle, container, false);

        add=view.findViewById(R.id.button3);
        listViewhEx=view.findViewById(R.id.modeexdata);
        listViewhIn=view.findViewById(R.id.modeindata);
        Global global=(Global)getActivity().getApplicationContext();

        new Thread(new Runnable() {
            @Override
            public void run() {
                Mysqlqrcode con = new Mysqlqrcode();
                historydataArrayList= con.getModeExData(global.accountname);
                for(int i=0;i<historydataArrayList.size();i++){
                    if(historydataArrayList.get(i).type.equals("expend")){
                        exhistorydataArrayList.add(historydataArrayList.get(i));
                        global.setExhistorydataArrayList(exhistorydataArrayList);
                    }else{
                        inhistorydataArrayList.add(historydataArrayList.get(i));
                        global.setInhistorydataArrayList(inhistorydataArrayList);
                    }
                }
                sleep(1000);
                end=true;
            }
        }).start();

        while (end!=true){
            BaseAdapter adapterex=new BaseAdapter() {
                @Override
                public int getCount() {
                    return exhistorydataArrayList.size();
                }

                @Override
                public Object getItem(int position) {
                    return position;
                }

                @Override
                public long getItemId(int position) {
                    return position;
                }

                @Override
                public View getView(int position, View convertView, ViewGroup parent) {

                    View layout = View.inflate(getActivity(), R.layout.modeexdatalayout,null);
                    titleExTextview=layout.findViewById(R.id.textView22);
                    mainSubCategoryExTextview=layout.findViewById(R.id.textView23);
                    moneyExTextview=layout.findViewById(R.id.textView24);
                    delete=layout.findViewById(R.id.delete);
                    ImageView imageView=layout.findViewById(R.id.imageView2);
                    revise=layout.findViewById(R.id.revise);
                    if((checkediterstate%2)==1){
                        delete.setVisibility(view.VISIBLE);
                        revise.setVisibility(view.VISIBLE);
                    }
                    else{
                        delete.setVisibility(view.GONE);
                        revise.setVisibility(view.GONE);
                    }
                    for(int i=0;i<exhistorydataArrayList.size();i++){
                        if(exhistorydataArrayList.get(position).maincategoryEx.equals("飲食")){
                            imageView.setImageResource(R.drawable.food);
                        }else if(exhistorydataArrayList.get(position).maincategoryEx.equals("交通")){
                            imageView.setImageResource(R.drawable.transportation);
                        }else if(exhistorydataArrayList.get(position).maincategoryEx.equals("娛樂")){
                            imageView.setImageResource(R.drawable.clapperboard);
                        }else if(exhistorydataArrayList.get(position).maincategoryEx.equals("購物")){
                            imageView.setImageResource(R.drawable.shopping);
                        }else if(exhistorydataArrayList.get(position).maincategoryEx.equals("醫療")){
                            imageView.setImageResource(R.drawable.medicaltreatment);
                        }else if(exhistorydataArrayList.get(position).maincategoryEx.equals("教育")){
                            imageView.setImageResource(R.drawable.educate);
                        }else if(exhistorydataArrayList.get(position).maincategoryEx.equals("家具")){
                            imageView.setImageResource(R.drawable.furniture);
                        }
                    }
                    titleExTextview.setText(exhistorydataArrayList.get(position).titleEx);
                    mainSubCategoryExTextview.setText(exhistorydataArrayList.get(position).maincategoryEx);
                    moneyExTextview.setText(exhistorydataArrayList.get(position).moneyEx+"$");
                    return layout;
                }
            };

            listViewhEx.setAdapter(adapterex);
            BaseAdapter adapterin=new BaseAdapter() {

                @Override
                public int getCount() {
                    return inhistorydataArrayList.size();
                }

                @Override
                public Object getItem(int position) {
                    return position;
                }

                @Override
                public long getItemId(int position) {
                    return position;
                }

                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View layout = View.inflate(getActivity(), R.layout.modeindatalayout,null);
                    titleInTextview=layout.findViewById(R.id.textView25);
                    mainSubCategoryInTextview=layout.findViewById(R.id.textView26);
                    moneyInTextview=layout.findViewById(R.id.textView27);
                    delete=layout.findViewById(R.id.delete);
                    revise=layout.findViewById(R.id.revise);
                    if((checkediterstate%2)==1){
                        delete.setVisibility(view.VISIBLE);
                        revise.setVisibility(view.VISIBLE);
                    }
                    else{
                        delete.setVisibility(view.GONE);
                        revise.setVisibility(view.GONE);
                    }

                    titleInTextview.setText(inhistorydataArrayList.get(position).titleEx);
                    mainSubCategoryInTextview.setText(inhistorydataArrayList.get(position).maincategoryEx);
                    moneyInTextview.setText(inhistorydataArrayList.get(position).moneyEx+"$");
                    return layout;
                }
            };
            listViewhIn.setAdapter(adapterin);
        }
        listViewhEx.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("典籍支出"+" // "+position);
                global.setPositionEX(String.valueOf(position));
                Intent intent = new Intent(getActivity(), tab.class);
                intent.putExtra("postion",1);
                startActivity(intent);
            }
        });
        listViewhIn.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("典籍收入"+" // "+position);
                global.setPositionIN(String.valueOf(position));
                Intent intent = new Intent(getActivity(), tab.class);
                intent.putExtra("postion",2);
                startActivity(intent);

            }
        });
        editer=view.findViewById(R.id.button2);

        editer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkediterstate++;
                System.out.println(checkediterstate);
                BaseAdapter adapterex=new BaseAdapter() {
                    @Override
                    public int getCount() {
                        return exhistorydataArrayList.size();
                    }

                    @Override
                    public Object getItem(int position) {
                        return position;
                    }

                    @Override
                    public long getItemId(int position) {
                        return position;
                    }

                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View layout = View.inflate(getActivity(), R.layout.modeexdatalayout,null);
                        titleExTextview=layout.findViewById(R.id.textView22);
                        mainSubCategoryExTextview=layout.findViewById(R.id.textView23);
                        moneyExTextview=layout.findViewById(R.id.textView24);
                        ImageView imageView=layout.findViewById(R.id.imageView2);
                        delete=layout.findViewById(R.id.delete);
                        revise=layout.findViewById(R.id.revise);
                        if((checkediterstate%2)==1){
                            delete.setVisibility(view.VISIBLE);
                            revise.setVisibility(view.VISIBLE);
                        }
                        else{
                            delete.setVisibility(view.GONE);
                            revise.setVisibility(view.GONE);
                        }

                        for(int i=0;i<exhistorydataArrayList.size();i++){
                            if(exhistorydataArrayList.get(position).maincategoryEx.equals("飲食")){
                                imageView.setImageResource(R.drawable.food);
                            }else if(exhistorydataArrayList.get(position).maincategoryEx.equals("交通")){
                                imageView.setImageResource(R.drawable.transportation);
                            }else if(exhistorydataArrayList.get(position).maincategoryEx.equals("娛樂")){
                                imageView.setImageResource(R.drawable.clapperboard);
                            }else if(exhistorydataArrayList.get(position).maincategoryEx.equals("購物")){
                                imageView.setImageResource(R.drawable.shopping);
                            }else if(exhistorydataArrayList.get(position).maincategoryEx.equals("醫療")){
                                imageView.setImageResource(R.drawable.medicaltreatment);
                            }else if(exhistorydataArrayList.get(position).maincategoryEx.equals("教育")){
                                imageView.setImageResource(R.drawable.educate);
                            }else if(exhistorydataArrayList.get(position).maincategoryEx.equals("家具")){
                                imageView.setImageResource(R.drawable.furniture);
                            }
                        }

                        delete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Mysqlqrcode con = new Mysqlqrcode();
                                        con.deletemode(exhistorydataArrayList.get(position).titleEx,global.accountname);
                                        exhistorydataArrayList.remove(position);
                                    }
                                }).start();
                                Intent intent = new Intent(getActivity(), tab.class);
                                int n=0;
                                intent.putExtra("postion",n);
                                startActivity(intent);
                            }
                        });
                        revise.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                global.setTitleEX(exhistorydataArrayList.get(position).titleEx);
                                Intent intent = new Intent(getActivity(), expendmoderevies.class);
                                int n=0;
                                intent.putExtra("postion",n);
                                startActivity(intent);
                            }
                        });
                        titleExTextview.setText(exhistorydataArrayList.get(position).titleEx);
                        mainSubCategoryExTextview.setText(exhistorydataArrayList.get(position).maincategoryEx);
                        moneyExTextview.setText(exhistorydataArrayList.get(position).moneyEx+"$");
                        return layout;
                    }
                };
                listViewhEx.setAdapter(adapterex);
                BaseAdapter adapterin=new BaseAdapter() {
                    @Override
                    public int getCount() {
                        return inhistorydataArrayList.size();
                    }

                    @Override
                    public Object getItem(int position) {
                        return position;
                    }

                    @Override
                    public long getItemId(int position) {
                        return position;
                    }

                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View layout = View.inflate(getActivity(), R.layout.modeindatalayout,null);
                        titleInTextview=layout.findViewById(R.id.textView25);
                        mainSubCategoryInTextview=layout.findViewById(R.id.textView26);
                        moneyInTextview=layout.findViewById(R.id.textView27);
                        delete=layout.findViewById(R.id.delete);
                        revise=layout.findViewById(R.id.revise);
                        if((checkediterstate%2)==1){
                            delete.setVisibility(view.VISIBLE);
                            revise.setVisibility(view.VISIBLE);
                        }
                        else{
                            delete.setVisibility(view.GONE);
                            revise.setVisibility(view.GONE);
                        }
                        delete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Mysqlqrcode con = new Mysqlqrcode();
                                        con.deletemode(inhistorydataArrayList.get(position).titleEx,global.accountname);
                                        inhistorydataArrayList.remove(position);
                                    }
                                }).start();
                                Intent intent = new Intent(getActivity(), tab.class);
                                int n=0;
                                intent.putExtra("postion",n);
                                startActivity(intent);
                            }
                        });
                        revise.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                global.setTitleIN(inhistorydataArrayList.get(position).titleEx);
                                Intent intent = new Intent(getActivity(), incomemoderevies.class);
                                int n=0;
                                intent.putExtra("postion",n);
                                startActivity(intent);
                            }
                        });
                        titleInTextview.setText(inhistorydataArrayList.get(position).titleEx);
                        mainSubCategoryInTextview.setText(inhistorydataArrayList.get(position).maincategoryEx);
                        moneyInTextview.setText(inhistorydataArrayList.get(position).moneyEx+"$");
                        return layout;
                    }
                };
                listViewhIn.setAdapter(adapterin);
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), tabmode.class);
                int n=0;
                intent.putExtra("postion",n);
                startActivity(intent);
            }
        });

        return  view;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode==KeyEvent.KEYCODE_BACK){
            //global.setPositionEX(null);
            //global.setPositionIN(null);
            Intent intent = new Intent(getActivity(), account.class);
            startActivity(intent);
        }
        return false;
    }
    public void refresh() {

        onCreate(null);

    }
}