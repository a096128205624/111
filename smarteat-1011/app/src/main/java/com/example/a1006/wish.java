package com.example.a1006;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Calendar;

public class wish extends AppCompatActivity  {
    FloatingActionButton fab,fab2;
    ListView wishlsit;
    TextView rank,goal,endtime,price,percentage,start,saveprice,del,modify;
    ArrayList<wishdata> wishdataArrayList=new ArrayList<wishdata>();
    boolean end=false,a=false;
    int checkediterstate=0,order=0;
    String wishname;
    Bundle bundle=new Bundle();
    Calendar cal =Calendar.getInstance();
    int year=cal.get(Calendar.YEAR);
    int month=cal.get(Calendar.MONTH);
    int day=cal.get(Calendar.DATE);
    int hour=cal.get(Calendar.HOUR_OF_DAY);
    int min=cal.get(Calendar.MINUTE);
    String date = year + "/" + (month+1) + "/" + day;

    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wish);
        View v= this.getWindow().getDecorView().findViewById(android.R.id.content);
        Global global=(Global)getApplicationContext();
        fab = (FloatingActionButton) findViewById(R.id.fab);
        wishlsit =findViewById(R.id.modeexdata1);
        String time = (hour) + ":" + String.format("%02d", min);
        registerForContextMenu(wishlsit);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Mysqlqrcode con = new Mysqlqrcode();
                wishdataArrayList = con.getwishdata(global.accountname);
                end=true;
                System.out.println("陣列:"+wishdataArrayList.size());
            }
        }).start();

        int i=0;
        while (end!=true){
            BaseAdapter adapterex1=new BaseAdapter() {
                @Override
                public int getCount() {
                    return wishdataArrayList.size();
                }

                @Override
                public Object getItem(int position) {
                    return position;
                }

                @Override
                public long getItemId(int position) {
                    return position;
                }

                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View layout = View.inflate(wish.this, R.layout.wishdatalayout, null);

                    rank=layout.findViewById(R.id.rank);
                    goal = layout.findViewById(R.id.goal);
                    endtime = layout.findViewById(R.id.endtime);
                    price = layout.findViewById(R.id.price);
                    percentage = layout.findViewById(R.id.percentage);
                    start = layout.findViewById(R.id.start);
                    saveprice = layout.findViewById(R.id.saveprice);
                    rank.setText(wishdataArrayList.get(position).rank+"");
                    goal.setText(wishdataArrayList.get(position).name);
                    endtime.setText(wishdataArrayList.get(position).end);
                    price.setText(wishdataArrayList.get(position).price+"$");
                    start.setText(wishdataArrayList.get(position).start);
                    saveprice.setText(wishdataArrayList.get(position).saveprice+"$");
                    double percentage1 =((double)wishdataArrayList.get(position).saveprice/(double)wishdataArrayList.get(position).price)*100;
                    double roundp = Math.round(percentage1*100.0)/100.0;
                    if(roundp>=100){
                        percentage.setText(100+"%");
                    }
                    else {
                        percentage.setText(roundp+"%");
                    }

                    return layout;
                }
            };
            wishlsit.setAdapter(adapterex1);
            AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(wish.this);
                    builder.setTitle("請輸入存入金額");
                    View view1 = LayoutInflater.from(getApplication()).inflate(R.layout.addview,null);
                    final EditText input =(EditText) view1.findViewById(R.id.editText1);
                    builder.setView(view1);
                    builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            int pay;
                            int savemoney=Integer.parseInt(input.getText().toString());
                            if(wishdataArrayList.get(position).price<=savemoney){
                                pay=wishdataArrayList.get(position).price;
                            }
                            else{
                                pay = savemoney+(wishdataArrayList.get(position).saveprice);
                            }
                            String gaol =String.valueOf(wishdataArrayList.get(position).name);
                            if (pay!=0){
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Mysqlqrcode con = new Mysqlqrcode();
                                        con.insertCurrentlymoney(global.accountname,gaol,pay);
                                        if(wishdataArrayList.get(position).price<=savemoney){
                                            int buffer=Integer.valueOf(wishdataArrayList.get(position).price)-Integer.valueOf(wishdataArrayList.get(position).saveprice);
                                            con.insertExpendData("",String.valueOf(buffer),"願望清單",gaol,date,time,global.accountname);
                                        }
                                        else{
                                            con.insertExpendData("",String.valueOf(savemoney),"願望清單",gaol,date,time,global.accountname);
                                        }
                                        a=true;
                                    }
                                }).start();
                                if (a!=true){
                                    Toast.makeText(getApplication(),"新增成功",Toast.LENGTH_LONG).show();
                                    input.setText("");
                                    wish.this.recreate(); } //從新創建

                            }
                        }
                    });
                    builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();
                }
            };
            wishlsit.setOnItemClickListener(onItemClickListener);
        }


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(wish.this, addwish.class));

            }
        });




    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu,view,menuInfo);
        AdapterView.AdapterContextMenuInfo info=(AdapterView.AdapterContextMenuInfo)menuInfo;
        wishname=wishdataArrayList.get(info.position).name;
        order=wishdataArrayList.get(info.position).rank;
        bundle.putInt("rank",(wishdataArrayList.get(info.position).rank));
        bundle.putString("gaol",String.valueOf(wishdataArrayList.get(info.position).name));
        bundle.putString("price",String.valueOf(wishdataArrayList.get(info.position).price));
        bundle.putString("start",String.valueOf(wishdataArrayList.get(info.position).start));
        bundle.putString("end",String.valueOf(wishdataArrayList.get(info.position).end));
        bundle.putString("saveprice",String.valueOf(wishdataArrayList.get(info.position).saveprice));
        menu.add(0,0,0,"刪除");
        menu.add(0,1,0,"修改");
    }
    @Override
    public boolean onContextItemSelected(MenuItem item){
        Global global=(Global)getApplicationContext();
        switch (item.getItemId()){
            case 0:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Mysqlqrcode con = new Mysqlqrcode();
                        int q=con.deletewish(order,wishname,global.accountname);
                        if (q==0){
                            Intent intent = new Intent(getApplication(), wish.class);
                            startActivity(intent);
                        }
                    }
                }).start();
                break;
            case 1:
                Intent intent=new Intent();
                intent.setClass(wish.this, moidfywish.class);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
        }
        return true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode==KeyEvent.KEYCODE_BACK){
            Intent intent = new Intent(wish.this, MainActivity.class);
            startActivity(intent);
        }
        return false;
    }
}
