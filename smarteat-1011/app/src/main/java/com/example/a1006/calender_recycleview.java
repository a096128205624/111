package com.example.a1006;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class calender_recycleview extends RecyclerView.Adapter {
    calendar calendar;
    ArrayList<historydata> historydata=new ArrayList<historydata>();


    public calender_recycleview(calendar context, ArrayList<historydata> historydata ){
        this.calendar = context;
        this.historydata = historydata;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(calendar).inflate(R.layout.account_recyclerview_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder holder1 = (ViewHolder) holder;
        holder1.item1.setText(historydata.get(position).maincategory);
        holder1.item2.setText(historydata.get(position).date);
        holder1.money.setText(String.valueOf(historydata.get(position).money)+" $");
        if(historydata.get(position).type.equals("expend")){
            holder1.money.setTextColor(Color.rgb(243,50, 73));
            if (historydata.get(position).maincategory.equals("飲食")) {
                holder1.image.setImageResource(R.drawable.food);
            } else if (historydata.get(position).maincategory.equals("交通")) {
                holder1.image.setImageResource(R.drawable.transportation);
            } else if (historydata.get(position).maincategory.equals("娛樂")) {
                holder1.image.setImageResource(R.drawable.clapperboard);
            } else if (historydata.get(position).maincategory.equals("購物")) {
                holder1.image.setImageResource(R.drawable.shopping);
            } else if (historydata.get(position).maincategory.equals("醫療")) {
                holder1.image.setImageResource(R.drawable.medicaltreatment);
            } else if (historydata.get(position).maincategory.equals("教育")) {
                holder1.image.setImageResource(R.drawable.educate);
            } else if (historydata.get(position).maincategory.equals("家具")) {
                holder1.image.setImageResource(R.drawable.furniture);
            }
        }
        else{
            holder1.image.setImageResource(R.drawable.profit);
            holder1.money.setTextColor(Color.rgb(46,139, 87));
        }



    }

    @Override
    public int getItemCount() {
        return historydata.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView item1, item2, money;
        ImageView image;
        LinearLayout linearlayout;

        public ViewHolder(View itemView) {
            super(itemView);
            item1 = itemView.findViewById(R.id.item1);
            item2 = itemView.findViewById(R.id.item2);
            money = itemView.findViewById(R.id.money);
            image = itemView.findViewById(R.id.image);
            linearlayout=itemView.findViewById(R.id.linearlayout);
            linearlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if(historydata.get(getAdapterPosition()).type.equals("expend")){
                        Intent intent = new Intent();
                        intent.setClass(calendar.getApplication(), editexpendrecord.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("subcategory",historydata.get(getAdapterPosition()).maincategory.toString());
                        System.out.println(String.valueOf(historydata.get(getAdapterPosition()).money));
                        bundle.putString("money", String.valueOf(historydata.get(getAdapterPosition()).money));
                        bundle.putString("time", historydata.get(getAdapterPosition()).time.toString());
                        bundle.putString("date", historydata.get(getAdapterPosition()).date.toString());
                        bundle.putString("position","2");
                        intent.putExtras(bundle);
                        calendar.startActivity(intent);
                    }
                    else if(historydata.get(getAdapterPosition()).type.equals("income")){
                        Intent intent = new Intent();
                        intent.setClass(calendar.getApplication(), editeincomerecord.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("subcategory",historydata.get(getAdapterPosition()).maincategory.toString());
                        System.out.println(String.valueOf(historydata.get(getAdapterPosition()).money));
                        bundle.putString("money", String.valueOf(historydata.get(getAdapterPosition()).money));
                        bundle.putString("time", historydata.get(getAdapterPosition()).time.toString());
                        bundle.putString("date", historydata.get(getAdapterPosition()).date.toString());
                        bundle.putString("position","2");
                        intent.putExtras(bundle);
                        calendar.startActivity(intent);
                    }
                }
            });

        }
    }
}
