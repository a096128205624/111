package com.example.a1006;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.provider.MediaStore;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class editeincomerecord extends AppCompatActivity {
    TextView tdate,ttime, enter,tdelete;
    EditText money,detail;
    String moneybuffer,detailbuffer,Maincategory,position;
    Spinner spinner;
    boolean end=false;
    Button add,delete;
    ImageView bt1, bt2;
    ArrayList<String> category=new ArrayList<String>();
    DatePickerDialog.OnDateSetListener dateSetListener;
    TimePickerDialog.OnTimeSetListener timeSetListener;
    @RequiresApi(api = Build.VERSION_CODES.M)
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.editeincomerecord);
        View view1=getLayoutInflater().inflate(R.layout.addview,null);
        ttime=findViewById(R.id.time);
        spinner = findViewById(R.id.spinner3);
        tdate=findViewById(R.id.date);
        add=findViewById(R.id.button7);
        delete=findViewById(R.id.button8);
        Calendar cal=Calendar.getInstance();
        int year=cal.get(Calendar.YEAR);
        int month=cal.get(Calendar.MONTH);
        int day=cal.get(Calendar.DATE);
        int hour=cal.get(Calendar.HOUR_OF_DAY);
        int min=cal.get(Calendar.MINUTE);
        enter=findViewById(R.id.button4);
        money=findViewById(R.id.money);
        detail=findViewById(R.id.detail);
        bt1 = findViewById(R.id.bt1);
        bt2 = findViewById(R.id.bt2);
        Global global=(Global)this.getApplicationContext();
        ArrayList<modeexdata> inhistorydataArrayList=global.getInhistorydataArrayList();

        new Thread(new Runnable() {
            @Override
            public void run() {
                Mysqlqrcode con = new Mysqlqrcode();
                category= con.getIncomecategory(global.accountname);
                System.out.println("收入"+category.size());
                end=true;
            }
        }).start();
        while (end!=true){
            ArrayAdapter<String> lunchList = new ArrayAdapter<>(editeincomerecord.this, android.R.layout.simple_spinner_dropdown_item, category);
            lunchList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(lunchList);
        }
        if (global.getPositionIN()!=null){
            System.out.println(inhistorydataArrayList.get(Integer.parseInt(global.getPositionIN())).moneyEx);
            money.setText(inhistorydataArrayList.get(Integer.parseInt(global.getPositionIN())).moneyEx);
            for(int i=0;i<category.size();i++){
                if(inhistorydataArrayList.get(Integer.parseInt(global.getPositionIN())).maincategoryEx.equals(category.get(i))){
                    spinner.setSelection(i);
                }
            }
        }

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(editeincomerecord.this);
                alertDialog.setTitle("新增分類");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newstring = editText.getText().toString();
                        for(int i=0;i<category.size();i++){
                            System.out.println(category.get(i));
                            if(newstring.equals(category.get(i))){
                                Toast.makeText(editeincomerecord.this,"此類別已存在",Toast.LENGTH_LONG).show();
                                editText.setText("");
                                return;
                            }
                        }
                        if(!newstring.equals("")){
                            category.add((newstring));
                            spinner.setSelection(category.size()-1);
                            Toast.makeText(editeincomerecord.this,"新增成功",Toast.LENGTH_LONG).show();
                            editText.setText("");
                        }
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(editeincomerecord.this);
                alertDialog.setTitle("刪除分類");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        boolean check=true;
                        String newstring = editText.getText().toString();
                        for(int i=0;i< category.size();i++){
                            if(newstring.equals(category.get(i))){
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Mysqlqrcode con = new Mysqlqrcode();
                                        con.deletecategory(newstring,global.accountname);
                                    }
                                }).start();
                                check=false;
                                category.remove((i));
                                if(i==0){
                                    spinner.setSelection(1);
                                }else {
                                    spinner.setSelection(0);
                                }
                                Toast.makeText(editeincomerecord.this,"刪除成功",Toast.LENGTH_LONG).show();
                                editText.setText("");
                            }
                        }
                        if(check==true){
                            Toast.makeText(editeincomerecord.this,"查無此類別名稱",Toast.LENGTH_LONG).show();
                            editText.setText("");
                        }
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Bundle bundle = this.getIntent().getExtras();
        String datasubcategory = bundle.getString("subcategory");
        String datamoney = bundle.getString("money");
        String datatime = bundle.getString("time");
        String datadate = bundle.getString("date");
        position=bundle.getString("position");
        for (int i=0;i<global.historydata.size();i++){
            if(global.historydata.get(i).maincategory.equals(datasubcategory) &&
                    global.historydata.get(i).date.equals(datadate) && global.historydata.get(i).time.equals(datatime) &&
                    global.historydata.get(i).money==Integer.valueOf(datamoney) ){
                System.out.println("now:"+global.historydata.get(i).time);
                tdate.setText(global.historydata.get(i).date);
                ttime.setText(global.historydata.get(i).time);
                money.setText(String.valueOf(global.historydata.get(i).money));
                for(int j=0;j<category.size();j++){
                    if (category.get(j).equals(global.historydata.get(i).maincategory)){
                        spinner.setSelection(j);
                    }
                }
                detail.setText(global.historydata.get(i).productname);
            }
        }
        //日期選擇
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view1.getParent() != null) {
                    ((ViewGroup)view1.getParent()).removeView(view1); // <- fix
                }
                Calendar cal=Calendar.getInstance();
                int year=cal.get(Calendar.YEAR);
                int month=cal.get(Calendar.MONTH);
                int day=cal.get(Calendar.DATE);


                dateSetListener=new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        String date = year + "/" + (month+1) + "/" + day;
                        tdate.setText(date);
                    }
                };

                DatePickerDialog dialog=new DatePickerDialog(editeincomerecord.this, dateSetListener, year,month,day);
                dialog.show();


            }
        });
        //時間選擇
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal=Calendar.getInstance();
                int hour=cal.get(Calendar.HOUR_OF_DAY);
                int min=cal.get(Calendar.MINUTE);

                timeSetListener=new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int min) {
                        Log.d("onTimeSet: hh/mm: ", +hour + "/" + min);
                        String time = hour + ":" + String.format("%02d", min);
                        ttime.setText(time);
                    }
                };

                TimePickerDialog dialog=new TimePickerDialog(
                        editeincomerecord.this,
                        timeSetListener,
                        hour,min, DateFormat.is24HourFormat(editeincomerecord.this)
                );
                dialog.show();

            }

        });
        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String datebuffer=tdate.getText().toString();
                String timebuffer=ttime.getText().toString();
                moneybuffer=money.getText().toString();
                detailbuffer=detail.getText().toString();
                Maincategory=(String) spinner.getSelectedItem();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Mysqlqrcode con = new Mysqlqrcode();
                        con.deleteincomedata(datadate,datatime,datamoney,global.accountname);
                        con.insertIncomeData(moneybuffer,Maincategory,detailbuffer,datebuffer,timebuffer,global.accountname);
                        Looper.prepare();
                        Toast toast=Toast.makeText(editeincomerecord.this, "修改成功", Toast.LENGTH_LONG);
                        toast.show();
                        Intent intent = new Intent(editeincomerecord.this, account.class);
                        startActivity(intent);
                        Looper.loop();
                    }
                }).start();


            }
        });
        tdelete=findViewById(R.id.delete);
        tdelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Mysqlqrcode con = new Mysqlqrcode();
                        con.deleteincomedata(datadate,datatime,datamoney,global.accountname);
                        Looper.prepare();
                        Toast toast=Toast.makeText(editeincomerecord.this, "刪除成功", Toast.LENGTH_LONG);
                        toast.show();
                        Intent intent = new Intent(editeincomerecord.this, account.class);
                        startActivity(intent);
                        Looper.loop();
                    }
                }).start();
            }
        });

    }
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode==KeyEvent.KEYCODE_BACK){
            if(position.equals("2")){
                Intent intent = new Intent(editeincomerecord.this, calendar.class);
                startActivity(intent);
            }else{
                Intent intent = new Intent(editeincomerecord.this, account.class);
                startActivity(intent);
            }

        }
        return false;
    }
}