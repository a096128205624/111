package com.example.a1006;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class login extends AppCompatActivity {
    EditText Eaccount,Epassword;
    TextView login,sing, confrim, forgetpassword;
    String account,password;
    boolean check=false;
    AccountValue av;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loging);
        sing=findViewById(R.id.sing);
        login=findViewById(R.id.login);
        Eaccount=findViewById(R.id.oldpassword);
        Epassword=findViewById(R.id.Password);
        confrim=findViewById(R.id.confirm);
        forgetpassword=findViewById(R.id.forgetpassword);

        forgetpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(login.this, forgetpassword.class);
                startActivity(intent);
                overridePendingTransition(R.animator.slide_in_right, R.animator.slide_out_left);
            }
        });

        confrim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(login.this, confirmemail.class);
                Bundle bundle = new Bundle();
                bundle.putString("email","");
                bundle.putString("account","");
                intent.putExtras(bundle);
                startActivity(intent);
                overridePendingTransition(R.animator.slide_in_right, R.animator.slide_out_left);
            }
        });
        sing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(com.example.a1006.login.this, com.example.a1006.sing.class);
                startActivity(intent);
                overridePendingTransition(R.animator.slide_in_right, R.animator.slide_out_left);
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                account=Eaccount.getText().toString();
                password=Epassword.getText().toString();
                Mysqlqrcode mysqlqrcode=new Mysqlqrcode();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        av=mysqlqrcode.getAccount(account);
                        System.out.println(av.account+av.password);

                    }
                }).start();
                while (av==null){//等待搜尋帳號
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if(av.account.equals(account) && av.password.equals(password) && av.state.equals("OK")){
                    Global global=(Global)getApplicationContext();
                    global.setAccountname(account);
                    global.setPass(password);
                    Intent intent = new Intent(com.example.a1006.login.this, MainActivity.class);
                    startActivity(intent);
                }
                else if(!av.account.equals(account)){
                    Toast toast = Toast.makeText(com.example.a1006.login.this, "無此帳號", Toast.LENGTH_LONG);
                    toast.show();
                }
                else if(!av.password.equals(password)){
                    Toast toast = Toast.makeText(com.example.a1006.login.this, "密碼錯誤", Toast.LENGTH_LONG);
                    toast.show();
                }
                else if(!av.state.equals("OK")){
                    Toast toast = Toast.makeText(com.example.a1006.login.this, "帳號未開通", Toast.LENGTH_LONG);
                    toast.show();
                }
                overridePendingTransition(R.animator.slide_in_right, R.animator.slide_out_left);
                finish();
            }
        });
    }
}
