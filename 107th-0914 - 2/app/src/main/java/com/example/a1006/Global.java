package com.example.a1006;

import android.app.Application;

import java.util.ArrayList;

public class Global extends Application {
    String code,Uniformnumbers,date,accountname,detail,titleEX,titleIN,positionEX,positionIN,password;
    int money;
    ArrayList<historydata> historydata;
    ArrayList<modeexdata> modeexdataEX;
    ArrayList<modeexdata> modeexdataIN;

    public void setHistorydata(ArrayList<historydata> historydata) {
        this.historydata = historydata;
    }
    public void setExhistorydataArrayList(ArrayList<modeexdata> modeexdataEX) {
        this.modeexdataEX = modeexdataEX;
    }

    public void setInhistorydataArrayList(ArrayList<modeexdata> modeexdataIN) {
        this.modeexdataIN = modeexdataIN;
    }
    public void setTitleIN(String titleIN) {
        this.titleIN = titleIN;
    }
    public String getTitleIN() {
        return titleIN;
    }

    public void setPass(String password){
        this.password=password;
    }
    public String getPass(){
        return this.password;
    }
    public void setTitleEX(String titleEX) {
        this.titleEX = titleEX;
    }

    public String getTitleEX() {
        return titleEX;
    }

    public ArrayList<historydata> getHistorydata() {
        return historydata;
    }

    public ArrayList<modeexdata> getExhistorydataArrayList() {
        return modeexdataEX;
    }

    public ArrayList<modeexdata> getInhistorydataArrayList() {
        return modeexdataIN;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setPositionEX(String positionEX) {
        this.positionEX = positionEX;
    }

    public String getPositionEX() {
        return positionEX;
    }

    public void setPositionIN(String positionIN) {
        this.positionIN = positionIN;
    }

    public String getPositionIN() {
        return positionIN;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDetail() {
        return detail;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setUniformnumbers(String Uniformnumbers) {
        this.Uniformnumbers = Uniformnumbers;
    }

    public String getUniformnumbers() {
        return Uniformnumbers;
    }

    public void setAccountname(String accountname) {
        this.accountname = accountname;
    }

    public String getAccountname() {
        return accountname;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getMoney() {
        return money;
    }


}
