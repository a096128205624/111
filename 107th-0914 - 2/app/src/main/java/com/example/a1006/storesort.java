package com.example.a1006;

import java.util.Comparator;

class storesort implements Comparator<store>
{
    @Override
    public int compare(store store, store t1) {
        return (int) (store.sum-t1.sum);
    }
}