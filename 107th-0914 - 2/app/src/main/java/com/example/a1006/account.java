package com.example.a1006;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.ui.AppBarConfiguration;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

public class account extends AppCompatActivity {
    Button bincome,bexpend;
    private DrawerLayout drawerLayout;
    private AppBarConfiguration mAppBarConfiguration;
    TextView navaccount,tdate,assets,Tincomesum,Texpendsum;
    boolean end=false;
    ListView listViewhistroy;
    ArrayList<historydata> historydataArrayList=new ArrayList<historydata>();
    Calendar c = Calendar.getInstance();
    int nyear = c.get(Calendar.YEAR);
    int nmonth = c.get(Calendar.MONTH)+1;
    int nday = c.get(Calendar.DAY_OF_MONTH);
    int sum=0,expnedsum=0,incomesum=0;
    String assetsvalue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account);
        Global globalaccountname=(Global)getApplicationContext();
        String accountname=globalaccountname.getAccountname();
        assets=findViewById(R.id.assets);
        assets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(account.this);
                final EditText editText = new EditText(account.this); //final一個editText
                builder.setView(editText);
                builder.setTitle("請輸入每月基本金額 目前是:"+assetsvalue+"$");
                builder.setPositiveButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if(!editText.getText().toString().equals("")){
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Mysqlqrcode con = new Mysqlqrcode();
                                    con.insertAssets(accountname,editText.getText().toString());
                                }
                            }).start();
                            assetsvalue=editText.getText().toString();
                            if(sum<0){
                                assets.setText(String.valueOf((Integer.valueOf(assetsvalue))+sum)+"$");
                            }
                            else {
                                assets.setText(String.valueOf((Integer.valueOf(assetsvalue))-sum)+"$");
                            }
                        }
                    }
                });
                builder.setNegativeButton("確定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                builder.create().show();
            }
        });
//nav--------------------------------------------------------------------------------------------
//        Toolbar toolbar = findViewById(R.id.toolbar);
//        toolbar.setTitleTextColor(Color.WHITE);
//        setSupportActionBar(toolbar);
//        drawerLayout=findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
//        drawerLayout.addDrawerListener(toggle);
//        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        navaccount=headerView.findViewById(R.id.accountname);
        navaccount.setText(accountname);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.itemfoodcommend:
                        Intent intent = new Intent(account.this, foodcommend.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("account",accountname);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        break;
                    case R.id.oldpassword:
                        intent = new Intent(account.this, account.class);
                        bundle = new Bundle();
                        bundle.putString("account",accountname);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        break;
                    case R.id.itemoninvoice:
                        intent = new Intent(account.this, Oninvoice.class);
                        bundle = new Bundle();
                        bundle.putString("account",accountname);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        break;
                }

                menuItem.setChecked(true);//點選了把它設為選中狀態
                return true;
            }
        });
//history---------------------------------------------------------------------------------------------
        listViewhistroy=findViewById(R.id.history);
        Texpendsum=findViewById(R.id.totalexpendvalue);
        Tincomesum=findViewById(R.id.totalincomevalue);
        ArrayList<historydata> historydatabuffer=new ArrayList<historydata>();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Mysqlqrcode con = new Mysqlqrcode();
                historydataArrayList= con.gethistoryData(accountname);
                Global global=(Global) getApplicationContext();
                global.setHistorydata(historydataArrayList);
                for(int i=0;i<historydataArrayList.size();i++){
                    String nowday[]=historydataArrayList.get(i).date.split("\\/");
                    if(nowday[0].equals(String.valueOf(nyear)) && nowday[1].equals(String.valueOf(nmonth))&& nowday[2].equals(String.valueOf(nday))){
                        historydatabuffer.add(historydataArrayList.get(i));
                    }
                    if(nowday[0].equals(String.valueOf(nyear)) && nowday[1].equals(String.valueOf(nmonth))){
                        if(historydataArrayList.get(i).type.equals("expend")){
                            sum=sum-Integer.valueOf(historydataArrayList.get(i).money);
                            expnedsum=expnedsum+Integer.valueOf(historydataArrayList.get(i).money);
                        }
                        else if(historydataArrayList.get(i).type.equals("income")){
                            sum=sum+Integer.valueOf(historydataArrayList.get(i).money);
                            incomesum=incomesum+Integer.valueOf(historydataArrayList.get(i).money);
                        }
                    }
                }
                assetsvalue=con.getassets(accountname);
                if(sum<0){
                 //   assets.setText(String.valueOf(Integer.valueOf(assetsvalue)+sum));
                }
                else {
                 //   assets.setText(String.valueOf(Integer.valueOf(assetsvalue)-sum));
                }
                Texpendsum.setText(String.valueOf(expnedsum));
                Tincomesum.setText(String.valueOf(incomesum));
                end=true;
                Collections.sort(historydatabuffer, new historysort());
            }
        }).start();
        while (end!=true){
            BaseAdapter adapter=new BaseAdapter() {
                @Override
                public int getCount() {
                    return historydatabuffer.size();
                }

                @Override
                public Object getItem(int i) {
                    return i;
                }

                @Override
                public long getItemId(int i) {
                    return i;
                }

                @Override
                public View getView(int i, View view, ViewGroup viewGroup) {
                    View layout=View.inflate(account.this,R.layout.expenddatalayout,null);
                    TextView subcategory=layout.findViewById(R.id.subcategory);
                    TextView money=layout.findViewById(R.id.money);
                    subcategory.setText(historydatabuffer.get(i).maincategory);
                    if(historydatabuffer.get(i).type.equals("expend")){
                        money.setText(" - "+String.valueOf(historydatabuffer.get(i).money)+" $");
                        money.setTextColor(Color.RED);
                    }
                    else if(historydatabuffer.get(i).type.equals("income")){
                        money.setText(" + "+String.valueOf(historydatabuffer.get(i).money)+" $");
                        money.setTextColor(Color.GREEN);
                    }
                    return layout;

                }
            };
            listViewhistroy.setAdapter(adapter);



        }
//time-----------------------------------------------------------------------------------------------
        tdate=findViewById(R.id.date);
        tdate.setText(nyear+"/"+nmonth+"/"+nday);
        tdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                historydatabuffer.clear();
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                new DatePickerDialog(account.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        tdate.setText(year+"/"+(month+1)+"/"+day);
                        nyear=year;
                        nmonth=month+1;
                        nday=day;
                        sum=0;
                        expnedsum=0;
                        incomesum=0;

                        for(int i=0;i<historydataArrayList.size();i++){
                            String nowday[]=historydataArrayList.get(i).date.split("\\/");
                            System.out.println(nowday[0]+nowday[1]+nowday[2]);
                            if(nowday[0].equals(String.valueOf(nyear)) && nowday[1].equals(String.valueOf(nmonth))&& nowday[2].equals(String.valueOf(nday))){
                                historydatabuffer.add(historydataArrayList.get(i));
                            }
                            if(nowday[0].equals(String.valueOf(nyear)) && nowday[1].equals(String.valueOf(nmonth))){
                                if(historydataArrayList.get(i).type.equals("expend")){
                                    sum=sum-Integer.valueOf(historydataArrayList.get(i).money);
                                    expnedsum=expnedsum+Integer.valueOf(historydataArrayList.get(i).money);
                                }
                                else if(historydataArrayList.get(i).type.equals("income")){
                                    sum=sum+Integer.valueOf(historydataArrayList.get(i).money);
                                    incomesum=incomesum+Integer.valueOf(historydataArrayList.get(i).money);
                                }
                            }
                        }
                        if(sum<0){
                            assets.setText(String.valueOf(Integer.valueOf(assetsvalue)+sum));
                        }
                        else {
                            assets.setText(String.valueOf(Integer.valueOf(assetsvalue)-sum));
                        }
                        Texpendsum.setText(String.valueOf(expnedsum));
                        Tincomesum.setText(String.valueOf(incomesum));
                        Collections.sort(historydatabuffer, new historysort());

                        BaseAdapter adapter=new BaseAdapter() {
                            @Override
                            public int getCount() {
                                return historydatabuffer.size();
                            }
                            @Override
                            public Object getItem(int i) {
                                return i;
                            }@Override

                            public long getItemId(int i) {
                                return i;
                            }
                            @Override
                            public View getView(int i, View view, ViewGroup viewGroup) {
                                View layout=View.inflate(account.this,R.layout.expenddatalayout,null);
                                TextView subcategory=layout.findViewById(R.id.subcategory);
                                TextView money=layout.findViewById(R.id.money);
                                subcategory.setText(historydatabuffer.get(i).maincategory);
                                if(historydatabuffer.get(i).type.equals("expend")){
                                    money.setText(" - "+String.valueOf(historydatabuffer.get(i).money)+" $");
                                    money.setTextColor(Color.RED);
                                }
                                else if(historydatabuffer.get(i).type.equals("income")){
                                    money.setText(" + "+String.valueOf(historydatabuffer.get(i).money)+" $");
                                    money.setTextColor(Color.BLUE);
                                }
                                return layout;

                            }
                        };
                        listViewhistroy.setAdapter(adapter);
                    }

                }, mYear,mMonth, mDay).show();
            }
        });
        bincome=findViewById(R.id.income);
        bincome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(account.this, tab.class);
                int n=2;
                intent.putExtra("postion",n);
                startActivity(intent);
            }
        });
        bexpend=findViewById(R.id.expend);
        bexpend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(account.this, tab.class);
                int n=1;
                intent.putExtra("postion",n);
                startActivity(intent);
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onSupportNavigateUp() {
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.calendar:
                Intent intent = new Intent(account.this, calendar.class);
                startActivity(intent);
                break;
            case R.id.updatepassword:
                 intent = new Intent(account.this, updatepassword.class);
                startActivity(intent);
                break;
            case R.id.logout:
                 intent = new Intent(account.this, login.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
}