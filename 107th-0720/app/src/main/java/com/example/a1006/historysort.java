package com.example.a1006;

import java.util.Comparator;

public class historysort implements Comparator<historydata>
{

    @Override
    public int compare(historydata h1, historydata h2) {
        return h1.time.compareTo(h2.time);
    }
}