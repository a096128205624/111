package com.example.a1006;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class wish extends AppCompatActivity {
    FloatingActionButton fab;
    ListView wishlsit;
    TextView rank,goal,endtime,price,percentage,start,daily;
    ArrayList<wishdata> wishdataArrayList=new ArrayList<wishdata>();
    boolean end=false;
    @Nullable
    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wish);
        Global global=(Global)getApplicationContext();
        fab = (FloatingActionButton) findViewById(R.id.fab);
        wishlsit =findViewById(R.id.modeexdata1);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Mysqlqrcode con = new Mysqlqrcode();
                wishdataArrayList = con.getwishdata(global.accountname);
                end=true;
                System.out.println("陣列:"+wishdataArrayList.size());
            }
        }).start();

        int i=0;
        while (end!=true){
            BaseAdapter adapterex1=new BaseAdapter() {
                @Override
                public int getCount() {
                    return wishdataArrayList.size();
                }

                @Override
                public Object getItem(int position) {
                    return position;
                }

                @Override
                public long getItemId(int position) {
                    return position;
                }

                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View layout = View.inflate(wish.this, R.layout.wishdatalayout, null);
                    rank=layout.findViewById(R.id.rank);
                    goal = layout.findViewById(R.id.goal);
                    endtime = layout.findViewById(R.id.endtime);
                    price = layout.findViewById(R.id.price);
                    percentage = layout.findViewById(R.id.percentage);
                    start = layout.findViewById(R.id.start);
                    daily = layout.findViewById(R.id.daily);
                    rank.setText(wishdataArrayList.get(position).rank+"");
                    goal.setText(wishdataArrayList.get(position).name);
                    endtime.setText(wishdataArrayList.get(position).end);
                    price.setText(wishdataArrayList.get(position).cost+"$");
                    start.setText(wishdataArrayList.get(position).start);
                    daily.setText(wishdataArrayList.get(position).daily+"$");

                    percentage.setText("100%");
                    return layout;
                }
            };
            wishlsit.setAdapter(adapterex1);
            AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //Toast.makeText(getActivity(),"您點選了:"+wishdataArrayList.get(position),Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent();
                    intent.setClass(wish.this, moidfywish.class);
                    Bundle bundle=new Bundle();
                    bundle.putInt("rank",(wishdataArrayList.get(position).rank));
                    bundle.putString("gaol",String.valueOf(wishdataArrayList.get(position).name));
                    bundle.putString("price",String.valueOf(wishdataArrayList.get(position).cost));
                    bundle.putString("start",String.valueOf(wishdataArrayList.get(position).start));
                    bundle.putString("end",String.valueOf(wishdataArrayList.get(position).end));
                    intent.putExtras(bundle);
                    startActivity(intent);
                    // startActivity(new Intent(getActivity(), moidfywish.class));
                }
            };
            wishlsit.setOnItemClickListener(onItemClickListener);
        }


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(wish.this, addwish.class));

            }
        });




        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(wish.this, addwish.class));

            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode==KeyEvent.KEYCODE_BACK){
            Intent intent = new Intent(wish.this, MainActivity.class);
            startActivity(intent);
        }
        return false;
    }
}
