package com.example.a1006;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;


public  class pageAdapterMode extends FragmentStateAdapter {
    private int numoftabs;


    public pageAdapterMode(@NonNull FragmentActivity fr, int numOfTabs) {
        super(fr);
        this.numoftabs = numOfTabs;
    }
    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0:
                return new expendmode();
            case 1:
                return new incomemode();
            default:
                return null;
        }

    }
    @Override
    public int getItemCount() {
        return numoftabs;
    }

}