package com.example.a1006;

import java.util.Comparator;

public class invoicesort implements Comparator<invoicedata>
{

    @Override
    public int compare(invoicedata h1, invoicedata h2) {
        return h1.date.compareTo(h2.date);
    }
}