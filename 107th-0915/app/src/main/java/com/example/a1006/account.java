package com.example.a1006;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.ui.AppBarConfiguration;

import com.google.android.material.navigation.NavigationView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

public class account extends AppCompatActivity {

    ImageView expandbt,incomebt;
    TextView tdate,assets,Texpendsum,Tincomesum;
    TextView onedate,onedatenumber,twodate,twodatenumber,threedate,threedatenumber,fourdate,fourdatenumber,fivedate,fivedatenumber;
    Global global;
    String assettextview;
    LinearLayout linearLayout,LinearLayout1,LinearLayout2,LinearLayout3,LinearLayout4,LinearLayout5;
    Calendar c =Calendar.getInstance();
    ArrayList<historydata> historydataArrayList=new ArrayList<historydata>();
    int assetsvalue;
    int nyear = c.get(Calendar.YEAR);
    int nmonth = c.get(Calendar.MONTH)+1;
    int nday = c.get(Calendar.DAY_OF_MONTH);
    int sum=0,expnedsum=0,incomesum=0;
    Boolean end=false;
    int choiceyear,choicemonth,choiceday;
    ArrayList<historydata> historydatabuffer=new ArrayList<historydata>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account);
        expandbt=findViewById(R.id.exbt);
        incomebt=findViewById(R.id.inbt);
        global=(Global)getApplicationContext();
        tdate=findViewById(R.id.date);
        assets=findViewById(R.id.assets);
        Texpendsum=findViewById(R.id.totalexpendvalue);
        linearLayout=findViewById(R.id.LinearLayout);
        LinearLayout1=findViewById(R.id.LinearLayout1);
        LinearLayout2=findViewById(R.id.LinearLayout2);
        LinearLayout4=findViewById(R.id.LinearLayout4);
        LinearLayout5=findViewById(R.id.LinearLayout5);
        Tincomesum=findViewById(R.id.totalincomevalue);
        onedate=findViewById(R.id.onedate);
        onedatenumber=findViewById(R.id.onedatenumber);
        twodate=findViewById(R.id.twodate);
        twodatenumber=findViewById(R.id.twodatenumber);
        threedate=findViewById(R.id.threedate);
        threedatenumber=findViewById(R.id.threedatenumber);
        fourdate=findViewById(R.id.fourdate);
        fourdatenumber=findViewById(R.id.fourdatenumber);
        fivedate=findViewById(R.id.fivedate);
        fivedatenumber=findViewById(R.id.fivedatenumber);
        settimetxtview(nyear,nmonth,nday);
        LinearLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int three=Integer.valueOf(threedatenumber.getText().toString());
                int one=Integer.valueOf(onedatenumber.getText().toString());
                System.out.println(one);
                if(three-one<0){
                    nmonth--;
                    if(nmonth<=0){
                        nyear--;
                        nmonth=12;
                    }
                    settimetxtview(nyear,nmonth,one);
                    tdate.setText(nyear+"/"+(nmonth)+"/"+one);
                }
                else{
                    settimetxtview(nyear,nmonth,one);
                    tdate.setText(nyear+"/"+nmonth+"/"+one);
                }

            }
        });
        LinearLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int three=Integer.valueOf(threedatenumber.getText().toString());
                int two=Integer.valueOf(twodatenumber.getText().toString());

                if(three-two<0){
                    nmonth--;
                    if(nmonth<=0){
                        nyear--;
                        nmonth=12;
                    }
                    settimetxtview(nyear,nmonth,two);
                    tdate.setText(nyear+"/"+(nmonth)+"/"+two);
                }
                else{
                    settimetxtview(nyear,nmonth,two);
                    tdate.setText(nyear+"/"+nmonth+"/"+two);
                }
            }
        });
        LinearLayout4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int three=Integer.valueOf(threedatenumber.getText().toString());
                int four=Integer.valueOf(fourdatenumber.getText().toString());
                System.out.println("----------------"+nmonth);
                if(four-three<0){
                    nmonth++;
                    System.out.println("----------------"+nmonth);
                    if(nmonth>12){
                        nyear++;
                        nmonth=1;
                    }
                    settimetxtview(nyear,nmonth,four);
                    tdate.setText(nyear+"/"+(nmonth)+"/"+four);
                }
                else{
                    settimetxtview(nyear,nmonth,four);
                    tdate.setText(nyear+"/"+nmonth+"/"+four);
                }
            }
        });
        LinearLayout5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int three=Integer.valueOf(threedatenumber.getText().toString());
                int five=Integer.valueOf(fivedatenumber.getText().toString());

                if(five-three<0){
                    nmonth++;
                    if(nmonth>12){
                        nyear++;
                        nmonth=1;
                    }
                    settimetxtview(nyear,nmonth,five);
                    tdate.setText(nyear+"/"+(nmonth)+"/"+five);
                }
                else{
                    settimetxtview(nyear,nmonth,five);
                    tdate.setText(nyear+"/"+nmonth+"/"+five);
                }
            }
        });




        new Thread(new Runnable() {
            @Override
            public void run() {
                Mysqlqrcode con = new Mysqlqrcode();
                historydataArrayList= con.gethistoryData(global.accountname);
                global.setHistorydata(historydataArrayList);
                for(int i=0;i<historydataArrayList.size();i++){
                    String nowday[]=historydataArrayList.get(i).date.split("\\/");
                    if(nowday[0].equals(String.valueOf(nyear)) && nowday[1].equals(String.valueOf(nmonth))&& nowday[2].equals(String.valueOf(nday))){
                        historydatabuffer.add(historydataArrayList.get(i));
                    }
                    if(nowday[0].equals(String.valueOf(nyear)) && nowday[1].equals(String.valueOf(nmonth))){
                        if(historydataArrayList.get(i).type.equals("expend")){
                            sum=sum-Integer.valueOf(historydataArrayList.get(i).money);
                            expnedsum=expnedsum+Integer.valueOf(historydataArrayList.get(i).money);
                        }
                        else if(historydataArrayList.get(i).type.equals("income")){
                            sum=sum+Integer.valueOf(historydataArrayList.get(i).money);
                            incomesum=incomesum+Integer.valueOf(historydataArrayList.get(i).money);
                        }
                    }
                }
                assetsvalue=con.getassets(global.accountname);
                System.out.println(assetsvalue+"  "+sum);
                Collections.sort(historydatabuffer, new historysort());
                end=true;

            }
        }).start();
       while (!end){
            assets.setText(String.valueOf(assetsvalue+sum));
            Tincomesum.setText("額外收入 : "+String.valueOf(incomesum)+"元");
            Texpendsum.setText("總支出 : "+String.valueOf(expnedsum)+"元");
        }


        assets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(account.this);
                final EditText editText = new EditText(account.this); //final一個editText
                builder.setView(editText);
                builder.setTitle("請輸入每月基本薪資 目前是:"+String.valueOf(assetsvalue)+"$");
                builder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if(!editText.getText().toString().equals("")){
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Mysqlqrcode con = new Mysqlqrcode();
                                    con.insertAssets(global.accountname,editText.getText().toString());
                                }
                            }).start();
                            assettextview=editText.getText().toString();
                            assetsvalue=Integer.valueOf(assettextview);
                            assets.setText(String.valueOf(assetsvalue+sum));
                        }
                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                builder.create().show();
            }
        });


        tdate.setText(nyear+"/"+nmonth+"/"+nday);
        tdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                historydatabuffer.clear();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                new DatePickerDialog(account.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        nmonth=month+1;
                        nyear=year;
                        nday=day;
                        tdate.setText(year+"/"+(month+1)+"/"+day);
                        if(c.get(Calendar.YEAR)!=year||c.get(Calendar.MONTH)!=month+1){
                            linearLayout.setVisibility(view.GONE);
                        }else{
                            linearLayout.setVisibility(view.VISIBLE);
                        }
                        sum=0;
                        expnedsum=0;
                        incomesum=0;

                        for(int i=0;i<historydataArrayList.size();i++){
                            String nowday[]=historydataArrayList.get(i).date.split("\\/");
                            System.out.println(nowday[0]+nowday[1]+nowday[2]);
                            if(nowday[0].equals(String.valueOf(year)) && nowday[1].equals(String.valueOf(month+1))&& nowday[2].equals(String.valueOf(day))){
                                historydatabuffer.add(historydataArrayList.get(i));
                            }
                            if(nowday[0].equals(String.valueOf(year)) && nowday[1].equals(String.valueOf(month+1))){
                                if(historydataArrayList.get(i).type.equals("expend")){
                                    sum=sum-Integer.valueOf(historydataArrayList.get(i).money);
                                    expnedsum=expnedsum+Integer.valueOf(historydataArrayList.get(i).money);
                                }
                                else if(historydataArrayList.get(i).type.equals("income")){
                                    sum=sum+Integer.valueOf(historydataArrayList.get(i).money);
                                    incomesum=incomesum+Integer.valueOf(historydataArrayList.get(i).money);
                                }
                            }
                        }
                        assets.setText(String.valueOf(Integer.valueOf(assetsvalue)+sum));
                        Tincomesum.setText("額外收入 : "+String.valueOf(incomesum)+"元");
                        Texpendsum.setText("總支出 : "+String.valueOf(expnedsum)+"元");
                        Collections.sort(historydatabuffer, new historysort());
                        settimetxtview(year,month+1,day);



                    }

                }, mYear,mMonth, mDay).show();
            }
        });


        incomebt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(account.this, tab.class);
                int n=2;
                intent.putExtra("postion",n);
                startActivity(intent);
            }
        });

        expandbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(account.this, tab.class);
                int n=1;
                intent.putExtra("postion",n);
                startActivity(intent);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onSupportNavigateUp() {
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.calendar:
                Intent intent = new Intent(account.this, calendar.class);
                startActivity(intent);
                break;
            case R.id.updatepassword:
                intent = new Intent(account.this, updatepassword.class);
                startActivity(intent);
                break;
            case R.id.logout:
                global.sp.edit().putBoolean("automatic_login",false).commit();
                global.sp.edit().putBoolean("rem_isCheck",false).commit();
                intent = new Intent(account.this, login.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
    public int getMonthLastDay(int year, int month) {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.YEAR, year);
        a.set(Calendar.MONTH, month - 1);
        a.set(Calendar.DATE, 1);//把日期设置为当月第一天
        a.roll(Calendar.DATE, -1);//日期回滚一天，也就是最后一天
        int maxDate = a.get(Calendar.DATE);
        return maxDate;
    }
    public String getweekday(int wek){
        String Week="";
        if (wek == 1) {
            Week  = "星期日";
        }
        if (wek == 2) {
            Week  = "星期一";
        }
        if (wek == 3) {
            Week  = "星期二";
        }
        if (wek == 4) {
            Week  = "星期三";
        }
        if (wek == 5) {
            Week  = "星期四";
        }
        if (wek == 6) {
            Week  = "星期五";
        }
        if (wek == 7) {
            Week  = "星期六";
        }
        return Week;
    }
    private int getDayofWeek(String dateTime) {
        Calendar cal = Calendar.getInstance();
        if (dateTime.equals("")) {
            cal.setTime(new Date(System.currentTimeMillis()));
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            Date date;
            try {
                date = sdf.parse(dateTime);
            } catch (ParseException e) {
                date = null;
                e.printStackTrace();
            }
            if (date != null) {
                cal.setTime(new Date(date.getTime()));
            }
        }
        return cal.get(Calendar.DAY_OF_WEEK);
    }
    public void settimetxtview(int year,int month,int day){
        int max=getMonthLastDay(year,month);
        String timebuffer=year+"/"+String.format("%02d",(month))+"/"+String.format("%02d",(day));
        System.out.println("timebuffer:"+timebuffer);
        int nweek = getDayofWeek(timebuffer);

        if(nweek==1){
            onedate.setText(getweekday(6));
            twodate.setText(getweekday(7));
            threedate.setText(getweekday(1));
            fourdate.setText(getweekday(2));
            fivedate.setText(getweekday(3));
        }
        else if(nweek==2){
            onedate.setText(getweekday(7));
            twodate.setText(getweekday(1));
            threedate.setText(getweekday(2));
            fourdate.setText(getweekday(3));
            fivedate.setText(getweekday(4));
        }
        else if(nweek==6){
            onedate.setText(getweekday(4));
            twodate.setText(getweekday(5));
            threedate.setText(getweekday(6));
            fourdate.setText(getweekday(7));
            fivedate.setText(getweekday(1));
        }
        else if(nweek==7){
            onedate.setText(getweekday(5));
            twodate.setText(getweekday(6));
            threedate.setText(getweekday(7));
            fourdate.setText(getweekday(1));
            fivedate.setText(getweekday(2));
        }
        else{
            onedate.setText(getweekday(nweek-2));
            twodate.setText(getweekday(nweek-1));
            threedate.setText(getweekday(nweek));
            fourdate.setText(getweekday(nweek+1));
            fivedate.setText(getweekday(nweek+2));
        }

        if(day==2){
            int lastmax=getMonthLastDay(year,month-1);
            onedatenumber.setText(String.valueOf(lastmax));
            twodatenumber.setText(String.valueOf(day-1));
            threedatenumber.setText(String.valueOf(day));
            fourdatenumber.setText(String.valueOf(day+1));
            fivedatenumber.setText(String.valueOf(day+2));
        }
        else if(day==1){
            int lastmax=getMonthLastDay(year,month-1);
            onedatenumber.setText(String.valueOf(lastmax-1));
            twodatenumber.setText(String.valueOf(lastmax));
            threedatenumber.setText(String.valueOf(day));
            fourdatenumber.setText(String.valueOf(day+1));
            fivedatenumber.setText(String.valueOf(day+2));
        }
        else if(day==max-1){
            onedatenumber.setText(String.valueOf(day-2));
            twodatenumber.setText(String.valueOf(day-1));
            threedatenumber.setText(String.valueOf(day));
            fourdatenumber.setText(String.valueOf(max));
            fivedatenumber.setText(String.valueOf(1));
        }
        else if(day==max){
            onedatenumber.setText(String.valueOf(day-2));
            twodatenumber.setText(String.valueOf(day-1));
            threedatenumber.setText(String.valueOf(day));
            fourdatenumber.setText(String.valueOf(1));
            fivedatenumber.setText(String.valueOf(2));
        }
        else{
            onedatenumber.setText(String.valueOf(day-2));
            twodatenumber.setText(String.valueOf(day-1));
            threedatenumber.setText(String.valueOf(day));
            fourdatenumber.setText(String.valueOf(day+1));
            fivedatenumber.setText(String.valueOf(day+2));
        }
    }
}