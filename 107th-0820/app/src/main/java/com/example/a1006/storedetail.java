package com.example.a1006;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;

public class storedetail extends AppCompatActivity {
    TextView tname,tphone,taddress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.storedetail);
        tname=findViewById(R.id.name);
        tphone=findViewById(R.id.phone);
        taddress=findViewById(R.id.address);
        Bundle bundle = getIntent().getExtras();
        String storename = bundle.getString("name");
        String phone =bundle.getString("phone");
        String address=bundle.getString("address");
        float lat=bundle.getFloat("lat");
        float lng=bundle.getFloat("lng");
        String tmp;
        tmp=phone.replaceAll("\\s","");
        tname.setText(String.valueOf(storename));
        tphone.setText(String.valueOf(tmp));
        taddress.setText(String.valueOf(address));

        taddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=(%s)",storename);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            }
        });
        tphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri=Uri.parse("tel:"+phone);
                Intent intent=new Intent(Intent.ACTION_DIAL,uri);
                startActivity(intent);
            }
        });



    }
}
