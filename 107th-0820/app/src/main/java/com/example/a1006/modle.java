package com.example.a1006;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link modle#newInstance} factory method to
 * create an instance of this fragment.
 */
public class modle extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public modle() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment tab3.
     */
    // TODO: Rename and change types and number of parameters
    public static modle newInstance(String param1, String param2) {
        modle fragment = new modle();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    TextView titleExTextview,mainSubCategoryExTextview,moneyExTextview;
    TextView titleInTextview,mainSubCategoryInTextview,moneyInTextview;
    ListView listViewhEx,listViewhIn,test;
    Button add;
    ArrayList<modeexdata> historydataArrayList=new ArrayList<modeexdata>();
    ArrayList<modeexdata> exhistorydataArrayList=new ArrayList<modeexdata>();
    ArrayList<modeexdata> inhistorydataArrayList=new ArrayList<modeexdata>();
    boolean end=false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.modle, container, false);

        add=view.findViewById(R.id.button3);
        listViewhEx=view.findViewById(R.id.modeexdata);
        listViewhIn=view.findViewById(R.id.modeindata);
        Global global=(Global)getActivity().getApplicationContext();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Mysqlqrcode con = new Mysqlqrcode();
                historydataArrayList= con.getModeExData(global.accountname);
                for(int i=0;i<historydataArrayList.size();i++){
                    if(historydataArrayList.get(i).category.equals("支出")){
                        exhistorydataArrayList.add(historydataArrayList.get(i));
                    }else{
                        inhistorydataArrayList.add(historydataArrayList.get(i));
                    }
                }
                end=true;
            }
        }).start();
        while (end!=true){
            BaseAdapter adapterex=new BaseAdapter() {
                @Override
                public int getCount() {
                    return exhistorydataArrayList.size();
                }

                @Override
                public Object getItem(int position) {
                    return position;
                }

                @Override
                public long getItemId(int position) {
                    return position;
                }

                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View layout = View.inflate(getActivity(), R.layout.modeexdatalayout,null);
                    titleExTextview=layout.findViewById(R.id.textView22);
                    mainSubCategoryExTextview=layout.findViewById(R.id.textView23);
                    moneyExTextview=layout.findViewById(R.id.textView24);

                    titleExTextview.setText(exhistorydataArrayList.get(position).titleEx);
                    mainSubCategoryExTextview.setText(exhistorydataArrayList.get(position).maincategoryEx);
                    moneyExTextview.setText(exhistorydataArrayList.get(position).moneyEx+"$");
                    return layout;
                }
            };

            listViewhEx.setAdapter(adapterex);
            BaseAdapter adapterin=new BaseAdapter() {
                @Override
                public int getCount() {
                    return inhistorydataArrayList.size();
                }

                @Override
                public Object getItem(int position) {
                    return position;
                }

                @Override
                public long getItemId(int position) {
                    return position;
                }

                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View layout = View.inflate(getActivity(), R.layout.modeindatalayout,null);
                    titleInTextview=layout.findViewById(R.id.textView25);
                    mainSubCategoryInTextview=layout.findViewById(R.id.textView26);
                    moneyInTextview=layout.findViewById(R.id.textView27);

                    titleInTextview.setText(inhistorydataArrayList.get(position).titleEx);
                    mainSubCategoryInTextview.setText(inhistorydataArrayList.get(position).maincategoryEx);
                    moneyInTextview.setText(inhistorydataArrayList.get(position).moneyEx+"$");
                    return layout;
                }
            };
            listViewhIn.setAdapter(adapterin);
        }
        listViewhEx.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                global.setPositionEX(String.valueOf(position));
                Intent intent = new Intent(getActivity(), tab.class);
                int n=1;
                intent.putExtra("postion",n);
                startActivity(intent);
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), tabmode.class);
                int n=0;
                intent.putExtra("postion",n);
                startActivity(intent);
            }
        });

        return  view;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode==KeyEvent.KEYCODE_BACK){
            Intent intent = new Intent(getActivity(), account.class);
            startActivity(intent);
        }
        return false;
    }
}