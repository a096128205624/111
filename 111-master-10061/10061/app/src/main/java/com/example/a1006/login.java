package com.example.a1006;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class login  extends AppCompatActivity {
    EditText Eaccount,Epassword;
    Button login,sing;
    String account,password;
    boolean check=false;
    AccountValue av;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loging);
        sing=findViewById(R.id.sing);
        login=findViewById(R.id.login);
        Eaccount=findViewById(R.id.account);
        Epassword=findViewById(R.id.Password);

        sing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(login.this, sing.class);
                startActivity(intent);
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                account=Eaccount.getText().toString();
                password=Epassword.getText().toString();
                Mysqlqrcode mysqlqrcode=new Mysqlqrcode();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        av=mysqlqrcode.getAccount(account);
                        System.out.println(av.account+av.password);

                    }
                }).start();
                while (av==null){//等待搜尋帳號
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if(av.account.equals(account) && av.password.equals(password)){
                    Intent intent = new Intent(login.this, MainActivity.class);
                    startActivity(intent);
                }
                else if(!av.account.equals(account)){
                    Toast toast = Toast.makeText(login.this, "無此帳號", Toast.LENGTH_LONG);
                    toast.show();
                }
                else if(!av.password.equals(password)){
                    Toast toast = Toast.makeText(login.this, "密碼錯誤", Toast.LENGTH_LONG);
                    toast.show();
                }


            }
        });
    }
}
