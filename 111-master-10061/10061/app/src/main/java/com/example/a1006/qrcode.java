package com.example.a1006;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

public class qrcode extends AppCompatActivity {
    SurfaceView surfaceView;
    TextView textView;
    CameraSource cameraSource;
    BarcodeDetector barcodeDetector;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qrcode);

        getPermissionsCanera();

        surfaceView=(SurfaceView)findViewById(R.id.surfaceView);
        textView=(TextView)findViewById(R.id.textView5);

        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE).build();
        cameraSource=new CameraSource.Builder(this,barcodeDetector)
                .setRequestedPreviewSize(300,300).build();
        cameraSource = new CameraSource.Builder(this,barcodeDetector).setAutoFocusEnabled(true).build();
        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback(){
            @Override
            public void surfaceCreated(@NonNull SurfaceHolder surfaceHolder) {
                if(ActivityCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.CAMERA)
                        !=PackageManager.PERMISSION_GRANTED)
                    return;
                try{
                    cameraSource.start(surfaceHolder);
                }catch (IOException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(@NonNull SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(@NonNull SurfaceHolder surfaceHolder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>(){

            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> qrCodes=detections.getDetectedItems();
                if(qrCodes.size()!=0){
                    textView.post(new Runnable() {
                        @Override
                        public void run() {
                            String strings,result,strings1 ="",code,data,randomcode,string2="";
                            strings=qrCodes.valueAt(0).displayValue;
                            strings.split("");
                            result=strings.substring(0,1);
                            boolean result1 = result.matches("[a-zA-Z]+");
                            if(result1){
                                strings1=strings;
                                code=strings1.substring(0,10);
                                data=strings1.substring(10,17);
                                randomcode=strings1.substring(17,21);
                                String[] strs=strings1.split(":");
                                //textView.setText(strs[5]);
                                string2=string2+"發票號碼:"+code+"\n日期:"+data+"\n隨機碼"+randomcode;
                                for(int i=5;i<strs.length-1;i=i+3){
                                    string2+="\n品名:"+strs[i]+"\t"+strs[i+1]+"個\t"+strs[i+2]+"元";
                                    if(strs.length-1==i){
                                        string2+="\n品名:"+strs[i]+"\t"+strs[i+1]+"個\t"+strs[i+2]+"元";
                                    }
                                }
                                //String[] strs1=string2.split(":");
                                //textView.setText("發票號碼:"+strs1[0]+"\n日期:"+strs1[1]+"\n隨機碼"+strs1[2]+"\n品名:"+strs1[3]+strs1[4]+);
                                textView.setText(strings1);
                                String finalString = string2;
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        // 將資料寫入資料庫
                                        Mysqlqrcode con = new Mysqlqrcode();
                                        con.insertData("stringdata");
                                    }
                                }).start();
                               // System.out.println(strs.length);
                            }
                            //textView.setText(qrCodes.valueAt(0).displayValue);
                        }

                    });
                }
            }
        });
    }


    public void getPermissionsCanera(){
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA},1);
        }
    }
}
