package com.example.a1006;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class tab extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager2 viewPager;
    TabItem modle,income,transfer,expend;
    pageAdapter pageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab);
        final String[] name={"modle","expend","income","transfer"};

        tabLayout = findViewById(R.id.Tablayout);
        viewPager = findViewById(R.id.viewpager);
        modle = findViewById(R.id.modle);
        expend = findViewById(R.id.expend);
        transfer = findViewById(R.id.transfer);
        income = findViewById(R.id.income);

        pageAdapter = new pageAdapter(this, tabLayout.getTabCount());
        int position;
        Intent intent=getIntent();
        position=intent.getIntExtra("postion",0);
        viewPager.setAdapter(pageAdapter);
        switch (position){
            case 0:
                viewPager.setCurrentItem(0);
                break;
            case 1:
                viewPager.setCurrentItem(1);
                break;
            case 2:
                viewPager.setCurrentItem(2);
                break;
            case 3:
                viewPager.setCurrentItem(3);
                break;

        }
        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(tabLayout, viewPager, true, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                tab.setText(name[position]);

            }
        });

        tabLayoutMediator.attach();

    }
}