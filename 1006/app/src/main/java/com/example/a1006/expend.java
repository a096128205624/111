package com.example.a1006;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link expend#newInstance} factory method to
 * create an instance of this fragment.
 */
public class expend extends Fragment  {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";



    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public expend() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment tab1.
     */
    // TODO: Rename and change types and number of parameters
    public static expend newInstance(String param1, String param2) {
        expend fragment = new expend();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }
    public static final String TAG = expend.class.getSimpleName()+"My";

    private String mPath = "";//設置高畫質的照片位址
    public static final int CAMERA_PERMISSION = 100;//檢測相機權限用
    public static final int REQUEST_HIGH_IMAGE = 101;//檢測高畫質相機回傳
    public static final int REQUEST_SELECT = 102;//檢測低畫質相機回傳
    TextView tdate,ttime;
    ImageView photo;
    DatePickerDialog.OnDateSetListener dateSetListener;
    TimePickerDialog.OnTimeSetListener timeSetListener;
    private static String mPublicPhotoPath;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

         View view=inflater.inflate(R.layout.expend, container, false);
         ttime=view.findViewById(R.id.time);
         tdate=view.findViewById(R.id.date);
         photo=view.findViewById(R.id.photo);
        Calendar cal=Calendar.getInstance();
        int year=cal.get(Calendar.YEAR);
        int month=cal.get(Calendar.MONTH);
        int day=cal.get(Calendar.DATE);
        int hour=cal.get(Calendar.HOUR_OF_DAY);
        int min=cal.get(Calendar.MINUTE);

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED)
            requestPermissions(new String[]{Manifest.permission.CAMERA},CAMERA_PERMISSION);
        //相機
        final String[] action = {"拍照","從相簿選擇","關閉"};
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialog_list = new AlertDialog.Builder(getActivity());
                dialog_list.setItems(action, new DialogInterface.OnClickListener(){
                    @Override
                    //只要你在onClick處理事件內，使用which參數，就可以知道按下陣列裡的哪一個了
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        if(action[which].equals("拍照")){
                            Intent highIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                            //取得相片檔案的URI位址及設定檔案名稱
                            File imageFile = getImageFile();
                            if (imageFile == null) return;
                            //取得相片檔案的URI位址
                            Uri imageUri = FileProvider.getUriForFile(
                                    getActivity(),
                                    "com.example.myapplication1.CameraEx",//記得要跟AndroidManifest.xml中的authorities 一致
                                    imageFile
                            );
                            highIntent.putExtra(MediaStore.EXTRA_OUTPUT,imageUri);

                            startActivityForResult(highIntent,REQUEST_HIGH_IMAGE);//開啟相機
                        }
                        else if(action[which].equals("從相簿選擇")){
                            Intent intent = new Intent();
                            /* 開啟Pictures畫面Type設定為image */
                            intent.setType("image/*");
                            /* 使用Intent.ACTION_GET_CONTENT這個Action */
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            /* 取得相片後返回本畫面 */
                            startActivityForResult(intent, REQUEST_SELECT);
                        }
                    }
                    private File getImageFile()  {
                        String time = new SimpleDateFormat("yyMMdd").format(new Date());
                        String fileName = time+"_";
                        File dir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                        try {
                            //給予檔案命名及檔案格式
                            File imageFile = File.createTempFile(fileName,".jpg",dir);
                            //給予全域變數中的照片檔案位置，方便後面取得
                            mPath = imageFile.getAbsolutePath();
                            return imageFile;
                        } catch (IOException e) {
                            return null;
                        }
                    }
                });
                dialog_list.show();
            }
        });
        //設定現在日期
        String date = year + "/" + String.format("%02d", (month+1)) + "/" + String.format("%02d", day);
        tdate.setText(date);
        //設定現在時間
        String time = (((hour+8)%24)) + ":" + String.format("%02d", min);
        ttime.setText(time);
        //日期選擇
         tdate.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Calendar cal=Calendar.getInstance();
                 int year=cal.get(Calendar.YEAR);
                 int month=cal.get(Calendar.MONTH);
                 int day=cal.get(Calendar.DATE);


                 dateSetListener=new DatePickerDialog.OnDateSetListener() {
                     @Override
                     public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                         String date = year + "/" + String.format("%02d", (month+1)) + "/" + String.format("%02d", day);
                         tdate.setText(date);
                     }
                 };

                 DatePickerDialog dialog=new DatePickerDialog(getActivity(), dateSetListener, year,month,day);
                 dialog.show();


             }
         });
         //時間選擇
         ttime.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Calendar cal=Calendar.getInstance();
                 int hour=cal.get(Calendar.HOUR_OF_DAY);
                 int min=cal.get(Calendar.MINUTE);

                 timeSetListener=new TimePickerDialog.OnTimeSetListener() {
                     @Override
                     public void onTimeSet(TimePicker timePicker, int hour, int min) {
                         Log.d("onTimeSet: hh/mm: ", +hour + "/" + min);
                         String time = (hour+8) + ":" + String.format("%02d", min);
                         ttime.setText(time);
                     }
                 };

                 TimePickerDialog dialog=new TimePickerDialog(
                         getActivity(),
                         timeSetListener,
                         hour,min, DateFormat.is24HourFormat(getActivity())
                 );
                 dialog.show();

             }

         });

        return view;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        photo = getActivity().findViewById(R.id.photo);
        if (requestCode == REQUEST_HIGH_IMAGE && resultCode == -1) {

            new Thread(() -> {
                //在BitmapFactory中以檔案URI路徑取得相片檔案，並處理為AtomicReference<Bitmap>，方便後續旋轉圖片
                AtomicReference<Bitmap> getHighImage = new AtomicReference<>(BitmapFactory.decodeFile(mPath));
                Matrix matrix = new Matrix();
                matrix.setRotate(0f);//轉90度
                Bitmap bit=Bitmap.createBitmap(getHighImage.get()
                        , 0, 0
                        , getHighImage.get().getWidth()
                        , getHighImage.get().getHeight()
                        , matrix, true);

                getHighImage.set(bit);

                getActivity().runOnUiThread(() -> {
                    //以Glide設置圖片(因為旋轉圖片屬於耗時處理，故會LAG一下，且必須使用Thread執行緒)
                    Glide.with(getActivity()).load(getHighImage.get()).centerCrop().into(photo);
                });
                String fileName = System.currentTimeMillis() + ".jpg";
                try {
                    MediaStore.Images.Media.insertImage(getActivity().getContentResolver(),
                          mPath, fileName, null);
                    getActivity().sendBroadcast(
                            new Intent(
                                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
                                    Uri.parse("file://" + mPath)));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }



            }).start();
        }
        else if (requestCode ==REQUEST_SELECT && resultCode == -1) {
            Uri uri = data.getData();
            Log.e("uri", uri.toString());
            ContentResolver cr = getActivity().getContentResolver();
            try {
                Bitmap bitmap = BitmapFactory.decodeStream(cr.openInputStream(uri));
                /* 將Bitmap設定到ImageView */
             photo.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                Log.e("Exception", e.getMessage(), e);
            }
        }
        else{
            Toast.makeText(getActivity(), "未作任何動作", Toast.LENGTH_SHORT).show();
        }

    }
    public static void saveImageToGallery(Context context, Bitmap bmp){
        // 首先儲存圖片
        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM);

// Create an image file name



    }

}
