package com.example.a1006;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class foodcommend extends AppCompatActivity {
    TextView lat, lng,address;
    Button b1;
    LocationManager lm;
    String commadStr;
    int mpacl = 11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.foodcommend);
        lat=findViewById(R.id.lat);
        lng=findViewById(R.id.lng);
        b1=findViewById(R.id.b1);
        address=findViewById(R.id.address);
        commadStr = LocationManager.GPS_PROVIDER;

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lm = (LocationManager) getSystemService(LOCATION_SERVICE);
                lm.requestLocationUpdates(commadStr,1000,0, ln);
                if (ActivityCompat.checkSelfPermission(foodcommend.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(foodcommend.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    ActivityCompat.requestPermissions(foodcommend.this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},mpacl);
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                Location location = lm.getLastKnownLocation(commadStr);
                if(location!=null) {
                    lat.setText(String.valueOf((double) location.getLatitude()));
                    lng.setText(String.valueOf((double) location.getLongitude()));
                    Geocoder gc = new Geocoder(getBaseContext(), Locale.TRADITIONAL_CHINESE);
                    List<Address> lstAddress = null;
                    try {
                        lstAddress = gc.getFromLocation(location.getLatitude(),location.getLongitude(), 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String returnAddress=lstAddress.get(0).getAddressLine(0);
                    address.setText(returnAddress);
                }else{
                    lat.setText("定位中");
                }

            }
        });
    }
    public LocationListener ln =new LocationListener() {
        @Override
        public void onLocationChanged(@NonNull Location location) {
            lat.setText(String.valueOf((double) location.getLatitude()));
            lng.setText(String.valueOf((double) location.getLongitude()));
            Geocoder gc = new Geocoder(getBaseContext(), Locale.TRADITIONAL_CHINESE);
            List<Address> lstAddress = null;
            try {
                lstAddress = gc.getFromLocation(location.getLatitude(),location.getLongitude(), 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String returnAddress=lstAddress.get(0).getAddressLine(0);
            address.setText(returnAddress);
        }
    };
}