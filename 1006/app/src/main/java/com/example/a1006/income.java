package com.example.a1006;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.fragment.app.Fragment;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link income#newInstance} factory method to
 * create an instance of this fragment.
 */
public class income extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public income() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment tab2.
     */
    // TODO: Rename and change types and number of parameters
    public static income newInstance(String param1, String param2) {
        income fragment = new income();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    TextView tdate,ttime;
    DatePickerDialog.OnDateSetListener dateSetListener;
    TimePickerDialog.OnTimeSetListener timeSetListener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.income, container, false);
        ttime=view.findViewById(R.id.time);
        tdate=view.findViewById(R.id.date);
        Calendar cal=Calendar.getInstance();
        int year=cal.get(Calendar.YEAR);
        int month=cal.get(Calendar.MONTH);
        int day=cal.get(Calendar.DATE);
        int hour=cal.get(Calendar.HOUR_OF_DAY);
        int min=cal.get(Calendar.MINUTE);
        //設定現在日期
        String date = year + "/" + String.format("%02d", (month+1)) + "/" + String.format("%02d", day);
        tdate.setText(date);
        //設定現在時間
        String time = (((hour+8)%24)) + ":" + String.format("%02d", min);
        ttime.setText(time);
        //日期選擇
        tdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal=Calendar.getInstance();
                int year=cal.get(Calendar.YEAR);
                int month=cal.get(Calendar.MONTH);
                int day=cal.get(Calendar.DATE);


                dateSetListener=new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        String date = year + "/" + String.format("%02d", (month+1)) + "/" + String.format("%02d", day);
                        tdate.setText(date);
                    }
                };

                DatePickerDialog dialog=new DatePickerDialog(getActivity(), dateSetListener, year,month,day);
                dialog.show();


            }
        });
        //時間選擇
        ttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal=Calendar.getInstance();
                int hour=cal.get(Calendar.HOUR_OF_DAY);
                int min=cal.get(Calendar.MINUTE);

                timeSetListener=new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int min) {
                        Log.d("onTimeSet: hh/mm: ", +hour + "/" + min);
                        String time = (hour+8) + ":" + String.format("%02d", min);
                        ttime.setText(time);
                    }
                };

                TimePickerDialog dialog=new TimePickerDialog(
                        getActivity(),
                        timeSetListener,
                        hour,min, DateFormat.is24HourFormat(getActivity())
                );
                dialog.show();

            }

        });
        // Inflate the layout for this fragment
        return view;
    }
}