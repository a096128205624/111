package com.example.a1006;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.provider.MediaStore;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link expendmode#newInstance} factory method to
 * create an instance of this fragment.
 */
public class expendmode extends Fragment{

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public expendmode() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment tab1.
     */
    // TODO: Rename and change types and number of parameters
    public static expendmode newInstance(String param1, String param2) {
        expendmode fragment = new expendmode();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }
    public static final String TAG = expendmode.class.getSimpleName()+"My";

    private  Spinner spinner;
    EditText money,detail;
    String money1;
    Button mode,add,delete;
    String Maincategory,Subcategory;
    public int pos;
    private  Spinner spinner1;
    private String[] lunch = {"行車交通", "休閒娛樂", "醫療保險", "食品酒水", "居家繳費","購物","教育","家具","其他"};
    private String[] lunch1 = {"公車", "高鐵", "火車", "捷運","計程車/Uber","機票","船票"};
    private String[][] lunch2 = {{"公車", "高鐵", "火車", "捷運","計程車/Uber","機票","船票"},{"無"},{"無"},{"無"},{"水費", "電費", "電話費", "瓦斯費","房租費"},{"無"},{"無"},{"無"},{"無"}};
    private ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>();
    private ArrayList<String> inSideList = new ArrayList<String>();
    DatePickerDialog.OnDateSetListener dateSetListener;
    TimePickerDialog.OnTimeSetListener timeSetListener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view1=inflater.inflate(R.layout.addview,container,false);
        View view=inflater.inflate(R.layout.expendmode, container, false);

        spinner = view.findViewById(R.id.spinner);
        spinner1 = view.findViewById(R.id.spinner2);
        mode=view.findViewById(R.id.button4);
        add=view.findViewById(R.id.button9);
        delete=view.findViewById(R.id.button10);
        money=view.findViewById(R.id.money);
        detail=view.findViewById(R.id.detail);
        detail.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        detail.setGravity(Gravity.TOP);
        Global global=(Global)getActivity().getApplicationContext();

        list.add(inSideList);
        for (int i=0;i<lunch2.length;i++){
            for(int j=0;j<lunch2[i].length;j++){
                list.get(i).add(lunch2[i][j]);
            }
            list.add(new ArrayList<>());
        }


        ArrayAdapter<String> lunchList = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, lunch);
                lunchList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(lunchList);
                spinner.setOnItemSelectedListener(selectListener);
                ArrayAdapter<String> lunch1List = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, lunch1);
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                spinner1.setOnItemSelectedListener(selectListener1);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("新增分類");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newstring = editText.getText().toString();
                        for(int i=0;i<list.get(pos).size();i++){
                           // System.out.println(pos);
                            if(newstring.equals(list.get(pos).get(i))){
                                Toast.makeText(getActivity(),"此類別已存在",Toast.LENGTH_LONG).show();
                                editText.setText("");
                                return;
                            }
                        }
                        if(!newstring.equals("")){
                            list.get(pos).add((newstring));
                            spinner1.setSelection(list.get(pos).size()-1);
                            Toast.makeText(getActivity(),"新增成功",Toast.LENGTH_LONG).show();
                            editText.setText("");
                        }

                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("刪除分類");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        boolean check=true;
                        String newstring = editText.getText().toString();
                        if(newstring.equals("無")){
                            Toast.makeText(getActivity(),"沒有類別可以刪除",Toast.LENGTH_LONG).show();
                        }else {
                            for(int i=0;i< list.get(pos).size();i++){
                                if(newstring.equals(list.get(pos).get(i))){
                                    if(!newstring.equals("")){
                                        check=false;
                                        list.get(pos).remove((i));
                                        if(i==0){
                                            spinner1.setSelection(1);
                                        }else {
                                            spinner1.setSelection(0);
                                        }
                                        Toast.makeText(getActivity(),"刪除成功",Toast.LENGTH_LONG).show();
                                        editText.setText("");
                                    }
                                }
                            }
                            if(check==true){
                                Toast.makeText(getActivity(),"查無此類別名稱",Toast.LENGTH_LONG).show();
                                editText.setText("");
                            }
                        }
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });



        mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("新增常用");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newstring = editText.getText().toString();
                        money1=money.getText().toString();
                        System.out.println(newstring+Maincategory+Subcategory+money1+global.accountname);
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Mysqlqrcode con = new Mysqlqrcode();
                                int q=con.insertmodleData(newstring,Maincategory,Subcategory,money1,global.accountname,"支出");
                                if(q==0){
                                    Looper.prepare();
                                    Toast toast=Toast.makeText(getActivity(), "輸入成功", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER,0,0);
                                    toast.show();
                                    Looper.loop();
                                    money.setText(null);detail.setText(null);
                                    editText.setText("");
                                }else {
                                    Looper.prepare();
                                    Toast toast=Toast.makeText(getActivity(), "輸入失敗", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER,0,0);
                                    toast.show();
                                    Looper.loop();
                                }
                            }
                        }).start();
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });

        return view;

    }


    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode==KeyEvent.KEYCODE_BACK){
            Intent intent = new Intent(getActivity(), account.class);
            startActivity(intent);
        }
        return false;
    }

    private AdapterView.OnItemSelectedListener selectListener = new AdapterView.OnItemSelectedListener(){

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            pos = spinner.getSelectedItemPosition();
            if(lunch[pos]==lunch[0]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(0));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=(String) spinner.getSelectedItem();
            }else if (lunch[pos]==lunch[1]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(1));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=(String) spinner.getSelectedItem();

            }else if (lunch[pos]==lunch[2]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(2));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=lunch[pos];
            }else if (lunch[pos]==lunch[3]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(3));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=lunch[pos];
            }else if (lunch[pos]==lunch[4]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(4));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=lunch[pos];
            }else if (lunch[pos]==lunch[5]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(5));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=lunch[pos];
            }else if (lunch[pos]==lunch[6]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(6));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=lunch[pos];
            }else if (lunch[pos]==lunch[7]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(7));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=lunch[pos];
            }else if (lunch[pos]==lunch[8]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(8));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=lunch[pos];
            }else if (lunch[pos]==lunch[9]){
                if(list.get(pos).size()>1&&list.get(pos).get(0).equals("無")){
                    list.get(pos).remove(0);
                    spinner.setSelection(pos);
                }
                ArrayAdapter<String> lunch1List = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list.get(9));
                lunch1List.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner1.setAdapter(lunch1List);
                Maincategory=lunch[pos];
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    private AdapterView.OnItemSelectedListener selectListener1 = new AdapterView.OnItemSelectedListener(){

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            Subcategory= (String) spinner1.getSelectedItem();
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

}

