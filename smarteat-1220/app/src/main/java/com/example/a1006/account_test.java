package com.example.a1006;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class account_test extends AppCompatActivity {
    RecyclerView account_RV;
    String[][] data = {{"薪資", "公司發薪"}, {"交通工具", "公車"}};
    int[] money = {123, 456};

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.account);
        account_RV = (RecyclerView) findViewById(R.id.account_RV);

        initRV_data();
    }

    private void initRV_data(){
        account_RV.setLayoutManager(new LinearLayoutManager(this));

    }
}
