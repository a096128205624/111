package com.example.a1006;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class addwish extends AppCompatActivity {

    DatePickerDialog.OnDateSetListener dateSetListener;
    public Boolean end=true;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addwish);
        TextView textView1, textView2, textView3;
        TextView startdate = findViewById(R.id.date);
        TextView enddate = findViewById(R.id.end);
        EditText name = findViewById(R.id.name);
        EditText cost = findViewById(R.id.cost);
        TextView add=findViewById(R.id.add);
        Global global=(Global)getApplicationContext();
        Calendar cal=Calendar.getInstance();
        int year=cal.get(Calendar.YEAR);
        int month=cal.get(Calendar.MONTH);
        int day=cal.get(Calendar.DATE);

        //start日期
        String nowdate = year + "/" + (month+1) + "/" + day;
        startdate.setText(nowdate);
        startdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateSetListener=new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        String date1 = year + "/" + String.format("%02d", (month+1)) + "/" + String.format("%02d", day);
                        startdate.setText(date1);
                    }
                };

                DatePickerDialog dialog=new DatePickerDialog(addwish.this, dateSetListener, year,month,day);
                dialog.show();


            }
        });

        //enddate
        enddate.setText(nowdate);
        enddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateSetListener=new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        String date2 = year + "/" + String.format("%02d", (month+1)) + "/" + String.format("%02d", day);
                        enddate.setText(date2);
                    }
                };

                DatePickerDialog dialog=new DatePickerDialog(addwish.this, dateSetListener, year,month,day);
                dialog.show();

            }
        });

        //新增到資料庫
//        ArrayList<wishdata>  wishdataArrayList=new ArrayList<wishdata>();
        add.setOnClickListener(new View.OnClickListener() {
            SimpleDateFormat df = new SimpleDateFormat("yyyy/mm/dd");
            Date date1,date2;
            @Override
            public void onClick(View v) {
                String start=startdate.getText().toString();
                String wishname = name.getText().toString();
                String wishcost = cost.getText().toString();
                String wishdeadline = enddate.getText().toString();
                String save;

                long calday=0;
                int price =Integer.parseInt(wishcost);

                try {
                    date1=df.parse(start);
                    date2=df.parse(wishdeadline);
                    calday = (date1.getTime()-date2.getTime())/(24*60*60*1000)>0 ? (date1.getTime()-date2.getTime())/(24*60*60*1000):
                            (date2.getTime()-date1.getTime())/(24*60*60*1000);
                    if (calday==0){
                        calday=1;
                    }
                    System.out.println("相差:"+calday); //相差天數
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                // int x=(int)calday;
                save=String.valueOf(price/calday);//剩餘金額/每日扣款=需要存的天數=>更新結束日期+當天
                if ((date1.getTime()-date2.getTime())>0){
                    Toast toast=Toast.makeText(addwish.this, "結束時間勿小於開始時間", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();
                } else if (calday>price) {
                    Toast toast=Toast.makeText(addwish.this, "結束日期太久", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();
                }
                else {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Mysqlqrcode con = new Mysqlqrcode();
                            String order=String.valueOf(con.getwishrank(global.accountname));
                            System.out.println("順序"+order);
                            if (order==null) {
                                order = String.valueOf(0);
                            }

                            int o= Integer.valueOf(order);
                            if (o<=0){
                                o=1;
                            }else {
                                o += 1;
                            }
                            int q=con.insertwish(global.accountname,wishname,start,wishcost,wishdeadline,"0",String.valueOf(o));
                            if(q==0){
                                Looper.prepare();
                                Toast toast=Toast.makeText(addwish.this, "輸入成功", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER,0,0);
                                toast.show();
                                Intent intent = new Intent(addwish.this, wish.class);
                                startActivity(intent);
                                Looper.loop();
                            }else {
                                Looper.prepare();
                                Toast toast=Toast.makeText(addwish.this, "輸入失敗", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER,0,0);
                                toast.show();
                                Intent intent = new Intent(addwish.this, wish.class);
                                startActivity(intent);
                                Looper.loop();
                            }
                            end=false;
                        }
                    }).start();


                }
            }
        });
    }

}