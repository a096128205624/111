package com.example.a1006;


import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.ArrayList;

public class tab extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager2 viewPager;
    TabItem modle,income,expend;
    pageAdapter pageAdapter;
    Global global;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab);
        final String[] name={"常用","支出","收入"};

        tabLayout = findViewById(R.id.Tablayout);
        viewPager = findViewById(R.id.viewpager);
        modle = findViewById(R.id.modle);
        expend = findViewById(R.id.expend);
        income = findViewById(R.id.income);
        global=(Global)getApplicationContext();
//        global.setPositionIN(null);
//        global.setPositionEX(null);
        pageAdapter = new pageAdapter(this, tabLayout.getTabCount());
        int position;
        Intent intent=getIntent();
        position=intent.getIntExtra("postion",0);
        viewPager.setAdapter(pageAdapter);

        switch (position){
            case 0:
                viewPager.setCurrentItem(0);
                break;
            case 1:
                viewPager.setCurrentItem(1);
                break;
            case 2:
                viewPager.setCurrentItem(2);
                break;

        }
        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(tabLayout, viewPager, true, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                tab.setText(name[position]);

            }
        });

        tabLayoutMediator.attach();

    }
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode==KeyEvent.KEYCODE_BACK){
            Global global=(Global)this.getApplicationContext();
            global.setCode(null);
            global.setMoney(0);
            global.setDate("");
            global.setUniformnumbers("");
            global.setDetail("");
            Intent intent = new Intent(this, account.class);
            startActivity(intent);
        }
        return false;
    }
}