package com.example.a1006;

import static android.os.SystemClock.sleep;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.provider.MediaStore;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class editexpendrecord extends AppCompatActivity {
    TextView tdate,ttime,enter,deletedata;
    private Spinner spinner;
    EditText code,money,detail,codee;
    Button add,delete;
    String Maincategory,moneybuffer, codebuffer,detailbuffer,position;
    boolean end=false;
    ArrayList<String> Industrycode=new ArrayList<String>();
    ArrayList<String> category=new ArrayList<String>();

    LinearLayout qrcodebt;
    DatePickerDialog.OnDateSetListener dateSetListener;
    TimePickerDialog.OnTimeSetListener timeSetListener;
    @RequiresApi(api = Build.VERSION_CODES.M)
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.editeexpendlayout);
        View view1=getLayoutInflater().inflate(R.layout.addview,null);
        ttime=findViewById(R.id.time);
        tdate=findViewById(R.id.date);
        spinner = findViewById(R.id.spinner);
        enter=findViewById(R.id.button4);
        add=findViewById(R.id.button9);
        delete=findViewById(R.id.button10);
        qrcodebt=findViewById(R.id.qrcodebt);
        Calendar cal=Calendar.getInstance();
        int year=cal.get(Calendar.YEAR);
        int month=cal.get(Calendar.MONTH);
        int day=cal.get(Calendar.DATE);
        int hour=cal.get(Calendar.HOUR_OF_DAY);
        int min=cal.get(Calendar.MINUTE);
        code=findViewById(R.id.invoice2);
        codee=findViewById(R.id.invoice);
        money=findViewById(R.id.money);
        detail=findViewById(R.id.detail);
        detail.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        detail.setGravity(Gravity.TOP);
        Global global=(Global)this.getApplicationContext();
        ArrayList<modeexdata> exhistorydataArrayList=global.getExhistorydataArrayList();

        //設定現在日期 String.format("%02d", day)
        final String date = year + "/" + (month+1) + "/" + day;
        tdate.setText(date);
        //設定現在時間 (((hour+8)%24))
        String time = (hour) + ":" + String.format("%02d", min);
        ttime.setText(time);


        new Thread(new Runnable() {
            @Override
            public void run() {
                Mysqlqrcode con = new Mysqlqrcode();
                category= con.getExpendcategory(global.accountname);
                System.out.println(category.size());
                sleep(1000);
                end=true;

            }
        }).start();
        while (end!=true){
            ArrayAdapter<String> lunchList = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, category);
            lunchList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(lunchList);
        }
        Bundle bundle = this.getIntent().getExtras();
        String datasubcategory = bundle.getString("subcategory");
        String datamoney = bundle.getString("money");
        position=bundle.getString("position");
        String datatime = bundle.getString("time");
        String datadate = bundle.getString("date");
        for (int i=0;i<global.historydata.size();i++){
            if(global.historydata.get(i).maincategory.equals(datasubcategory) &&
                    global.historydata.get(i).date.equals(datadate) && global.historydata.get(i).time.equals(datatime) &&
                    global.historydata.get(i).money==Integer.valueOf(datamoney) ){
                tdate.setText(global.historydata.get(i).date);
                ttime.setText(global.historydata.get(i).time);
                money.setText(String.valueOf(global.historydata.get(i).money));
                for(int j=0;j<category.size();j++){
                    if (category.get(j).equals(global.historydata.get(i).maincategory)){
                        spinner.setSelection(j);
                    }
                }
                if(!global.historydata.get(i).code.equals("")){
                    codee.setText(global.historydata.get(i).code.substring(0,3));
                    code.setText(global.historydata.get(i).code.substring(3));
                }
                detail.setText(global.historydata.get(i).productname);
            }
        }
        end=false;
        if (global.positionEX!=null){
            System.out.println("position"+ global.positionEX);
            money.setText(exhistorydataArrayList.get(Integer.parseInt(global.positionEX)).moneyEx);
            for(int i=0;i<category.size();i++){
                if(exhistorydataArrayList.get(Integer.parseInt(global.positionEX)).maincategoryEx.equals(category.get(i))){
                    spinner.setSelection(i);
                }
            }
        }
        if(global.code!=null){
            String codenumber=global.code.substring(2);
            String codeenglish=global.code.substring(0,2);
            codee.setText(codeenglish);
            code.setText(codenumber);
            money.setText(String.valueOf(global.money));
            detail.setText(global.detail);
            String datearr[]=global.date.split("\\/");

            if(datearr[1].substring(0,1).equals("0")){
                datearr[1]=datearr[1].substring(1);
            }
            if(datearr[2].substring(0,1).equals("0")){
                datearr[2]=datearr[2].substring(1);
            }

            String datebuffer = String.valueOf(Integer.valueOf(datearr[0])+1911 )+ "/"+ datearr[1] + "/" +datearr[2];
            tdate.setText(datebuffer);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Mysqlqrcode con = new Mysqlqrcode();
                    Industrycode= con.getIndustrycode(global.Uniformnumbers);
                    System.out.println("j:"+Industrycode.size());
                    end=true;
                }
            }).start();
            while (end!=true){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            for(int i=0;i<Industrycode.size();i++){
                System.out.println("i:"+Industrycode.get(i));
                if(Industrycode.get(i).equals("56") || Industrycode.get(i).equals("472")){
                    spinner.setSelection(0);
                }
                else if(Industrycode.get(i).equals("463")){
                    spinner.setSelection(1);
                }
                else if(Industrycode.get(i).equals("932") || Industrycode.get(i).equals("55")){
                    spinner.setSelection(2);
                }
                else if(Industrycode.get(i).equals("471") || Industrycode.get(i).equals("473")){
                    spinner.setSelection(3);
                }
                else if(Industrycode.get(i).equals("475") ){
                    spinner.setSelection(4);
                }
                else if(Industrycode.get(i).equals("476") ){
                    spinner.setSelection(5);
                }
                else if(Industrycode.get(i).equals("474") ){
                    spinner.setSelection(6);
                }

            }
        }
        codee.setTransformationMethod(new AllCapTransformationMethod(true));
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(editexpendrecord.this);
                alertDialog.setTitle("新增分類");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newstring = editText.getText().toString();
                        for(int i=0;i<category.size();i++){
                            if(newstring.equals(category.get(i))){
                                Toast.makeText(editexpendrecord.this,"此類別已存在",Toast.LENGTH_LONG).show();
                                editText.setText("");
                                return;
                            }
                        }
                        if(!newstring.equals("")){
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Mysqlqrcode con = new Mysqlqrcode();
                                    con.insertExpendcategory(newstring,global.accountname);
                                }
                            }).start();
                            category.add(newstring);
                            Toast.makeText(editexpendrecord.this,"新增成功",Toast.LENGTH_LONG).show();
                            editText.setText("");
                        }

                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(editexpendrecord.this);
                alertDialog.setTitle("刪除分類");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        boolean check=true;
                        String newstring = editText.getText().toString();
                        for(int i=0;i< category.size();i++){
                            if(newstring.equals(category.get(i))){
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Mysqlqrcode con = new Mysqlqrcode();
                                        con.deletecategory(newstring,global.accountname);
                                    }
                                }).start();
                                check=false;
                                category.remove((i));
                                if(i==0){
                                    spinner.setSelection(1);
                                }else {
                                    spinner.setSelection(0);
                                }
                                Toast.makeText(editexpendrecord.this,"刪除成功",Toast.LENGTH_LONG).show();
                                editText.setText("");
                            }
                        }
                        if(check==true){
                            Toast.makeText(editexpendrecord.this,"查無此類別名稱",Toast.LENGTH_LONG).show();
                            editText.setText("");
                        }
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        //日期選擇
        tdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal=Calendar.getInstance();
                int year=cal.get(Calendar.YEAR);
                int month=cal.get(Calendar.MONTH);
                int day=cal.get(Calendar.DATE);


                dateSetListener=new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        String date = year + "/" + (month+1) + "/" + day;
                        tdate.setText(date);
                    }
                };

                DatePickerDialog dialog=new DatePickerDialog(editexpendrecord.this, dateSetListener, year,month,day);
                dialog.show();


            }
        });
        //時間選擇
        ttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal=Calendar.getInstance();
                int hour=cal.get(Calendar.HOUR_OF_DAY);
                int min=cal.get(Calendar.MINUTE);

                timeSetListener=new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int min) {
                        Log.d("onTimeSet: hh/mm: ", +hour + "/" + min);
                        String time = (hour) + ":" + String.format("%02d", min);
                        ttime.setText(time);
                    }
                };

                TimePickerDialog dialog=new TimePickerDialog(
                        editexpendrecord.this,
                        timeSetListener,
                        hour,min, DateFormat.is24HourFormat(editexpendrecord.this)
                );
                dialog.show();

            }

        });
        qrcodebt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(editexpendrecord.this, qrcode.class);
                startActivity(intent);
            }
        });
        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String datebuffer=tdate.getText().toString();
                String timebuffer=ttime.getText().toString();
                moneybuffer=money.getText().toString();
                detailbuffer=detail.getText().toString();
                Maincategory=(String) spinner.getSelectedItem();
                codebuffer=(String.valueOf(codee.getText())+String.valueOf(code.getText()));
                if (moneybuffer.length()>1&&moneybuffer.startsWith("0")){
                    moneybuffer=moneybuffer.replaceFirst("0","");
                }

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Mysqlqrcode con = new Mysqlqrcode();
                        con.deleteexpenddata(datadate,datatime,datamoney,global.accountname);
                        con.insertExpendData(codebuffer, moneybuffer,Maincategory,detailbuffer,datebuffer,timebuffer,global.accountname);

                        Looper.prepare();
                        Toast toast=Toast.makeText(editexpendrecord.this, "修改成功", Toast.LENGTH_LONG);
                        toast.show();
                        Intent intent = new Intent(editexpendrecord.this, account.class);
                        startActivity(intent);
                        Looper.loop();
                    }
                }).start();
                tdate.setText(date);
                ttime.setText(time);
                code.setText(null);codee.setText(null);
                money.setText(null);detail.setText(null);
            }
        });
        deletedata=findViewById(R.id.delete);
        deletedata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Mysqlqrcode con = new Mysqlqrcode();
                        con.deleteexpenddata(datadate,datatime,datamoney,global.accountname);
                        Looper.prepare();
                        Toast toast=Toast.makeText(editexpendrecord.this, "刪除成功", Toast.LENGTH_LONG);
                        toast.show();
                        Intent intent = new Intent(editexpendrecord.this, account.class);
                        startActivity(intent);
                        Looper.loop();
                    }
                }).start();
            }
        });
    }
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode==KeyEvent.KEYCODE_BACK){
            if(position.equals("2")){
                Intent intent = new Intent(editexpendrecord.this, calendar.class);
                startActivity(intent);
            }else{
                Intent intent = new Intent(editexpendrecord.this, account.class);
                startActivity(intent);
            }

        }
        return false;
    }
}