package com.example.a1006;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class foodcommend extends AppCompatActivity {
    private ProgressBar mLoadingBar;
    ListView store;
    LocationManager lm;
    SeekBar seekBar;
    Button submit;
    String commadStr;
    TextView rangetext;
    int mpacl = 11;
    boolean end = false;
    int range = 1000;
    ArrayList<store> storebuffer = new ArrayList<store>();
    ArrayList<store> totalstore = new ArrayList<store>();
    ArrayList<store> totalstore2 = new ArrayList<store>();
    Calendar c = Calendar.getInstance();
    int hour = c.get(Calendar.HOUR_OF_DAY);
    int minute = c.get(Calendar.MINUTE);
    int timesum = hour * 60 + minute;
    int weekday = c.get(Calendar.DAY_OF_WEEK);

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.foodcommend);
        mLoadingBar = (ProgressBar) findViewById(R.id.progressbar);
        store = findViewById(R.id.wish);
        Global globalaccountname = (Global) getApplicationContext();
        String account = globalaccountname.getAccountname();
        commadStr = LocationManager.GPS_PROVIDER;
        seekBar = findViewById(R.id.range);
        rangetext = findViewById(R.id.rangetext);
        rangetext.setText("1Km");
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                rangetext.setText(i + "Km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                rangetext.setText(String.valueOf(seekBar.getProgress()) + "Km");
                range = seekBar.getProgress() * 1000;
            }
        });
        submit = findViewById(R.id.submit);

        lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        mLoadingBar.setVisibility(View.VISIBLE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = lm.getLastKnownLocation(commadStr);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println(range);
                end=false;

                if(location!=null) {
                    mLoadingBar.setVisibility(View.GONE);

                    Geocoder gc = new Geocoder(getBaseContext(), Locale.TRADITIONAL_CHINESE);
                    List<Address> lstAddress = null;
                    try {
                        lstAddress = gc.getFromLocation(location.getLatitude(),location.getLongitude(), 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String returnAddress=lstAddress.get(0).getAddressLine(0);

                    String buffer=returnAddress.substring(8,11);


                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            storebuffer.clear();
                            totalstore.clear();
                            totalstore2.clear();
                            Mysqlqrcode con = new Mysqlqrcode();
                            storebuffer= con.getData(buffer);

                            for(int i=0;i<storebuffer.size();i++){
                                double sum=algorithm(location.getLatitude(),location.getLongitude(),storebuffer.get(i).lat,storebuffer.get(i).lng);
                                if(sum<range){
                                    storebuffer.get(i).sum= (float) sum;
                                    totalstore.add(storebuffer.get(i));
                                }
                            }
                            Collections.sort(totalstore, new storesort());

                            for(int i=0;i<totalstore.size();i++) {
                                totalstore.get(i).time = totalstore.get(i).time.replaceAll("\\[", "");
                                totalstore.get(i).time = totalstore.get(i).time.replaceAll("\\]", "");
                                totalstore.get(i).time = totalstore.get(i).time.replaceAll("\\'", ";");
                                String[] timeall = totalstore.get(i).time.split("\\;");
                                ArrayList<String> tradetime = new ArrayList<String>();
                                for (int j = 1; j < timeall.length; j += 2) {
                                    tradetime.add(timeall[j]);
                                }
                                String tmp="";
                                if(weekday-1==0){
                                    weekday=6;
                                    tmp = tradetime.get(weekday).substring(4);
                                }
                                else{
                                    tmp = tradetime.get(weekday - 1).substring(4);
                                }

                                tmp=tmp.replaceAll("\\s","");

                                if(tmp.equals("24小時營業")) {
                                    totalstore2.add(totalstore.get(i));
                                }
                                if (!tmp.equals("休息")) {
                                    if(!tmp.equals("24小時營業")){

                                        String[] buffer=tmp.split("\\,");

                                        for(int j=0;j<buffer.length;j++){

                                            int[] timerange=new int[2];
                                            String[] buffer2=buffer[j].split("\\–");
                                            for(int q=0;q<buffer2.length;q++){

                                                String[] buffer3=buffer2[q].split("\\:");


                                                timerange[q]=Integer.valueOf(buffer3[0])*60+Integer.valueOf(buffer3[1]);
                                            }

                                            if(timesum>=timerange[0] && timesum<=timerange[1]){
                                                totalstore2.add(totalstore.get(i));

                                            }
                                        }
                                    }

                                }

                            }end=true;

                        }
                    }).start();


                    while (end!=true){
                        BaseAdapter adapter=new BaseAdapter() {
                            @Override
                            public int getCount() {
                                return totalstore2.size();
                            }

                            @Override
                            public Object getItem(int i) {
                                return i;
                            }

                            @Override
                            public long getItemId(int i) {
                                return i;
                            }

                            @Override
                            public View getView(int i, View view, ViewGroup viewGroup) {
                                View layout=View.inflate(foodcommend.this,R.layout.stroelayout,null);
                                TextView address=layout.findViewById(R.id.phone);
                                TextView name=layout.findViewById(R.id.name);
                                TextView range=layout.findViewById(R.id.range);
                                TextView map=layout.findViewById(R.id.map);
                                TextView phone=layout.findViewById(R.id.phine);
                                ImageView time=layout.findViewById(R.id.time);

                                map.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=(%s)",totalstore2.get(i).name);
                                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                                        intent.setPackage("com.google.android.apps.maps");
                                        startActivity(intent);
                                    }
                                });

                                phone.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Uri uri=Uri.parse("tel:"+String.valueOf(totalstore2.get(i).phone));
                                        Intent intent=new Intent(Intent.ACTION_DIAL,uri);
                                        startActivity(intent);
                                    }
                                });

                                double sum=algorithm(location.getLatitude(),location.getLongitude(),totalstore2.get(i).lat,totalstore2.get(i).lng);
                                name.setText((i + 1) + ". " + totalstore2.get(i).name);
                                address.setText(totalstore2.get(i).address);
                                range.setText("距離: " +String.format("%.2f",sum/1000) + " km");
                                time.setImageResource(R.drawable.open);
                                return layout;

                            }
                        };
                        store.setAdapter(adapter);

                    }
                    store.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent();
                            intent.setClass(foodcommend.this,storedetail.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("discount",String.valueOf(totalstore2.get(position).discount));
                            bundle.putString("address",totalstore2.get(position).address);
                            bundle.putString("storename",totalstore2.get(position).name);
                            intent.putExtras(bundle);   // 記得put進去，不然資料不會帶過去哦
                            startActivity(intent);
                        }
                    });
                    if(totalstore2.size()==0){
                        AlertDialog.Builder dialog = new AlertDialog.Builder(foodcommend.this);
                        dialog.setTitle("提示");
                        dialog.setMessage("目前沒有營業的店家");

                        dialog.setPositiveButton("YES",new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                // TODO Auto-generated method stub
                                Toast.makeText(foodcommend.this, "確定",Toast.LENGTH_SHORT).show();
                            }

                        });
                        dialog.show();
                    }


                }

            }
        });

    }


    private static double rad(double d) {
        return d * Math.PI / 180.00; // 角度轉換成弧度
    }
    public static double algorithm(double mylng, double mylat, double lng, double lat) {
        double Lat1 = rad(mylat); // 緯度
        double Lat2 = rad(lat);
        double a = Lat1 - Lat2;// 兩點緯度之差
        double b = rad(mylng) - rad(lng); // 經度之差
        double s = 2 * Math.asin(Math
                .sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(Lat1) * Math.cos(Lat2) * Math.pow(Math.sin(b / 2), 2)));// 計算兩點距離的公式
        s = s * 6378137.0;// 弧長乘地球半徑（半徑為米）
        s = Math.round(s * 10000d) / 10000d;// 精確距離的數值

        return s;
    }
    public LocationListener ln =new LocationListener() {
        @Override
        public void onLocationChanged(@NonNull Location location) {

            Geocoder gc = new Geocoder(getBaseContext(), Locale.TRADITIONAL_CHINESE);
            List<Address> lstAddress = null;
            try {
                lstAddress = gc.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String returnAddress = lstAddress.get(0).getAddressLine(0);

        }

        @Override
        public void onProviderEnabled(@NonNull String provider) {

        }

        @Override
        public void onProviderDisabled(@NonNull String provider) {

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
}
