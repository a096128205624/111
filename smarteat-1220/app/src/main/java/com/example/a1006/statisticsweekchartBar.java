package com.example.a1006;

import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class statisticsweekchartBar extends AppCompatActivity  {
    public BarChart chart;
    String[] values;

    ImageButton rimageButton,limageButton;
    TextView textView,change;
    ArrayList<historydata> historydataArrayList=new ArrayList<historydata>();
    ArrayList<PieEntry> categorypiechart=new ArrayList<PieEntry>();
    ArrayList<String> excategory=new ArrayList<String>();
    ArrayList<String> incategory=new ArrayList<String>();
    int[] excategorysum,incategorysum;
    Calendar c = Calendar.getInstance();
    int endyear = c.get(Calendar.YEAR);
    int firstyear=endyear;
    int endmonth = c.get(Calendar.MONTH)+1;
    int firstmonth=endmonth;
    int ndate = c.get(Calendar.DATE);
    int nweek = c.get(Calendar.DAY_OF_WEEK);
    int fistweek,endweek;
    boolean changebuffer=true;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.barchart);
        chart = findViewById(R.id.BarChart);

        rimageButton=findViewById(R.id.minusmonth2);
        limageButton=findViewById(R.id.addmonth2);
        textView =findViewById(R.id.time2);
        fistweek=(ndate-nweek+1);
        endweek=(fistweek+7-1);
        textView.setText(String.valueOf(endyear)+" 年 "+endmonth+" 月 "+fistweek+" 到 "+endweek+" 日 ");
        change=findViewById(R.id.textView4);
        ArrayList<historydata> historydatabuffer=new ArrayList<historydata>();
        Global globalaccountname=(Global)getApplicationContext();
        String accountname=globalaccountname.getAccountname();

        new Thread(new Runnable() {
            @Override
            public void run() {
                Mysqlqrcode con = new Mysqlqrcode();
                historydataArrayList= con.gethistoryData(accountname);
                excategory= con.getExpendcategory(accountname);
                incategory= con.getIncomecategory(accountname);
                Global global=(Global) getApplicationContext();
                global.setHistorydata(historydataArrayList);
                excategorysum=new int[excategory.size()];
                incategorysum=new int[incategory.size()];
                for(int i=0;i<excategorysum.length;i++){
                    excategorysum[i]=0;
                }
                for(int i=0;i<incategorysum.length;i++){
                    incategorysum[i]=0;
                }
                for(int i=0;i<historydataArrayList.size();i++){
                    String nowday[]=historydataArrayList.get(i).date.split("\\/");
                    if(nowday[0].equals(String.valueOf(endyear)) && nowday[1].equals(String.valueOf(endmonth)) && Integer.valueOf(nowday[2])>=fistweek && Integer.valueOf(nowday[2])<=endweek){
                        historydatabuffer.add(historydataArrayList.get(i));
                    }
                    if(nowday[0].equals(String.valueOf(endyear)) && nowday[1].equals(String.valueOf(endmonth)) && Integer.valueOf(nowday[2])>=fistweek && Integer.valueOf(nowday[2])<=endweek){
                        if(historydataArrayList.get(i).type.equals("expend")){
                            System.out.println(historydataArrayList.get(i).date+"------"+historydataArrayList.get(i).maincategory);
                            for (int j=0;j<excategory.size();j++){
                                if(historydataArrayList.get(i).maincategory.equals(excategory.get(j))){
                                    excategorysum[j]=excategorysum[j]+Integer.valueOf(historydataArrayList.get(i).money);
                                }
                            }
                        }
                        else if(historydataArrayList.get(i).type.equals("income")){
                            for (int j=0;j<incategory.size();j++){
                                if(historydataArrayList.get(i).maincategory.equals(incategory.get(j))){
                                    incategorysum[j]=incategorysum[j]+Integer.valueOf(historydataArrayList.get(i).money);
                                }
                            }
                        }
                    }
                }
                Looper.prepare();
                show();
                Looper.loop();
            }
        }).start();
        limageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int max=getMonthLastDay(endyear,endmonth);
                endweek+=7;
                fistweek=endweek-6;
                firstmonth=endmonth;
                firstyear=endyear;
                if(endweek>max){
                    endweek=endweek-max;
                    fistweek=endweek-6;
                    endmonth++;
                    firstmonth++;
                    if(fistweek<=0){
                        firstmonth--;
                        fistweek=fistweek+max;
                    }
                    if(endmonth>12){
                        endyear++;
                        endmonth=1;
                    }
                    if(firstmonth>12){
                        firstyear++;
                        firstmonth=1;
                    }
                }
                textView.setText(String.valueOf(firstyear)+" 年 "+(firstmonth)+" 月 "+fistweek+" 日 到 "+String.valueOf(endyear)+" 年 "+(endmonth)+" 月 "+endweek+" 日 ");
                for(int i=0;i<excategorysum.length;i++){
                    excategorysum[i]=0;
                }
                for(int i=0;i<incategorysum.length;i++){
                    incategorysum[i]=0;
                }
                if(endmonth==firstmonth){
                    for(int i=0;i<historydataArrayList.size();i++){
                        String nowday[]=historydataArrayList.get(i).date.split("\\/");
                        if(nowday[0].equals(String.valueOf(firstyear)) && nowday[1].equals(String.valueOf(firstmonth)) && (Integer.valueOf(nowday[2])>=fistweek && Integer.valueOf(nowday[2])<=endweek)){
                            if(historydataArrayList.get(i).type.equals("expend")){
                                System.out.println(historydataArrayList.get(i).date+"------"+historydataArrayList.get(i).maincategory);
                                for (int j=0;j<excategory.size();j++){
                                    if(historydataArrayList.get(i).maincategory.equals(excategory.get(j))){
                                        excategorysum[j]=excategorysum[j]+Integer.valueOf(historydataArrayList.get(i).money);
                                    }
                                }
                            }
                            else if(historydataArrayList.get(i).type.equals("income")){
                                for (int j=0;j<incategory.size();j++){
                                    if(historydataArrayList.get(i).maincategory.equals(incategory.get(j))){
                                        incategorysum[j]=incategorysum[j]+Integer.valueOf(historydataArrayList.get(i).money);
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    for(int i=0;i<historydataArrayList.size();i++){
                        String nowday[]=historydataArrayList.get(i).date.split("\\/");
                        if((nowday[0].equals(String.valueOf(firstyear)) && nowday[1].equals(String.valueOf(firstmonth)) && (Integer.valueOf(nowday[2])>=fistweek && Integer.valueOf(nowday[2])<=max))||
                                (nowday[0].equals(String.valueOf(endyear)) && nowday[1].equals(String.valueOf(endmonth)) && (Integer.valueOf(nowday[2])>=1 && Integer.valueOf(nowday[2])<=endweek))){
                            if(historydataArrayList.get(i).type.equals("expend")){
                                System.out.println(historydataArrayList.get(i).date+"------"+historydataArrayList.get(i).maincategory);
                                for (int j=0;j<excategory.size();j++){
                                    if(historydataArrayList.get(i).maincategory.equals(excategory.get(j))){
                                        excategorysum[j]=excategorysum[j]+Integer.valueOf(historydataArrayList.get(i).money);
                                    }
                                }
                            }
                            else if(historydataArrayList.get(i).type.equals("income")){
                                for (int j=0;j<incategory.size();j++){
                                    if(historydataArrayList.get(i).maincategory.equals(incategory.get(j))){
                                        incategorysum[j]=incategorysum[j]+Integer.valueOf(historydataArrayList.get(i).money);
                                    }
                                }
                            }
                        }
                    }
                }

                show();

            }
        });
        rimageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fistweek-=7;
                endmonth=firstmonth;
                endyear=firstyear;
                if(fistweek<=0){
                    firstmonth--;
                    int max=getMonthLastDay(firstyear,firstmonth);
                    fistweek=fistweek+max;
                }
                int max=getMonthLastDay(firstyear,firstmonth);
                endweek=fistweek+6;
                if(endweek>=max){
                    endmonth=firstmonth+1;
                    endweek=endweek-max;
                }
                if(firstmonth<=0){
                    firstmonth=12;
                    firstyear--;
                }
                if(endmonth<=0){
                    endmonth=12;
                    endyear--;
                }

                textView.setText(String.valueOf(firstyear)+" 年 "+(firstmonth)+" 月 "+fistweek+" 日 到 "+String.valueOf(endyear)+" 年 "+(endmonth)+" 月 "+endweek+" 日 ");
                for(int i=0;i<excategorysum.length;i++){
                    excategorysum[i]=0;
                }
                for(int i=0;i<incategorysum.length;i++){
                    incategorysum[i]=0;
                }
                if(endmonth==firstmonth){
                    for(int i=0;i<historydataArrayList.size();i++){
                        String nowday[]=historydataArrayList.get(i).date.split("\\/");
                        if(nowday[0].equals(String.valueOf(firstyear)) && nowday[1].equals(String.valueOf(firstmonth)) && (Integer.valueOf(nowday[2])>=fistweek && Integer.valueOf(nowday[2])<=endweek)){
                            if(historydataArrayList.get(i).type.equals("expend")){
                                System.out.println(historydataArrayList.get(i).date+"------"+historydataArrayList.get(i).maincategory);
                                for (int j=0;j<excategory.size();j++){
                                    if(historydataArrayList.get(i).maincategory.equals(excategory.get(j))){
                                        excategorysum[j]=excategorysum[j]+Integer.valueOf(historydataArrayList.get(i).money);
                                    }
                                }
                            }
                            else if(historydataArrayList.get(i).type.equals("income")){
                                for (int j=0;j<incategory.size();j++){
                                    if(historydataArrayList.get(i).maincategory.equals(incategory.get(j))){
                                        incategorysum[j]=incategorysum[j]+Integer.valueOf(historydataArrayList.get(i).money);
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    for(int i=0;i<historydataArrayList.size();i++){
                        String nowday[]=historydataArrayList.get(i).date.split("\\/");
                        if((nowday[0].equals(String.valueOf(firstyear)) && nowday[1].equals(String.valueOf(firstmonth)) && (Integer.valueOf(nowday[2])>=fistweek && Integer.valueOf(nowday[2])<=max))||
                                (nowday[0].equals(String.valueOf(endyear)) && nowday[1].equals(String.valueOf(endmonth)) && (Integer.valueOf(nowday[2])>=1 && Integer.valueOf(nowday[2])<=endweek))){
                            if(historydataArrayList.get(i).type.equals("expend")){
                                System.out.println(historydataArrayList.get(i).date+"------"+historydataArrayList.get(i).maincategory);
                                for (int j=0;j<excategory.size();j++){
                                    if(historydataArrayList.get(i).maincategory.equals(excategory.get(j))){
                                        excategorysum[j]=excategorysum[j]+Integer.valueOf(historydataArrayList.get(i).money);
                                    }
                                }
                            }
                            else if(historydataArrayList.get(i).type.equals("income")){
                                for (int j=0;j<incategory.size();j++){
                                    if(historydataArrayList.get(i).maincategory.equals(incategory.get(j))){
                                        incategorysum[j]=incategorysum[j]+Integer.valueOf(historydataArrayList.get(i).money);
                                    }
                                }
                            }
                        }
                    }
                }
                show();

            }
        });
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(changebuffer==true){
                    changebuffer=false;
                    change.setText("切換支出");
                    show();
                }else {
                    changebuffer=true;
                    change.setText("切換收入");
                    show();
                }

            }
        });



    }

    public void show(){

        //設置最大值條目，超出之後不會有值
        chart.setMaxVisibleValueCount(60);
        //分別在x軸和y軸上進行縮放
        chart.setPinchZoom(true);
        //設置剩餘統計圖的陰影
        chart.setDrawBarShadow(false);
        //設置網格佈局
        chart.setDrawGridBackground(true);

        //通過自定義一個x軸標籤來實現2,015 有分割符符bug
        //ValueFormatter custom = new MyValueFormatter(" ");



        if(changebuffer==true){
            values=new String[excategory.size()];
            for(int i=0;i<excategory.size();i++){
                values[i]=excategory.get(i);
                System.out.println(values[i]);
            }
        }else{
            values=new String[incategory.size()];
            for(int i=0;i<incategory.size();i++){
                values[i]=incategory.get(i);
                System.out.println(values[i]);
            }

        }
        MyXFormatter formatter = new MyXFormatter(values);

        //獲取x軸線
        XAxis xAxis = chart.getXAxis();
        //設置x軸的顯示位置
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        //設置網格佈局
        xAxis.setDrawGridLines(true);
        //圖表將避免第一個和最後一個標籤條目被減掉在圖表或屏幕的邊緣
        xAxis.setAvoidFirstLastClipping(false);
        //繪製標籤  指x軸上的對應數值 默認true
        xAxis.setDrawLabels(true);
        xAxis.setValueFormatter(formatter);

        //縮放後x 軸數據重疊問題
        xAxis.setGranularityEnabled(true);

        //獲取右邊y標籤
        YAxis axisRight = chart.getAxisRight();
        axisRight.setStartAtZero(true);
        //獲取左邊y軸的標籤
        YAxis axisLeft = chart.getAxisLeft();
        //設置Y軸數值 從零開始
        axisLeft.setStartAtZero(true);
        chart.getAxisLeft().setDrawGridLines(false);

        //設置動畫時間
        chart.animateXY(600,600);
        chart.getLegend().setEnabled(true);
        getData();
        //設置柱形統計圖上的值
        chart.getData().setValueTextSize(10);
        for (IDataSet set : chart.getData().getDataSets()){
            set.setDrawValues(!set.isDrawValuesEnabled());
        }
    }
    public void getData(){

        ArrayList<BarEntry> values = new ArrayList<>();

        if(changebuffer==true){
            values.clear();
            //BarEntry barEntry = new BarEntry(Float.valueOf("2016"),Float.valueOf(String.valueOf(excategorysum[0])));
            for(int i=0;i<excategory.size();i++){
                String x=String.valueOf(2016+i);
                values.add(new BarEntry(Float.valueOf(x),Float.valueOf(String.valueOf(excategorysum[i]))));
                System.out.println(x);
            }



        }else {
            values.clear();
            BarEntry barEntry1 = new BarEntry(Float.valueOf("2016"),Float.valueOf(String.valueOf(incategorysum[0])));
            values.add(barEntry1);
        }






        BarDataSet set1;

        if (chart.getData() != null &&
                chart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) chart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            chart.getData().notifyDataChanged();
            chart.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(values, "類別圖表");
            set1.setColors(ColorTemplate.VORDIPLOM_COLORS);
            set1.setDrawValues(false);
            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);
            BarData data = new BarData(dataSets);
            chart.setData(data);
            chart.setFitBars(true);
        }

        //繪製圖表
        chart.invalidate();
    }
    public int getMonthLastDay(int year, int month) {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.YEAR, year);
        a.set(Calendar.MONTH, month - 1);
        a.set(Calendar.DATE, 1);//把日期设置为当月第一天
        a.roll(Calendar.DATE, -1);//日期回滚一天，也就是最后一天
        int maxDate = a.get(Calendar.DATE);
        return maxDate;
    }

}

