package com.example.a1006;

import android.app.Application;

public class Global extends Application {
    String code,editer,date,accountname;
    int money;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setEditer(String editer) {
        this.editer = editer;
    }

    public String getEditer() {
        return editer;
    }

    public void setAccountname(String accountname) {
        this.accountname = accountname;
    }

    public String getAccountname() {
        return accountname;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getMoney() {
        return money;
    }
}
