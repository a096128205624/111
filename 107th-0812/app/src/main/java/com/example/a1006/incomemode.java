package com.example.a1006;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link incomemode#newInstance} factory method to
 * create an instance of this fragment.
 */
public class incomemode extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private final String[] lunch = {"工作收入", "其他收入"};

    private List<String> all;
    public incomemode() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment tab2.
     */
    // TODO: Rename and change types and number of parameters
    public static incomemode newInstance(String param1, String param2) {
        incomemode fragment = new incomemode();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    TextView tdate,ttime;
    EditText money,detail;
    String money1,Maincategory;
    Spinner spinner;
    Button enter,add,delete,mode;

    DatePickerDialog.OnDateSetListener dateSetListener;
    TimePickerDialog.OnTimeSetListener timeSetListener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.incomemode, container, false);
        View view1=inflater.inflate(R.layout.addview,container,false);
        ttime=view.findViewById(R.id.time);
        spinner = view.findViewById(R.id.spinner3);
        tdate=view.findViewById(R.id.date);
        add=view.findViewById(R.id.button7);
        delete=view.findViewById(R.id.button8);
        enter=view.findViewById(R.id.button5);
        money=view.findViewById(R.id.money);
        detail=view.findViewById(R.id.detail);
        mode=view.findViewById(R.id.button12);
        Global global=(Global)getActivity().getApplicationContext();


        all=new ArrayList<String>();
        for (int i=0;i<lunch.length;i++){
            all.add(lunch[i]);
        }

        ArrayAdapter<String> lunchList = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, all);
        lunchList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(lunchList);

        mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("新增常用");
                alertDialog.setView(view1);
                Maincategory=(String) spinner.getSelectedItem();
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newstring = editText.getText().toString();
                        money1=money.getText().toString();
                        System.out.println(newstring+Maincategory+money1+global.accountname);
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Mysqlqrcode con = new Mysqlqrcode();
                                int q=con.insertmodleData(newstring,Maincategory,"",money1,global.accountname,"收入");
                                if(q==0){
                                    Looper.prepare();
                                    Toast toast=Toast.makeText(getActivity(), "輸入成功", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER,0,0);
                                    toast.show();
                                    Looper.loop();
                                    money.setText(null);detail.setText(null);
                                    editText.setText("");
                                }else {
                                    Looper.prepare();
                                    Toast toast=Toast.makeText(getActivity(), "輸入失敗", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER,0,0);
                                    toast.show();
                                    Looper.loop();
                                }
                            }
                        }).start();
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("新增分類");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newstring = editText.getText().toString();
                        for(int i=0;i<all.size();i++){
                            System.out.println(all.get(i));
                            if(newstring.equals(all.get(i))){
                                Toast.makeText(getActivity(),"此類別已存在",Toast.LENGTH_LONG).show();
                                editText.setText("");
                                return;
                            }
                        }
                        if(!newstring.equals("")){
                            all.add((newstring));
                            spinner.setSelection(all.size()-1);
                            Toast.makeText(getActivity(),"新增成功",Toast.LENGTH_LONG).show();
                            editText.setText("");
                        }
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("刪除分類");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        boolean check=true;
                        String newstring = editText.getText().toString();
                        for(int i=0;i< all.size();i++){
                            System.out.println(all.get(i));
                            if(newstring.equals(all.get(i))){
                                if(!newstring.equals("")){
                                    check=false;
                                    all.remove((i));
                                    spinner.setSelection(0);
                                    Toast.makeText(getActivity(),"刪除成功",Toast.LENGTH_LONG).show();
                                    editText.setText("");
                                }
                            }
                        }
                        if(check==true){
                            Toast.makeText(getActivity(),"查無此類別名稱",Toast.LENGTH_LONG).show();
                            editText.setText("");
                        }
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode==KeyEvent.KEYCODE_BACK){
            Intent intent = new Intent(getActivity(), account.class);
            startActivity(intent);
        }
        return false;
    }
}