package com.example.a1006;

import android.app.Notification;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Document;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Oninvoice extends AppCompatActivity {
    TextView textView;

    public Oninvoice() throws MalformedURLException {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.oninvoice);

        textView=findViewById(R.id.textView6);
        textView.setGravity(Gravity.CENTER);
        String m[]={"01-02","03-04","05-06","07-08","09-10","11-12"};
        String nowDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String nowDate1[]=nowDate.split("-");
        boolean status=false;
        for(int i=0;i<6;i++){
            status= m[i].contains(nowDate1[1]);
            if(status) {
                nowDate=nowDate1[0]+"-"+m[i];
                break;
            }
        }
        textView.setText(nowDate);


    }


}
