package com.example.a1006;

import android.os.Bundle;
import android.app.Notification;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import org.w3c.dom.Document;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

public class wish extends Fragment {
    FloatingActionButton fab;
    ListView wishlsit;
    TextView goal,endtime,price,percentage;
    ArrayList<wishdata> wishdataArrayList=new ArrayList<wishdata>();
    boolean end=false;
    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.wish, container, false);
        Global global=(Global)getActivity().getApplicationContext();
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        wishlsit =view.findViewById(R.id.modeexdata1);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Mysqlqrcode con = new Mysqlqrcode();
                wishdataArrayList= con.getwishdata(global.accountname);
                end=true;
            }

        }).start();
        int i=0;
        while (end!=true){
            BaseAdapter adapterex1=new BaseAdapter() {
                @Override
                public int getCount() {
                    return wishdataArrayList.size();
                }

                @Override
                public Object getItem(int position) {
                    return position;
                }

                @Override
                public long getItemId(int position) {
                    return position;
                }

                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View layout = View.inflate(getActivity(), R.layout.wishdatalayout, null);
                    goal = layout.findViewById(R.id.goal);
                    endtime = layout.findViewById(R.id.endtime);
                    price = layout.findViewById(R.id.price);
                    percentage = layout.findViewById(R.id.percentage);
                    goal.setText(wishdataArrayList.get(position).name);
                    endtime.setText(wishdataArrayList.get(position).end);
                    price.setText(wishdataArrayList.get(position).cost+"$");
                    percentage.setText("100%");
                    return layout;
                }
            };
            wishlsit.setAdapter(adapterex1);
        }


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), addwish.class));
            }
        });
        return view;
    }
}
