package com.example.a1006;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.os.Looper;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.Calendar;

public class addwish  extends AppCompatActivity {

    DatePickerDialog.OnDateSetListener dateSetListener;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addwish);
        TextView textView1, textView2, textView3;
        TextView startdate = findViewById(R.id.date);
        TextView enddate = findViewById(R.id.end);
        EditText name = findViewById(R.id.name);
        EditText cost = findViewById(R.id.cost);
        Button add=findViewById(R.id.add);
        Global global=(Global)getApplicationContext();
        Calendar cal=Calendar.getInstance();
        int year=cal.get(Calendar.YEAR);
        int month=cal.get(Calendar.MONTH);
        int day=cal.get(Calendar.DATE);

        //start日期
        String nowdate = year + "/" + (month+1) + "/" + day;
        startdate.setText(nowdate);
        startdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateSetListener=new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        String date1 = year + "/" + String.format("%02d", (month+1)) + "/" + String.format("%02d", day);
                        startdate.setText(date1);
                    }
                };

                DatePickerDialog dialog=new DatePickerDialog(addwish.this, dateSetListener, year,month,day);
                dialog.show();


            }
        });

        //enddate
        enddate.setText(nowdate);
        enddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateSetListener=new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        String date2 = year + "/" + String.format("%02d", (month+1)) + "/" + String.format("%02d", day);
                        enddate.setText(date2);
                    }
                };

                DatePickerDialog dialog=new DatePickerDialog(addwish.this, dateSetListener, year,month,day);
                dialog.show();

            }
        });

        //新增到資料庫
//        ArrayList<wishdata>  wishdataArrayList=new ArrayList<wishdata>();
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String start=startdate.getText().toString();
                String wishname = name.getText().toString();
                String wishcost = cost.getText().toString();
                String wishdeadline = enddate.getText().toString();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Mysqlqrcode con = new Mysqlqrcode();
                        int q=con.insertwish(global.accountname,wishname,start,wishcost,wishdeadline);
                        if(q==0){
                            Looper.prepare();
                            Toast toast=Toast.makeText(addwish.this, "輸入成功", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER,0,0);
                            toast.show();
                            Looper.loop();
                        }else {
                            Looper.prepare();
                            Toast toast=Toast.makeText(addwish.this, "輸入失敗", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER,0,0);
                            toast.show();
                            Looper.loop();
                        }
                    }
                }).start();
            }
        });
    }
}
