package com.example.a1006;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.ui.AppBarConfiguration;

import com.google.android.material.navigation.NavigationView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class foodcommend extends AppCompatActivity {
    private ProgressBar mLoadingBar;
    ListView store;
    Button button;
    LocationManager lm;
    private DrawerLayout drawerLayout;

    String commadStr;
    int mpacl = 11;
    boolean end = false;
    int range = 1000;
    ArrayList<store> storebuffer = new ArrayList<store>();
    ArrayList<store> totalstore = new ArrayList<store>();
    ArrayList<store> totalstore2 = new ArrayList<store>();
    Calendar calendar = Calendar.getInstance();
    Calendar c = Calendar.getInstance();
    int hour = c.get(Calendar.HOUR_OF_DAY);
    int minute = c.get(Calendar.MINUTE);
    int timesum = hour * 60 + minute;
    int weekday = c.get(Calendar.DAY_OF_WEEK);

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.foodcommend);
        mLoadingBar = (ProgressBar) findViewById(R.id.progressbar);
        store = findViewById(R.id.wish);
        Bundle bundle = getIntent().getExtras();
        String account = bundle.getString("account");
        //  button=findViewById(R.id.find);
        commadStr = LocationManager.GPS_PROVIDER;
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout=findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        toolbar.setTitleTextColor(Color.WHITE);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        TextView navaccount=headerView.findViewById(R.id.accountname);
        navaccount.setText(account);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.itemfoodcommend:
                        Intent intent = new Intent(foodcommend.this, foodcommend.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("account",account);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        break;
                    case R.id.itemaccount:
                        intent = new Intent(foodcommend.this, account.class);
                        bundle = new Bundle();
                        bundle.putString("account",account);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        break;
                    case R.id.itemoninvoice:
                        intent = new Intent(foodcommend.this, Oninvoice.class);
                        bundle = new Bundle();
                        bundle.putString("account",account);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        break;
                }

                menuItem.setChecked(true);//點選了把它設為選中狀態
                return true;
            }
        });

        lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        mLoadingBar.setVisibility(View.VISIBLE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = lm.getLastKnownLocation(commadStr);



        System.out.println(String.valueOf(location));

        if(location!=null) {
            mLoadingBar.setVisibility(View.GONE);

            Geocoder gc = new Geocoder(getBaseContext(), Locale.TRADITIONAL_CHINESE);
            List<Address> lstAddress = null;
            try {
                lstAddress = gc.getFromLocation(location.getLatitude(),location.getLongitude(), 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String returnAddress=lstAddress.get(0).getAddressLine(0);

            String buffer=returnAddress.substring(8,11);


            new Thread(new Runnable() {
                @Override
                public void run() {
                    Mysqlqrcode con = new Mysqlqrcode();
                    storebuffer= con.getData(buffer);

                    for(int i=0;i<storebuffer.size();i++){
                        double sum=algorithm(location.getLatitude(),location.getLongitude(),storebuffer.get(i).lat,storebuffer.get(i).lng);
                        if(sum<range){
                            storebuffer.get(i).sum= (float) sum;
                            totalstore.add(storebuffer.get(i));
                        }
                    }
                    Collections.sort(totalstore, new storesort());

                    for(int i=0;i<totalstore.size();i++) {
                        totalstore.get(i).time = totalstore.get(i).time.replaceAll("\\[", "");
                        totalstore.get(i).time = totalstore.get(i).time.replaceAll("\\]", "");
                        String[] timeall = totalstore.get(i).time.split("\\'");
                        ArrayList<String> tradetime = new ArrayList<String>();
                        for (int j = 1; j < timeall.length; j += 2) {
                            tradetime.add(timeall[j]);
                        }
                        String tmp = tradetime.get(weekday - 1).substring(4);

                        tmp=tmp.replaceAll("\\s","");

                        if(tmp.equals("24小時營業")) {
                            totalstore2.add(totalstore.get(i));
                        }
                        if (!tmp.equals("休息")) {
                            if(!tmp.equals("24小時營業")){

                                String[] buffer=tmp.split("\\,");

                                for(int j=0;j<buffer.length;j++){
                                    int[] timerange=new int[2];
                                    String[] buffer2=buffer[j].split("\\–");
                                    for(int q=0;q<buffer2.length;q++){

                                        String[] buffer3=buffer2[q].split("\\:");


                                        timerange[q]=Integer.valueOf(buffer3[0])*60+Integer.valueOf(buffer3[1]);
                                    }

                                    if(timesum>=timerange[0] && timesum<=timerange[1]){
                                        totalstore2.add(totalstore.get(i));

                                    }
                                }
                            }

                        }

                    }end=true;
                    System.out.println(totalstore2.size());
                }
            }).start();


            while (end!=true){
                BaseAdapter adapter=new BaseAdapter() {
                    @Override
                    public int getCount() {
                        return totalstore2.size();
                    }

                    @Override
                    public Object getItem(int i) {
                        return i;
                    }

                    @Override
                    public long getItemId(int i) {
                        return i;
                    }

                    @Override
                    public View getView(int i, View view, ViewGroup viewGroup) {
                        View layout=View.inflate(foodcommend.this,R.layout.stroelayout,null);
                        TextView address=layout.findViewById(R.id.phone);
                        TextView name=layout.findViewById(R.id.name);
                        TextView range=layout.findViewById(R.id.range);
                        TextView time=layout.findViewById(R.id.time);

                        double sum=algorithm(location.getLatitude(),location.getLongitude(),totalstore2.get(i).lat,totalstore2.get(i).lng);
                        name.setText((i + 1) + ". " + totalstore2.get(i).name);
                        address.setText(totalstore2.get(i).address);
                        range.setText("距離: " +String.format("%.2f",sum/1000) + " km");
                        time.setText("營業中");
                        return layout;

                    }
                };
                store.setAdapter(adapter);
            }
            store.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent();
                    intent.setClass(foodcommend.this,storedetail.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("name",String.valueOf(totalstore2.get(position).name));
                    bundle.putString("phone",String.valueOf(totalstore2.get(position).phone));
                    bundle.putFloat("lat",totalstore2.get(position).lat);
                    bundle.putFloat("lng",totalstore2.get(position).lng);
                    bundle.putString("address",String.valueOf(totalstore2.get(position).address));

                    intent.putExtras(bundle);   // 記得put進去，不然資料不會帶過去哦
                    startActivity(intent);
                }
            });
            if(totalstore2.size()==0){
                AlertDialog.Builder dialog = new AlertDialog.Builder(foodcommend.this);
                dialog.setTitle("提示");
                dialog.setMessage("目前沒有營業的店家");

                dialog.setPositiveButton("YES",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        // TODO Auto-generated method stub
                        Toast.makeText(foodcommend.this, "確定",Toast.LENGTH_SHORT).show();
                    }

                });
                dialog.show();
            }


        }

    }
    // });


    // }
    private static double rad(double d) {
        return d * Math.PI / 180.00; // 角度轉換成弧度
    }
    public static double algorithm(double mylng, double mylat, double lng, double lat) {
        double Lat1 = rad(mylat); // 緯度
        double Lat2 = rad(lat);
        double a = Lat1 - Lat2;// 兩點緯度之差
        double b = rad(mylng) - rad(lng); // 經度之差
        double s = 2 * Math.asin(Math
                .sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(Lat1) * Math.cos(Lat2) * Math.pow(Math.sin(b / 2), 2)));// 計算兩點距離的公式
        s = s * 6378137.0;// 弧長乘地球半徑（半徑為米）
        s = Math.round(s * 10000d) / 10000d;// 精確距離的數值

        return s;
    }
    public LocationListener ln =new LocationListener() {
        @Override
        public void onLocationChanged(@NonNull Location location) {

            Geocoder gc = new Geocoder(getBaseContext(), Locale.TRADITIONAL_CHINESE);
            List<Address> lstAddress = null;
            try {
                lstAddress = gc.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String returnAddress = lstAddress.get(0).getAddressLine(0);

        }

        @Override
        public void onProviderEnabled(@NonNull String provider) {

        }

        @Override
        public void onProviderDisabled(@NonNull String provider) {

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }
    };


}
