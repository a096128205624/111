package com.example.a1006;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;


public  class pageAdapter extends FragmentStateAdapter {
    private int numoftabs;


    public pageAdapter(@NonNull FragmentActivity fr, int numOfTabs) {
        super(fr);
        this.numoftabs = numOfTabs;
    }
    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0:
                return new modle();
            case 1:
                return new expend();
            case 2:
                return new income();
            case 3:
                return new wish();
            default:
                return null;
        }

    }
    @Override
    public int getItemCount() {
        return numoftabs;
    }

}