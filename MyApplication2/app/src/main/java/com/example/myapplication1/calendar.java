package com.example.myapplication1;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class calendar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendarlayout);
        List<EventDay> events = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        Calendar c=Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        c.set(yy,mm,dd-1);
//or if you want to specify event label color
        events.add(new EventDay(calendar, R.drawable.button_income_background, Color.parseColor("#228B22")));
        events.add(new EventDay(c, R.drawable.button_pay_background));
        CalendarView calendarView = (CalendarView) findViewById(R.id.calendarView);
        final TextView show=findViewById(R.id.show);
        calendarView.setEvents(events);
        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                Calendar clickedDayCalendar = eventDay.getCalendar();
                show.setText(String.valueOf(clickedDayCalendar.get(Calendar.YEAR))+"/"+String.valueOf(clickedDayCalendar.get(Calendar.MONTH)+1)+"/"+String.valueOf(clickedDayCalendar.get(Calendar.DATE)));
            }
        });


    }
}