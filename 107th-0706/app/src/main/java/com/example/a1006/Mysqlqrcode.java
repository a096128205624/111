package com.example.a1006;

import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import static android.os.SystemClock.sleep;

public class Mysqlqrcode  {
    String mysql_ip = "120.105.161.106";
    int mysql_port = 3306; // Port 預設為 3306
    String db_name = "107th";
    String url = "jdbc:mysql://"+mysql_ip+":"+mysql_port+"/"+db_name+"?useUnicode=true&characterEncoding=UTF-8";
    String db_user = "root";
    String db_password = "th107";

    public void run() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Log.v("DB","加載驅動成功");
        }catch( ClassNotFoundException e) {
            Log.e("DB","加載驅動失敗");
            return;
        }

        // 連接資料庫
        try {
            Connection con = DriverManager.getConnection(url,db_user,db_password);
            Log.v("DB","遠端連接成功");
        }catch(SQLException e) {
            Log.e("DB","遠端連接失敗");
            Log.e("DB", e.toString());
        }
    }
    public ArrayList<store> getData(String address) {
        ArrayList<store> arrayList=new ArrayList<store>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Log.v("DB","加載驅動成功");
            try {
                Connection con = DriverManager.getConnection(url, db_user, db_password);
                Log.v("DB",address);
                String str="%"+address+"%";
                String sql = "Select * FROM store where address LIKE '"+str+"'";

                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sql);

                while (rs.next())
                {
                    String name = rs.getString("name");
                    String add = rs.getString("address");
                    float lat =rs.getFloat("lat");
                    float lng=rs.getFloat("lng");
                    String time=rs.getString("tradetime");
                    String phone=rs.getString("phone");
                    store sto=new store(name,add,lat,lng,0,time,phone);

                    arrayList.add(sto);
                }


                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }catch( ClassNotFoundException e) {
            Log.e("DB","加載驅動失敗");
        }
        return arrayList;

    }
    public void insertAccount(String account,String password,String email)  {
        try {

            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(url, db_user, db_password);
            String sql = "INSERT INTO members "+"VALUES('"+account+"','使用者','"+password+"','"+email+"',NULL,'X')";
            Statement st = con.createStatement();
            st.executeUpdate(sql);
            st.close();
            Log.v("DB", "寫入資料完成：" );

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            Log.e("DB", "寫入資料失敗");
            Log.e("DB", e.toString());
        }
    }
    public AccountValue getAccount(String account){
        AccountValue av =new AccountValue();
        System.out.println(account);
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(url, db_user, db_password);

            String sql = "SELECT * FROM `members` WHERE account = '"+account+"' ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                av.setAccount(rs.getString("account"),rs.getString("passwd"),rs.getString("state"));
                System.out.println(av.account);
            }
            else{
                av.setAccount("noaccount","nopasswd","X");
            }
            st.close();


        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
        return av;
    }
    public String getData() {
        String data = "";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(url, db_user, db_password);
            String sql = "SELECT * FROM text";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next())
            {
                //String id = rs.getString("id");
                String name = rs.getString("name");
                data +=  name + "\n";
            }
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return data;
    }


    public boolean q(String data) {
        int i=0;
        try {
            Log.v("rs.next()", String.valueOf(i));
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(url, db_user, db_password);
            String sql = "SELECT code FROM text";
            sleep(1000);
            Statement st = con.createStatement();
            sleep(1000);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next())
            {
                String name = rs.getString("code");
                if (name.equals(data)){
                    i++;
                }
            }
            Log.v("rs.next()", String.valueOf(i));
            st.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (i==0){
            Log.v("true","true");
            return true;
        }else {
            Log.v("false","false");
            return false;
        }
    }

    int insertData(String code,String money,String maincategory,String subcategory,String detail1,String date,String time) {
        int q=0;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(url, db_user, db_password);
            String sql ="INSERT INTO expend (code,money,maincategory,subcategory,productname,date,time) values " + "('"+code+"','"+money+"','"+maincategory+"','"+subcategory+"','"+detail1+"','"+date+"','"+time+"')";//`code`,`date`,`randomcode`,`productname`
            Statement st = con.createStatement();
            st.executeUpdate(sql);
            st.close();
            Log.v("DB", "寫入資料完成：" + date);
        } catch (SQLException  e) {
            e.printStackTrace();
            Log.e("DB", "寫入資料失敗");
            q=1;
            Log.e("DB", e.toString());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return q;
    }
    int insertData1(String money, String maincategory, String detail1, String date, String time) {
        int q=0;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(url, db_user, db_password);
            String sql ="INSERT INTO income (money,maincategory,productname,date,time) values " + "('"+money+"','"+maincategory+"','"+detail1+"','"+date+"','"+time+"')";//`code`,`date`,`randomcode`,`productname`
            Statement st = con.createStatement();
            st.executeUpdate(sql);
            st.close();
            Log.v("DB", "寫入資料完成：" + date);
        } catch (SQLException  e) {
            e.printStackTrace();
            Log.e("DB", "寫入資料失敗");
            q=1;
            Log.e("DB", e.toString());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return q;
    }

}
