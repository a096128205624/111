package com.example.a1006;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;

public class storedetail extends AppCompatActivity {
    TextView discount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.storedetail);
        discount=findViewById(R.id.name);
        WebView webview = (WebView) findViewById(R.id.web_view);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebViewClient(new WebViewClient()); //不調用系統瀏覽器
        Bundle bundle = getIntent().getExtras();
        String discountdata = bundle.getString("discount");
        String storename=bundle.getString("storename");
        String address= bundle.getString("address");
        webview.loadUrl("http://120.105.161.106/107th/stores/androidview.php?storename="+storename+"&"+"address="+address);

        System.out.println(discountdata);
        if(discountdata.equals("null")){
            discount.setText("暫無優惠");
        }else{
            discount.setText(discountdata+"\n");
        }



    }
}
