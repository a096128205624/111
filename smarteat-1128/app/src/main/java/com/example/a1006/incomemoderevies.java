package com.example.a1006;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

public class incomemoderevies extends AppCompatActivity {

    TextView tdate,ttime,mode;
    EditText money,detail;
    String moneybuffer,Maincategory,title;
    Spinner spinner;
    Button add,delete;
    Global global;
    boolean end=false;
    ArrayList<String> category=new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.incomemode);
        View view1=getLayoutInflater().inflate(R.layout.addview,null);
        ttime=findViewById(R.id.time);
        spinner = findViewById(R.id.spinner3);
        tdate=findViewById(R.id.date);
        add=findViewById(R.id.button7);
        delete=findViewById(R.id.button8);
        money=findViewById(R.id.money);
        detail=findViewById(R.id.detail);
        mode=findViewById(R.id.button12);
        global=(Global)getApplicationContext();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Mysqlqrcode con = new Mysqlqrcode();
                category= con.getIncomecategory(global.accountname);
                System.out.println(category.size());
                end=true;
            }
        }).start();
        while (end!=true){
            ArrayAdapter<String> lunchList = new ArrayAdapter<>(incomemoderevies.this, android.R.layout.simple_spinner_dropdown_item, category);
            lunchList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(lunchList);
        }

        mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(incomemoderevies.this);
                alertDialog.setTitle("修改常用");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Maincategory=(String) spinner.getSelectedItem();
                        String newstring = editText.getText().toString();
                        moneybuffer=money.getText().toString();
                        title=global.getTitleIN();
                        System.out.println(newstring+Maincategory+moneybuffer+global.accountname+title);
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Mysqlqrcode con = new Mysqlqrcode();
                                int q=con.reviesmodleData(title,newstring,Maincategory,moneybuffer,global.accountname,"收入");
                                if(q==0){
                                    Looper.prepare();
                                    Toast toast=Toast.makeText(incomemoderevies.this, "輸入成功", Toast.LENGTH_LONG);
                                    toast.show();
                                    Looper.loop();

                                }else {
                                    Looper.prepare();
                                    Toast toast=Toast.makeText(incomemoderevies.this, "輸入失敗", Toast.LENGTH_LONG);
                                    toast.show();
                                    Looper.loop();
                                }
                            }
                        }).start();
                        Intent intent = new Intent(incomemoderevies.this, tab.class);
                        int n=0;
                        intent.putExtra("postion",n);
                        startActivity(intent);
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(incomemoderevies.this);
                alertDialog.setTitle("新增分類");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newstring = editText.getText().toString();
                        for(int i=0;i<category.size();i++){
                            System.out.println(category.get(i));
                            if(newstring.equals(category.get(i))){
                                Toast.makeText(incomemoderevies.this,"此類別已存在",Toast.LENGTH_LONG).show();
                                editText.setText("");
                                return;
                            }
                        }
                        if(!newstring.equals("")){
                            category.add((newstring));
                            spinner.setSelection(category.size()-1);
                            Toast.makeText(incomemoderevies.this,"新增成功",Toast.LENGTH_LONG).show();
                            editText.setText("");
                        }
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(incomemoderevies.this);
                alertDialog.setTitle("刪除分類");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        boolean check=true;
                        String newstring = editText.getText().toString();
                        for(int i=0;i< category.size();i++){
                            if(newstring.equals(category.get(i))){
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Mysqlqrcode con = new Mysqlqrcode();
                                        con.deletecategory(newstring,global.accountname);
                                    }
                                }).start();
                                check=false;
                                category.remove((i));
                                if(i==0){
                                    spinner.setSelection(1);
                                }else {
                                    spinner.setSelection(0);
                                }
                                Toast.makeText(incomemoderevies.this,"刪除成功",Toast.LENGTH_LONG).show();
                                editText.setText("");
                            }
                        }
                        if(check==true){
                            Toast.makeText(incomemoderevies.this,"查無此類別名稱",Toast.LENGTH_LONG).show();
                            editText.setText("");
                        }
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode==KeyEvent.KEYCODE_BACK){
            global.setPositionIN(null);
            Intent intent = new Intent(incomemoderevies.this, account.class);
            startActivity(intent);
        }
        return false;
    }
}