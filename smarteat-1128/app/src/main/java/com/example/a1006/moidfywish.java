package com.example.a1006;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class moidfywish extends AppCompatActivity {
    DatePickerDialog.OnDateSetListener dateSetListener;
    String startd,endd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modifywish);
        Global global=(Global)getApplication();
        TextView start = findViewById(R.id.date);
        TextView end = findViewById(R.id.end);
        Calendar cal=Calendar.getInstance();
        int year=cal.get(Calendar.YEAR);
        int month=cal.get(Calendar.MONTH);
        int day=cal.get(Calendar.DATE);
        EditText erank = findViewById(R.id.rank);
        EditText goal = findViewById(R.id.name);
        EditText price = findViewById(R.id.cost);
//        EditText start = findViewById(R.id.date);
//        EditText end = findViewById(R.id.end);
        Button modify = findViewById(R.id.modify);
        Button del = findViewById(R.id.del);
        Bundle bundle = getIntent().getExtras();
        int rank=bundle.getInt("rank");
        String wish=bundle.getString("gaol");
        goal.setText(bundle.getString("gaol"));
        price.setText(bundle.getString("price"));
        start.setText(bundle.getString("start"));
        end.setText(bundle.getString("end"));
        erank.setText(String.valueOf(rank));

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateSetListener=new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        String date1 = year + "/" + String.format("%02d", (month+1)) + "/" + String.format("%02d", day);
                        start.setText(date1);
                        startd= date1;
                    }
                };

                DatePickerDialog dialog=new DatePickerDialog(moidfywish.this, dateSetListener, year,month,day);
                dialog.show();


            }
        });

        end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateSetListener=new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        String date2 = year + "/" + String.format("%02d", (month+1)) + "/" + String.format("%02d", day);
                        end.setText(date2);
                    }
                };
                DatePickerDialog dialog=new DatePickerDialog(moidfywish.this, dateSetListener, year,month,day);
                dialog.show();
            }
        });


        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Mysqlqrcode con = new Mysqlqrcode();
                        int q=con.deletewish(rank,wish,global.accountname);
                        if (q==0){
                            Intent intent = new Intent(getApplication(), wish.class);
                            startActivity(intent);
                        }
                    }
                }).start();
            }
        });

        modify.setOnClickListener(new View.OnClickListener() {
            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
            Date date1;
            Date date2;
            boolean f=false;
            @Override
            public void onClick(View v) {
                String wish1=goal.getText().toString();
                String start1=start.getText().toString();
                String price1=price.getText().toString();
                String end1=end.getText().toString();
                int rank1= Integer.parseInt(erank.getText().toString());
                long calday=0;
                String save;
                int price =Integer.parseInt(price1);
                try {
                    date1=df.parse(start1);
                    date2=df.parse(end1);

                    calday = (date1.getTime()-date2.getTime())/(24*60*60*1000)>0 ? (date1.getTime()-date2.getTime())/(24*60*60*1000):
                            (date2.getTime()-date1.getTime())/(24*60*60*1000);
                    System.out.println("相差:"+calday);
                    if (calday==0){
                        calday=1;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                // int x=(int)calday;

                save=bundle.getString("saveprice");
                if ((date1.getTime()-date2.getTime())>0){
                    Toast toast=Toast.makeText(moidfywish.this, "結束時間勿小於開始時間", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();
                } else if (calday>price) {
                    Toast toast=Toast.makeText(moidfywish.this, "結束日期太久", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();
                }
                else {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Mysqlqrcode con = new Mysqlqrcode();
                            int q=con.modifywish(wish1,price1,start1,end1,wish,save,rank1,rank,global.accountname);
                            if(q==0){
                                Looper.prepare();
                                Toast toast=Toast.makeText(moidfywish.this, "更改成功", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER,0,0);
                                toast.show();
                                Intent intent = new Intent(getApplication(), wish.class);
                                startActivity(intent);
                                Looper.loop();
                            }else {
                                Looper.prepare();
                                Toast toast=Toast.makeText(moidfywish.this, "更改失敗", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER,0,0);
                                toast.show();
                                Intent intent = new Intent(getApplication(), wish.class);
                                startActivity(intent);
                                Looper.loop();
                            }
                        }
                    }).start();

                }
            }
        });
    }

}
