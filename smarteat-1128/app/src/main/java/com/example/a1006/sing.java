package com.example.a1006;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class sing extends AppCompatActivity {
    EditText Eaccount,Epassword,Epasswordconfirm,Eemail,Ephone;
    TextView submit;
    String account,password,passwordconfirm,email,phone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sing);
        Eaccount=findViewById(R.id.oldpassword);
        Epassword=findViewById(R.id.pass);
        Epasswordconfirm=findViewById(R.id.pass2);
        Eemail=findViewById(R.id.newpasswrod);

        submit=findViewById(R.id.bsubmit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                account=Eaccount.getText().toString();
                password=Epassword.getText().toString();
                passwordconfirm=Epasswordconfirm.getText().toString();
                email=Eemail.getText().toString();

                if(password.equals(passwordconfirm)){
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Mysqlqrcode con = new Mysqlqrcode();
                            con.insertAccount(account,password,email);

                        }
                    }).start();
                    Toast toast = Toast.makeText(sing.this, "註冊成功", Toast.LENGTH_LONG);
                    toast.show();

                    Intent intent = new Intent();
                    intent.setClass(sing.this,confirmemail.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("email",email);
                    bundle.putString("account",account);
                    intent.putExtras(bundle);   // 記得put進去，不然資料不會帶過去哦
                    startActivity(intent);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(sing.this);
                    builder.setMessage("密碼確認錯誤");
                    builder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
}
