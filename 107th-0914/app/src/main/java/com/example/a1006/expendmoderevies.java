package com.example.a1006;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class expendmoderevies extends AppCompatActivity {

    private Spinner spinner;
    EditText money,detail;
    Button add,delete;
    TextView mode;
    Global global;
    String Maincategory,title;
    boolean end=false;
    ArrayList<String> category=new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expendmode);
        View view1=getLayoutInflater().inflate(R.layout.addview,null);
        spinner = findViewById(R.id.spinner);
        mode=findViewById(R.id.button4);
        add=findViewById(R.id.button9);
        delete=findViewById(R.id.button10);
        money=findViewById(R.id.money);
        detail=findViewById(R.id.detail);
        global=(Global)this.getApplicationContext();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Mysqlqrcode con = new Mysqlqrcode();
                category= con.getExpendcategory(global.accountname);
                System.out.println(category.size());
                end=true;
            }
        }).start();
        while (end!=true){
            ArrayAdapter<String> lunchList = new ArrayAdapter<>(expendmoderevies.this, android.R.layout.simple_spinner_dropdown_item, category);
            lunchList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(lunchList);
        }
        end=false;
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(expendmoderevies.this);
                alertDialog.setTitle("新增分類");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newstring = editText.getText().toString();
                        for(int i=0;i<category.size();i++){
                            if(newstring.equals(category.get(i))){
                                Toast.makeText(expendmoderevies.this,"此類別已存在",Toast.LENGTH_LONG).show();
                                editText.setText("");
                                return;
                            }
                        }
                        if(!newstring.equals("")){
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Mysqlqrcode con = new Mysqlqrcode();
                                    con.insertExpendcategory(newstring,global.accountname);
                                }
                            }).start();
                            category.add(newstring);
                            Toast.makeText(expendmoderevies.this,"新增成功",Toast.LENGTH_LONG).show();
                            editText.setText("");
                        }

                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(expendmoderevies.this);
                alertDialog.setTitle("刪除分類");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        boolean check=true;
                        String newstring = editText.getText().toString();
                        for(int i=0;i< category.size();i++){
                            if(newstring.equals(category.get(i))){
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Mysqlqrcode con = new Mysqlqrcode();
                                        con.deletecategory(newstring,global.accountname);
                                    }
                                }).start();
                                check=false;
                                category.remove((i));
                                if(i==0){
                                    spinner.setSelection(1);
                                }else {
                                    spinner.setSelection(0);
                                }
                                Toast.makeText(expendmoderevies.this,"刪除成功",Toast.LENGTH_LONG).show();
                                editText.setText("");
                            }
                        }
                        if(check==true){
                            Toast.makeText(expendmoderevies.this,"查無此類別名稱",Toast.LENGTH_LONG).show();
                            editText.setText("");
                        }
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });



        mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(expendmoderevies.this);
                alertDialog.setTitle("修改常用");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newstring = editText.getText().toString();
                        String moneybuffer=money.getText().toString();
                        Maincategory=(String) spinner.getSelectedItem();
                        title=global.getTitleEX();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Mysqlqrcode con = new Mysqlqrcode();
                                int q=con.reviesmodleData(title,newstring,Maincategory,moneybuffer,global.accountname,"支出");
                            }
                        }).start();
                        Intent intent = new Intent(expendmoderevies.this, tab.class);
                        int n=0;
                        intent.putExtra("postion",n);
                        startActivity(intent);
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
    }
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode==KeyEvent.KEYCODE_BACK){
            global.setPositionEX(null);
            Intent intent = new Intent(expendmoderevies.this, account.class);
            startActivity(intent);
        }
        return false;
    }
}