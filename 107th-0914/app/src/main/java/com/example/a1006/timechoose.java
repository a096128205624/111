package com.example.a1006;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

public class timechoose extends AppCompatActivity {
    LinearLayout statisticsweekchartPie,statisticsyearchartPie,statisticsmonthchartPie;
    LinearLayout statisticsweekchartBar,statisticsyearchartBar,statisticsmonthchartBar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timechoose);
        statisticsyearchartPie=findViewById(R.id.button14);
        statisticsmonthchartPie=findViewById(R.id.button15);
        statisticsweekchartPie=findViewById(R.id.button17);
        statisticsyearchartBar=findViewById(R.id.button13);
        statisticsmonthchartBar=findViewById(R.id.button16);
        statisticsweekchartBar=findViewById(R.id.button18);

        statisticsyearchartPie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.setClass(timechoose.this,statisticsyearchart.class);
                startActivity(intent);
            }
        });
        statisticsmonthchartPie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.setClass(timechoose.this,statisticsmonthchart.class);
                startActivity(intent);
            }
        });
        statisticsweekchartPie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.setClass(timechoose.this,statisticsweekchart.class);
                startActivity(intent);
            }
        });
        statisticsyearchartBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.setClass(timechoose.this,statisticsyearchartBar.class);
                startActivity(intent);
            }
        });
        statisticsmonthchartBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.setClass(timechoose.this,statisticsmonthchartBar.class);
                startActivity(intent);
            }
        });
        statisticsweekchartBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.setClass(timechoose.this,statisticsweekchartBar.class);
                startActivity(intent);
            }
        });
    }
}