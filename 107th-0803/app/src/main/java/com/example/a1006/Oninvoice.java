package com.example.a1006;

import android.app.Notification;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

import org.w3c.dom.Document;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Oninvoice extends AppCompatActivity {
    TextView textView;
    private DrawerLayout drawerLayout;
    public Oninvoice() throws MalformedURLException {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.oninvoice);
        Bundle bundle = getIntent().getExtras();
        String account = bundle.getString("account");
        textView=findViewById(R.id.textView6);
        textView.setGravity(Gravity.CENTER);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout=findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        toolbar.setTitleTextColor(Color.WHITE);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        TextView navaccount=headerView.findViewById(R.id.accountname);
        navaccount.setText(account);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.itemfoodcommend:
                        Intent intent = new Intent(Oninvoice.this, foodcommend.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("account",account);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        break;
                    case R.id.itemaccount:
                        intent = new Intent(Oninvoice.this, account.class);
                        bundle = new Bundle();
                        bundle.putString("account",account);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        break;
                    case R.id.itemoninvoice:
                        intent = new Intent(Oninvoice.this, Oninvoice.class);
                        bundle = new Bundle();
                        bundle.putString("account",account);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        break;
                }

                menuItem.setChecked(true);//點選了把它設為選中狀態
                return true;
            }
        });
        String m[]={"01-02","03-04","05-06","07-08","09-10","11-12"};
        String nowDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String nowDate1[]=nowDate.split("-");
        boolean status=false;
        for(int i=0;i<6;i++){
            status= m[i].contains(nowDate1[1]);
            if(status) {
                nowDate=nowDate1[0]+"-"+m[i];
                break;
            }
        }
        textView.setText(nowDate);


    }


}
