package com.example.a1006;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

import static android.os.SystemClock.sleep;

public class qrcode extends AppCompatActivity {
    SurfaceView surfaceView;
    TextView textView;
    CameraSource cameraSource;
    BarcodeDetector barcodeDetector;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qrcode);

        getPermissionsCanera();

        surfaceView=(SurfaceView)findViewById(R.id.surfaceView);
        textView=(TextView)findViewById(R.id.textView5);
        Global global=(Global)getApplicationContext();

        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE).build();
        cameraSource=new CameraSource.Builder(this,barcodeDetector)
                .setRequestedPreviewSize(300,300).build();
        cameraSource = new CameraSource.Builder(this,barcodeDetector).setAutoFocusEnabled(true).build();
        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback(){
            @Override
            public void surfaceCreated(@NonNull SurfaceHolder surfaceHolder) {
                if(ActivityCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.CAMERA)
                        !=PackageManager.PERMISSION_GRANTED)
                    return;
                try{
                    cameraSource.start(surfaceHolder);
                }catch (IOException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(@NonNull SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(@NonNull SurfaceHolder surfaceHolder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>(){

            @Override
            public void release() {

            }

            public String unicode2big5(String strUTF8)
            {
                String strReturn="";
                try
                {
                    strReturn = new String(strUTF8.getBytes("UTF-8"), "big5");
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                return strReturn;
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> qrCodes=detections.getDetectedItems();
                sleep(1000);
                if(qrCodes.size()!=0){
                    textView.post(new Runnable() {
                        @Override
                        public void run() {
                            String strings,result,strings1 ="",code,editer,date,randomcode,strings2=null,date1,strings3="",detail;
                            strings=qrCodes.valueAt(0).displayValue;
                            strings.split("");
                            result=strings.substring(0,1);
                            boolean result1 = result.matches("[a-zA-Z]+");
                            if(result1){
                                strings1=strings;
                                code=strings1.substring(0,10);
                                date=strings1.substring(10,17);
                                date1=date.substring(0,3)+"/"+date.substring(3,5)+"/"+date.substring(5,7);
                                randomcode=strings1.substring(17,21);
                                editer=strings1.substring(45,53);
                                String[] strs=strings1.split(":");
                                strings2="發票號碼:"+code+"\n日期:"+date1+"\n隨機碼"+randomcode+"\n統編"+editer;
                                int sum=0;
                                if(code.equals("44953265")){
                                    for(int i=5;i<strs.length-1;i=i+3){
                                        strs[i]=unicode2big5(strs[i]);
                                        strings3+="\n品名:"+strs[i]+"\t"+strs[i+1]+"個\t"+strs[i+2]+"元";
                                        strings2+="\n品名:"+strs[i]+"\t"+strs[i+1]+"個\t"+strs[i+2]+"元";
                                        if(strs.length-1==i){
                                            strs[i]=unicode2big5(strs[i]);
                                            strings3+="\n品名:"+strs[i]+"\t"+strs[i+1]+"個\t"+strs[i+2]+"元";
                                            strings2+="\n品名:"+strs[i]+"\t"+strs[i+1]+"個\t"+strs[i+2]+"元";
                                        }
                                    }
                                }else{
                                    for(int i=5;i<strs.length-1;i=i+3){
                                        if(strs[i-2].equals("1")&&i==5){
                                            sum+=Integer.parseInt(strs[i+2]);
                                            strings3+="\n品名:"+strs[i]+"\t"+strs[i+1]+"個\t單價"+Integer.parseInt(strs[i+2])/Integer.parseInt(strs[i+1])+"元";
                                            if(strs.length-1==i){
                                                strings3+="\n品名:"+strs[i]+"\t"+strs[i+1]+"個\t單價"+Integer.parseInt(strs[i+2])/Integer.parseInt(strs[i+1])+"元";
                                            }
                                        }else {
                                            sum+=Integer.parseInt(strs[i-2])*Integer.parseInt(strs[i+2]);
                                            strings3+="\n品名:"+strs[i]+"\t"+strs[i-2]+"個\t單價"+strs[i+2]+"元";
                                            if(strs.length-1==i){
                                                strings3+="\n品名:"+strs[i]+"\t"+strs[i-2]+"個\t單價"+strs[i+2]+"元";
                                            }
                                        }
                                    }
                                    //strings3+="\t總金額"+sum;
                                }

                                System.out.println(strings);
                                System.out.println(strings2);
                                System.out.println(strings3);
                                //sleep();
                                if(strings2!=null){
                                    global.setCode(code);
                                    global.setMoney(sum);
                                    global.setDate(date1);
                                    global.setEditer(editer);
                                    global.setDetail(strings3);
                                  //  global.setDetail(detail);
                                    Intent intent = new Intent(qrcode.this, tab.class);
                                    intent.putExtra("postion",1);
                                    final data data=new data();
                                    data.setdata(code);
                                    startActivity(intent);
                                }

//                                new Thread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        int i=0;
//                                        Mysqlqrcode con = new Mysqlqrcode();
//                                        if(con.q(code)&&i==0){
//                                            con.insertData(code, date1, randomcode, finalStrings);
//                                        }
//                                        i++;
//                                    }
//                                }).start();

                               // System.out.println(strs.length);
                            }
                            //textView.setText(qrCodes.valueAt(0).displayValue);
                        }

                    });
                }
            }
        });
    }



    public void getPermissionsCanera(){
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA},1);
        }
    }
}
