package com.example.a1006;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
            Intent resultintent=new Intent(context,login.class);
            PendingIntent pendingIntent=PendingIntent.getActivity(context,1,resultintent,PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder builder=new NotificationCompat.Builder(context,"My Notification");
            builder.setContentTitle("Smart Eat");
            builder.setContentText("您今天記帳了嗎，Smart Eat關心您");
            builder.setSmallIcon(R.drawable.accounting);
            builder.setAutoCancel(true);
            builder.setContentIntent(pendingIntent);
            NotificationManagerCompat managerCompat=NotificationManagerCompat.from(context);
            managerCompat.notify(1,builder.build());


    }
}
