package com.example.a1006;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;


public class statisticsyearchart extends AppCompatActivity {


    public PieChart pieChart ;
    ImageButton rimageButton,limageButton;
    TextView textView,change;
    ArrayList<historydata> historydataArrayList=new ArrayList<historydata>();
    ArrayList<PieEntry> categorypiechart=new ArrayList<PieEntry>();
    ArrayList<String> excategory=new ArrayList<String>();
    ArrayList<String> incategory=new ArrayList<String>();
    int[] excategorysum,incategorysum;
    Calendar c = Calendar.getInstance();
    int nyear = c.get(Calendar.YEAR);
    int nmonth = c.get(Calendar.MONTH)+1;
    int countyear=2021;
    int year=110+(nyear-countyear);
    boolean changebuffer=true;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.text);
        rimageButton=findViewById(R.id.minusmonth2);
        limageButton=findViewById(R.id.addmonth2);
        textView =findViewById(R.id.time2);
        textView.setText(String.valueOf(nyear)+" 年 ");
        change=findViewById(R.id.textView4);
        ArrayList<historydata> historydatabuffer=new ArrayList<historydata>();
        Global globalaccountname=(Global)getApplicationContext();
        String accountname=globalaccountname.getAccountname();

        new Thread(new Runnable() {
            @Override
            public void run() {
                Mysqlqrcode con = new Mysqlqrcode();
                historydataArrayList= con.gethistoryData(accountname);
                excategory= con.getExpendcategory(accountname);
                incategory= con.getIncomecategory(accountname);
                Global global=(Global) getApplicationContext();
                global.setHistorydata(historydataArrayList);
                excategorysum=new int[excategory.size()];
                incategorysum=new int[incategory.size()];

                for(int i=0;i<excategorysum.length;i++){
                    excategorysum[i]=0;
                }
                for(int i=0;i<incategorysum.length;i++){
                    incategorysum[i]=0;
                }
                for(int i=0;i<historydataArrayList.size();i++){
                    String nowday[]=historydataArrayList.get(i).date.split("\\/");
                    if(nowday[0].equals(String.valueOf(nyear))){
                        historydatabuffer.add(historydataArrayList.get(i));
                    }
                    if(nowday[0].equals(String.valueOf(nyear))){
                        if(historydataArrayList.get(i).type.equals("expend")){
                            System.out.println(historydataArrayList.get(i).date+"------"+historydataArrayList.get(i).maincategory);
                            for (int j=0;j<excategory.size();j++){
                                if(historydataArrayList.get(i).maincategory.equals(excategory.get(j))){
                                    excategorysum[j]=excategorysum[j]+Integer.valueOf(historydataArrayList.get(i).money);
                                }
                            }
                        }
                        else if(historydataArrayList.get(i).type.equals("income")){
                            for (int j=0;j<incategory.size();j++){
                                if(historydataArrayList.get(i).maincategory.equals(incategory.get(j))){
                                    incategorysum[j]=incategorysum[j]+Integer.valueOf(historydataArrayList.get(i).money);
                                }
                            }
                        }
                    }
                }
                Looper.prepare();
                show();
                Looper.loop();
            }
        }).start();
        limageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                year++;
                nyear++;
                textView.setText(String.valueOf(nyear)+" 年 ");
                for(int i=0;i<excategorysum.length;i++){
                    excategorysum[i]=0;
                }
                for(int i=0;i<incategorysum.length;i++){
                    incategorysum[i]=0;
                }
                for(int i=0;i<historydataArrayList.size();i++){
                    String nowday[]=historydataArrayList.get(i).date.split("\\/");
                    if(nowday[0].equals(String.valueOf(nyear))){
                        historydatabuffer.add(historydataArrayList.get(i));
                    }
                    if(nowday[0].equals(String.valueOf(nyear))){
                        if(historydataArrayList.get(i).type.equals("expend")){
                            System.out.println(historydataArrayList.get(i).date+"------"+historydataArrayList.get(i).maincategory);
                            for (int j=0;j<excategory.size();j++){
                                if(historydataArrayList.get(i).maincategory.equals(excategory.get(j))){
                                    excategorysum[j]=excategorysum[j]+Integer.valueOf(historydataArrayList.get(i).money);
                                }
                            }
                        }
                        else if(historydataArrayList.get(i).type.equals("income")){
                            for (int j=0;j<incategory.size();j++){
                                if(historydataArrayList.get(i).maincategory.equals(incategory.get(j))){
                                    incategorysum[j]=incategorysum[j]+Integer.valueOf(historydataArrayList.get(i).money);
                                }
                            }
                        }
                    }
                }
                show();
            }
        });
        rimageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                year--;
                nyear--;
                textView.setText(String.valueOf(nyear)+" 年 ");
                for(int i=0;i<excategorysum.length;i++){
                    excategorysum[i]=0;
                }
                for(int i=0;i<incategorysum.length;i++){
                    incategorysum[i]=0;
                }
                for(int i=0;i<historydataArrayList.size();i++){
                    String nowday[]=historydataArrayList.get(i).date.split("\\/");
                    if(nowday[0].equals(String.valueOf(nyear))){
                        historydatabuffer.add(historydataArrayList.get(i));
                    }
                    if(nowday[0].equals(String.valueOf(nyear))){
                        if(historydataArrayList.get(i).type.equals("expend")){
                            System.out.println(historydataArrayList.get(i).date+"------"+historydataArrayList.get(i).maincategory);
                            for (int j=0;j<excategory.size();j++){
                                if(historydataArrayList.get(i).maincategory.equals(excategory.get(j))){
                                    excategorysum[j]=excategorysum[j]+Integer.valueOf(historydataArrayList.get(i).money);
                                }
                            }
                        }
                        else if(historydataArrayList.get(i).type.equals("income")){
                            for (int j=0;j<incategory.size();j++){
                                if(historydataArrayList.get(i).maincategory.equals(incategory.get(j))){
                                    incategorysum[j]=incategorysum[j]+Integer.valueOf(historydataArrayList.get(i).money);
                                }
                            }
                        }
                    }
                }
                show();
            }
        });
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(changebuffer==true){
                    changebuffer=false;
                    change.setText("切換支出");
                    show();
                }else {
                    changebuffer=true;
                    change.setText("切換收入");
                    show();
                }

            }
        });

    }
    public void show() {
        pieChart = (PieChart) findViewById(R.id.act_pie_pieChart) ;
        //如果啟用此選項,則圖表中的值將以百分比形式繪製,而不是以原始值繪製
        pieChart.setUsePercentValues(true);
        //如果這個元件應該啟用(應該被繪製)FALSE如果沒有。如果禁用,此元件的任何內容將被繪製預設
        pieChart.getDescription().setEnabled(false);
        pieChart.getDescription().setTextSize(20);
        //將額外的偏移量(在圖表檢視周圍)附加到自動計算的偏移量
        pieChart.setExtraOffsets(5, 10, 5, 5);
        //較高的值表明速度會緩慢下降 例如如果它設定為0,它會立即停止。1是一個無效的值,並將自動轉換為0.999f。
        pieChart.setDragDecelerationFrictionCoef(0.95f);
//        //設定中間字型

        //設定為true將餅中心清空
        pieChart.setDrawHoleEnabled(false);
        //套孔,繪製在PieChart中心的顏色
//        pieChart.setHoleColor(Color.WHITE);
//        //設定透明圓應有的顏色。
//        pieChart.setTransparentCircleColor(Color.WHITE);
//        //設定透明度圓的透明度應該有0 =完全透明,255 =完全不透明,預設值為100。
//        pieChart.setTransparentCircleAlpha(110);
        //設定在最大半徑的百分比餅圖中心孔半徑(最大=整個圖的半徑),預設為50%
//        pieChart.setHoleRadius(58f);
//        //設定繪製在孔旁邊的透明圓的半徑,在最大半徑的百分比在餅圖*(max =整個圖的半徑),預設55% -> 5%大於中心孔預設
//        pieChart.setTransparentCircleRadius(61f);
        //將此設定為true,以繪製顯示在pie chart
        pieChart.setDrawCenterText(true);
        //集度的radarchart旋轉偏移。預設270f -->頂(北)
        pieChart.setRotationAngle(0);
        //設定為true,使旋轉/旋轉的圖表觸控。設定為false禁用它。預設值:true
        pieChart.setRotationEnabled(true);
        //將此設定為false,以防止由抽頭姿態突出值。值仍然可以通過拖動或程式設計高亮顯示。預設值:真
        pieChart.setHighlightPerTapEnabled(true);
        //建立Legend物件
        Legend l = pieChart.getLegend();
        //設定垂直對齊of the Legend
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        //設定水平of the Legend
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        //設定方向
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        //其中哪一個將畫在圖表或外
        l.setDrawInside(false);
        //設定水平軸上圖例項之間的間距
        l.setXEntrySpace(7f);
        //設定在垂直軸上的圖例項之間的間距
        l.setYEntrySpace(0f);
        //設定此軸上標籤的所使用的y軸偏移量 更高的偏移意味著作為一個整體的Legend將被放置遠離頂部。
        l.setYOffset(0f);


        //設定入口標籤的顏色。
        pieChart.setEntryLabelColor(Color.BLACK);
        //設定入口標籤的大小。預設值:13dp
        pieChart.setEntryLabelTextSize(20f);
        //模擬的資料來源

        if(changebuffer==true){
            categorypiechart.clear();
            pieChart.setCenterText("");
            for(int i=0;i<excategory.size();i++){
                if(excategorysum[i]!=0){
                    PieEntry buffer = new PieEntry( excategorysum[i], excategory.get(i) ) ;
                    categorypiechart.add(buffer);
                    //System.out.println(categorypiechart.get(i));
                }

            }
            if(categorypiechart.size()==0){
                pieChart.setCenterText("尚無紀錄資料");
                pieChart.setCenterTextSize(50f);
            }
        }else {
            categorypiechart.clear();
            pieChart.setCenterText("");
            for(int i=0;i<incategory.size();i++){
                if(incategorysum[i]!=0){
                    PieEntry buffer = new PieEntry( incategorysum[i], incategory.get(i) ) ;
                    categorypiechart.add(buffer);
                }

            }
            if(categorypiechart.size()==0){
                pieChart.setCenterText("尚無紀錄資料");
                pieChart.setCenterTextSize(50f);
            }
        }




        PieDataSet set = new PieDataSet(categorypiechart , "類別") ;
        set.setDrawValues(true);
        set.setValueTextColor(Color.BLUE);  //设置所有DataSet内数据实体（百分比）的文本颜色
        set.setValueTextSize(20f);          //设置所有DataSet内数据实体（百分比）的文本字体大小
        set.setValueFormatter(new PercentFormatter());

        /**
         * 設定該資料集前應使用的顏色。顏色使用只要資料集所代表的條目數目高於顏色陣列的大小。
         * 如果您使用的顏色從資源, 確保顏色已準備好(通過呼叫getresources()。getColor(…))之前,將它們新增到資料集
         * */
        ArrayList<Integer> colors = new ArrayList<Integer>();
        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);
        colors.add(ColorTemplate.getHoloBlue());
        set.setColors(colors);
        //傳入PieData
        PieData data = new PieData(set);
        //為圖表設定新的資料物件
        pieChart.setData(data);
        //重新整理
        pieChart.invalidate();
        //動畫圖上指定的動畫時間軸的繪製
        pieChart.animateY(1400);

    }

}
