package com.example.a1006;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link income#newInstance} factory method to
 * create an instance of this fragment.
 */
public class income extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private final String[] lunch = {"工作收入", "其他收入"};

    private List<String> all;
    public income() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment tab2.
     */
    // TODO: Rename and change types and number of parameters
    public static income newInstance(String param1, String param2) {
        income fragment = new income();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    TextView tdate,ttime;
    EditText money,detail;
    String moneybuffer,detailbuffer,Maincategory;
    Spinner spinner;
    boolean end=false;
    Button enter,add,delete,mode;
    ArrayList<String> category=new ArrayList<String>();
    DatePickerDialog.OnDateSetListener dateSetListener;
    TimePickerDialog.OnTimeSetListener timeSetListener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.income, container, false);
        View view1=inflater.inflate(R.layout.addview,container,false);
        ttime=view.findViewById(R.id.time);
        spinner = view.findViewById(R.id.spinner3);
        tdate=view.findViewById(R.id.date);
        add=view.findViewById(R.id.button7);
        delete=view.findViewById(R.id.button8);
        Calendar cal=Calendar.getInstance();
        int year=cal.get(Calendar.YEAR);
        int month=cal.get(Calendar.MONTH);
        int day=cal.get(Calendar.DATE);
        int hour=cal.get(Calendar.HOUR_OF_DAY);
        int min=cal.get(Calendar.MINUTE);
        enter=view.findViewById(R.id.button5);
        money=view.findViewById(R.id.money);
        detail=view.findViewById(R.id.detail);
        mode=view.findViewById(R.id.button12);
        Global global=(Global)getActivity().getApplicationContext();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Mysqlqrcode con = new Mysqlqrcode();
                category= con.getIncomecategory(global.accountname);
                System.out.println(category.size());
                end=true;
            }
        }).start();
        while (end!=true){
            ArrayAdapter<String> lunchList = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, category);
            lunchList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(lunchList);
        }
        mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("新增常用");
                alertDialog.setView(view1);
                Maincategory=(String) spinner.getSelectedItem();
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newstring = editText.getText().toString();
                        moneybuffer=money.getText().toString();
                        System.out.println(newstring+Maincategory+moneybuffer+global.accountname);
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Mysqlqrcode con = new Mysqlqrcode();
                                int q=con.insertmodleData(newstring,Maincategory,moneybuffer,global.accountname,"收入");
                                if(q==0){
                                    Looper.prepare();
                                    Toast toast=Toast.makeText(getActivity(), "輸入成功", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER,0,0);
                                    toast.show();
                                    Looper.loop();
                                    money.setText(null);detail.setText(null);
                                    editText.setText("");
                                }else {
                                    Looper.prepare();
                                    Toast toast=Toast.makeText(getActivity(), "輸入失敗", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER,0,0);
                                    toast.show();
                                    Looper.loop();
                                }
                            }
                        }).start();
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("新增分類");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newstring = editText.getText().toString();
                        for(int i=0;i<category.size();i++){
                            System.out.println(category.get(i));
                            if(newstring.equals(category.get(i))){
                                Toast.makeText(getActivity(),"此類別已存在",Toast.LENGTH_LONG).show();
                                editText.setText("");
                                return;
                            }
                        }
                        if(!newstring.equals("")){
                            category.add((newstring));
                            spinner.setSelection(category.size()-1);
                            Toast.makeText(getActivity(),"新增成功",Toast.LENGTH_LONG).show();
                            editText.setText("");
                        }
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view1 != null) {
                    ViewGroup parentViewGroup = (ViewGroup) view1.getParent();
                    if (parentViewGroup != null ) {
                        parentViewGroup.removeView(view1);
                    }
                }
                EditText editText=view1.findViewById(R.id.editText1);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("刪除分類");
                alertDialog.setView(view1);
                alertDialog.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        boolean check=true;
                        String newstring = editText.getText().toString();
                        for(int i=0;i< category.size();i++){
                            if(newstring.equals(category.get(i))){
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Mysqlqrcode con = new Mysqlqrcode();
                                        con.deletecategory(newstring,global.accountname);
                                    }
                                }).start();
                                check=false;
                                category.remove((i));
                                if(i==0){
                                    spinner.setSelection(1);
                                }else {
                                    spinner.setSelection(0);
                                }
                                Toast.makeText(getActivity(),"刪除成功",Toast.LENGTH_LONG).show();
                                editText.setText("");
                            }
                        }
                        if(check==true){
                            Toast.makeText(getActivity(),"查無此類別名稱",Toast.LENGTH_LONG).show();
                            editText.setText("");
                        }
                    }
                });
                alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //設定現在日期 String.format("%02d", day)
        String date = year + "/" + (month+1) + "/" + day;
        tdate.setText(date);
        //設定現在時間 (((hour+8)%24))
        String time = (hour) + ":" + String.format("%02d", min);
        ttime.setText(time);

        //日期選擇
        tdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view1.getParent() != null) {
                    ((ViewGroup)view1.getParent()).removeView(view1); // <- fix
                }
                Calendar cal=Calendar.getInstance();
                int year=cal.get(Calendar.YEAR);
                int month=cal.get(Calendar.MONTH);
                int day=cal.get(Calendar.DATE);


                dateSetListener=new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        String date = year + "/" + (month+1) + "/" + day;
                        tdate.setText(date);
                    }
                };

                DatePickerDialog dialog=new DatePickerDialog(getActivity(), dateSetListener, year,month,day);
                dialog.show();


            }
        });
        //時間選擇
        ttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal=Calendar.getInstance();
                int hour=cal.get(Calendar.HOUR_OF_DAY);
                int min=cal.get(Calendar.MINUTE);

                timeSetListener=new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int min) {
                        Log.d("onTimeSet: hh/mm: ", +hour + "/" + min);
                        String time = hour + ":" + String.format("%02d", min);
                        ttime.setText(time);
                    }
                };

                TimePickerDialog dialog=new TimePickerDialog(
                        getActivity(),
                        timeSetListener,
                        hour,min, DateFormat.is24HourFormat(getActivity())
                );
                dialog.show();

            }

        });
        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String datebuffer=tdate.getText().toString();
                String timebuffer=ttime.getText().toString();
                moneybuffer=money.getText().toString();
                detailbuffer=detail.getText().toString();
                Maincategory=(String) spinner.getSelectedItem();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Mysqlqrcode con = new Mysqlqrcode();
                        int q=con.insertIncomeData(moneybuffer,Maincategory,detailbuffer,datebuffer,timebuffer,global.accountname);
                        if(q==0){
                            Looper.prepare();
                            Toast toast=Toast.makeText(getActivity(), "輸入成功", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER,0,0);
                            toast.show();
                            Looper.loop();
                        }else {
                            Looper.prepare();
                            Toast toast=Toast.makeText(getActivity(), "輸入失敗", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER,0,0);
                            toast.show();
                            Looper.loop();
                        }
                    }
                }).start();
                tdate.setText(date);
                ttime.setText(time);
                money.setText("");

            }
        });
        // Inflate the layout for this fragment
        return view;
    }
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode==KeyEvent.KEYCODE_BACK){
            Intent intent = new Intent(getActivity(), account.class);
            startActivity(intent);
        }
        return false;
    }
}