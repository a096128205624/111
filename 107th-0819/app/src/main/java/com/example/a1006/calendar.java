package com.example.a1006;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class calendar extends AppCompatActivity {
    ArrayList<historydata> historydataArrayList=new ArrayList<historydata>();

    ListView listViewhistroy;
    boolean end=false;
    Calendar c = Calendar.getInstance();
    int nyear = c.get(Calendar.YEAR);
    int nmonth = c.get(Calendar.MONTH)+1;
    int nday = c.get(Calendar.DAY_OF_MONTH);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendarlayout);
        List<EventDay> events = new ArrayList<>();
        Global global=(Global)getApplicationContext();
        String accountname=global.getAccountname();
        Calendar c=Calendar.getInstance();
        ArrayList<historymark> historymarks=new ArrayList<historymark>();
        ArrayList<historydata> historydataArrayList=global.getHistorydata();
        ArrayList<historydata> historydatabuffer=new ArrayList<historydata>();
        //有資料的時間---------------------------------------------------------------
        for (int i=0;i<historydataArrayList.size();i++){
            if (historymarks.size()==0){
                historymarks.add(new historymark(historydataArrayList.get(i).date,0,0));

            }
            else{
                boolean check=true;
                for(int j=0;j<historymarks.size();j++){
                    if(historymarks.get(j).date.equals(historydataArrayList.get(i).date)){
                        check=false;
                    }
                }
                if (check){
                    historymarks.add(new historymark(historydataArrayList.get(i).date,0,0));
                }
            }
        }

        for (int i=0;i<historymarks.size();i++){
            System.out.println(String.valueOf(historymarks.get(i).expend));
            System.out.println(String.valueOf(historymarks.get(i).income));
            for(int j=0;j<historydataArrayList.size();j++){
                if(historymarks.get(i).date.equals(historydataArrayList.get(j).date)){
                    if(historydataArrayList.get(j).type.equals("income")){
                        historymarks.get(i).income=1;
                    }
                    if(historydataArrayList.get(j).type.equals("expend")){
                        historymarks.get(i).expend=1;
                    }
                }
            }
        }

        for(int i=0;i<historymarks.size();i++){
            Calendar calendar=Calendar.getInstance();
            String nowday[]=historymarks.get(i).date.split("\\/");
            System.out.println(nowday[0]+nowday[1]+nowday[2]);
            if(historymarks.get(i).income==1 && historymarks.get(i).expend==1){
                System.out.println("3");
                calendar.set(Integer.valueOf(nowday[0]),Integer.valueOf(nowday[1])-1,Integer.valueOf(nowday[2]));
                events.add(new EventDay(calendar, R.drawable.bothmark));

            }
            if(historymarks.get(i).income==0 && historymarks.get(i).expend==1) {
                System.out.println("1");
                calendar.set(Integer.valueOf(nowday[0]),Integer.valueOf(nowday[1])-1,Integer.valueOf(nowday[2]));
                events.add(new EventDay(calendar, R.drawable.expendmark));

            }
            if(historymarks.get(i).income==1 && historymarks.get(i).expend==0){
                System.out.println("2");
                calendar.set(Integer.valueOf(nowday[0]),Integer.valueOf(nowday[1])-1,Integer.valueOf(nowday[2]));
                events.add(new EventDay(calendar, R.drawable.incomemark));

            }
        }
        //-----------------------------------------------------------------------------------
        listViewhistroy=findViewById(R.id.history);
        for(int i=0;i<historydataArrayList.size();i++){
            String nowday[]=historydataArrayList.get(i).date.split("\\/");
            if(nowday[0].equals(String.valueOf(nyear)) && nowday[1].equals(String.valueOf(nmonth))&& nowday[2].equals(String.valueOf(nday))){
                historydatabuffer.add(historydataArrayList.get(i));
            }
        }
        Collections.sort(historydatabuffer, new historysort());

        BaseAdapter adapter=new BaseAdapter() {
            @Override
            public int getCount() {
                return historydatabuffer.size();
            }
            @Override
            public Object getItem(int i) {
                return i;
            }
            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                View layout=View.inflate(calendar.this,R.layout.expenddatalayout,null);
                TextView subcategory=layout.findViewById(R.id.subcategory);
                TextView money=layout.findViewById(R.id.money);
                subcategory.setText(historydatabuffer.get(i).maincategory);
                if(historydatabuffer.get(i).type.equals("expend")){
                    money.setText(" - "+String.valueOf(historydatabuffer.get(i).money)+" $");
                    money.setTextColor(Color.RED);
                }
                else if(historydatabuffer.get(i).type.equals("income")){
                    money.setText(" + "+String.valueOf(historydatabuffer.get(i).money)+" $");
                    money.setTextColor(Color.GREEN);
                }
                return layout;

            }
        };
        listViewhistroy.setAdapter(adapter);
        CalendarView calendarView = (CalendarView) findViewById(R.id.calendarView);
        calendarView.setEvents(events);
        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                historydatabuffer.clear();
                Calendar clickedDayCalendar = eventDay.getCalendar();
                nyear=clickedDayCalendar.get(Calendar.YEAR);
                nmonth=clickedDayCalendar.get(Calendar.MONTH)+1;
                nday=clickedDayCalendar.get(Calendar.DATE);
                for(int i=0;i<historydataArrayList.size();i++){
                    String nowday[]=historydataArrayList.get(i).date.split("\\/");
                    if(nowday[0].equals(String.valueOf(nyear)) && nowday[1].equals(String.valueOf(nmonth))&& nowday[2].equals(String.valueOf(nday))){
                        historydatabuffer.add(historydataArrayList.get(i));
                    }
                }
                Collections.sort(historydatabuffer, new historysort());
                BaseAdapter adapter=new BaseAdapter() {
                    @Override
                    public int getCount() {
                        return historydatabuffer.size();
                    }
                    @Override
                    public Object getItem(int i) {
                        return i;
                    }@Override

                    public long getItemId(int i) {
                        return i;
                    }
                    @Override
                    public View getView(int i, View view, ViewGroup viewGroup) {
                        View layout=View.inflate(calendar.this,R.layout.expenddatalayout,null);
                        TextView subcategory=layout.findViewById(R.id.subcategory);
                        TextView money=layout.findViewById(R.id.money);
                        subcategory.setText(historydatabuffer.get(i).maincategory);
                        if(historydatabuffer.get(i).type.equals("expend")){
                            money.setText(" - "+String.valueOf(historydatabuffer.get(i).money)+" $");
                            money.setTextColor(Color.RED);
                        }
                        else if(historydatabuffer.get(i).type.equals("income")){
                            money.setText(" + "+String.valueOf(historydatabuffer.get(i).money)+" $");
                            money.setTextColor(Color.BLUE);
                        }
                        return layout;

                    }
                };
                listViewhistroy.setAdapter(adapter);
            }
        });


    }
}