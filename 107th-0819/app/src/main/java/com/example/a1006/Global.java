package com.example.a1006;

import android.app.Application;

import java.util.ArrayList;

public class Global extends Application {
    String code,Uniformnumbers,date,accountname,detail,positionEX;
    int money;
    ArrayList<historydata> historydata;
    ArrayList<modeexdata> modeexdata;

    public void setHistorydata(ArrayList<historydata> historydata) {
        this.historydata = historydata;
    }
    public void setModeHistorydata(ArrayList<modeexdata> modeexdata) {
        this.modeexdata = modeexdata;
    }

    public ArrayList<historydata> getHistorydata() {
        return historydata;
    }
    public ArrayList<modeexdata> getModeHistorydata() {
        return modeexdata;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setPositionEX(String positionEX) {
        this.positionEX = positionEX;
    }

    public String getPositionEX() {
        return positionEX;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDetail() {
        return detail;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setUniformnumbers(String Uniformnumbers) {
        this.Uniformnumbers = Uniformnumbers;
    }

    public String getUniformnumbers() {
        return Uniformnumbers;
    }

    public void setAccountname(String accountname) {
        this.accountname = accountname;
    }

    public String getAccountname() {
        return accountname;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getMoney() {
        return money;
    }


}
