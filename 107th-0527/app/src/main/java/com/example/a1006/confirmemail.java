package com.example.a1006;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class confirmemail extends AppCompatActivity {
    Button bsubmit,bsend;
    EditText eemail,eaccount;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirmmail);
        Bundle bundle = getIntent().getExtras();
        String account = bundle.getString("account");
        String email = bundle.getString("email");
        bsubmit=findViewById(R.id.bsubmit);
        bsend=findViewById(R.id.bsned);
        eemail=findViewById(R.id.email);
        eaccount=findViewById(R.id.account);
        eaccount.setText(account);
        eemail.setText(email);
        bsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            URL url = new URL("http://120.105.161.106/107th/add.php");

                            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                            connection.setRequestMethod("POST");
                            connection.setDoOutput(true);
                            connection.setDoInput(true);
                            connection.setUseCaches(false);
                            connection.connect();
                            OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
                            String data="";
                            data += "&" + URLEncoder.encode("account","UTF-8") + "=" + URLEncoder.encode(account, "UTF-8")
                                    + "&" + URLEncoder.encode("email","UTF-8") + "=" + URLEncoder.encode(email, "UTF-8");
                            wr.write(data);
                            wr.flush();
                            int responseCode = connection.getResponseCode();
                            Log.e("post response code", connection.getResponseCode() + " ");
                        }catch (Exception e){
                            Log.e("connect",e.getMessage());
                        }
                    }
                }).start();
                Toast.makeText(confirmemail.this, "已傳送信件，請至信箱查收", Toast.LENGTH_SHORT).show();




            }
        });
        bsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(confirmemail.this, login.class);
                startActivity(intent);
            }
        });

    }

}
