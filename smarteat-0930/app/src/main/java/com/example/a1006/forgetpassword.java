package com.example.a1006;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class forgetpassword extends AppCompatActivity {
    TextView bsend;
    EditText eemail,eaccount;
    ArrayList<forgetpasswordvalue> allaccount=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgetpassword);
        bsend=findViewById(R.id.bsend);
        eemail=findViewById(R.id.newpasswrod);
        eaccount=findViewById(R.id.oldpassword);


        bsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        boolean check=false;
                        String account = eaccount.getText().toString();
                        String email = eemail.getText().toString();
                        Mysqlqrcode con = new Mysqlqrcode();
                        allaccount=con.getAllAccount();
                        System.out.println(email+"/"+account);
                        System.out.println(allaccount.size());
                        for(int i=0;i<allaccount.size();i++){
                            System.out.println(allaccount.get(i).mail+"/"+allaccount.get(i).account);
                            if( allaccount.get(i).mail.equals(email) && allaccount.get(i).account.equals(account)){
                                check=true;
                                try {
                                    URL url = new URL("http://120.105.161.106/107th/getpassword.php");

                                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                                    connection.setRequestMethod("POST");
                                    connection.setDoOutput(true);
                                    connection.setDoInput(true);
                                    connection.setUseCaches(false);
                                    connection.connect();
                                    OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
                                    String data="";
                                    data += "&" + URLEncoder.encode("account","UTF-8") + "=" + URLEncoder.encode(account, "UTF-8")
                                            + "&" + URLEncoder.encode("email","UTF-8") + "=" + URLEncoder.encode(email, "UTF-8");
                                    wr.write(data);
                                    wr.flush();
                                    int responseCode = connection.getResponseCode();
                                    Log.e("post response code", connection.getResponseCode() + " ");
                                    Looper.prepare();
                                    Toast.makeText(forgetpassword.this, "已傳送信件，請至信箱查收", Toast.LENGTH_SHORT).show();
                                    Looper.loop();
                                }catch (Exception e){
                                    Log.e("connect",e.getMessage());
                                }
                            }
                        }
                        if(check==false){
                            Looper.prepare();
                            Toast.makeText(forgetpassword.this, "帳號或信箱錯誤", Toast.LENGTH_SHORT).show();
                            Looper.loop();
                        }
                    }
                }).start();


            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
}