package com.example.a1006;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class login extends AppCompatActivity {
    EditText Eaccount,Epassword;
    TextView login,sing, confrim, forgetpassword;
    String account,password;
    CheckBox remember_key,automatic_login;//記住密碼勾選框
    public Boolean rem_isCheck = false;
    Boolean auto_isCheck = false;
    boolean check=false;
    Global global;
    AccountValue av;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loging);
        sing=findViewById(R.id.sing);
        login=findViewById(R.id.login);
        Eaccount=findViewById(R.id.oldpassword);
        Epassword=findViewById(R.id.Password);
        confrim=findViewById(R.id.confirm);
        forgetpassword=findViewById(R.id.forgetpassword);
        remember_key =findViewById(R.id.checkBox);
        automatic_login =findViewById(R.id.checkBox2);
        global=(Global)getApplicationContext();
        global.sp = this.getSharedPreferences("userInfo", Context.MODE_PRIVATE);

        remember_key.setChecked(true);//設定記住密碼初始化為true

        if (global.sp.getBoolean("rem_isCheck", true)) {
            //設定預設是記錄密碼狀態
            remember_key.setChecked(true);
            Eaccount.setText(global.sp.getString("USER_NAME", ""));
            Epassword.setText(global.sp.getString("PASSWORD", ""));
            Log.e("自動恢復儲存的賬號密碼", "自動恢復儲存的賬號密碼");

            //判斷自動登陸多選框狀態
            if (global.sp.getBoolean("auto_isCheck", true) && !global.sp.getString("USER_NAME", "").equals("")) {
                //設定預設是自動登入狀態

                global.setAccountname(global.sp.getString("USER_NAME", ""));
                global.setPass(global.sp.getString("PASSWORD", ""));
                automatic_login.setChecked(true);
                //跳轉介面
                Intent intent = new Intent(login.this, MainActivity.class);
                login.this.startActivity(intent);
                Toast toast1 = Toast.makeText(getApplicationContext(), "已自動登入", Toast.LENGTH_SHORT);
                Log.e("自動登陸", "自動登陸");
            }
        }



        forgetpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(login.this, forgetpassword.class);
                startActivity(intent);
                overridePendingTransition(R.animator.slide_in_right, R.animator.slide_out_left);
            }
        });

        confrim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(login.this, confirmemail.class);
                Bundle bundle = new Bundle();
                bundle.putString("email","");
                bundle.putString("account","");
                intent.putExtras(bundle);
                startActivity(intent);
                overridePendingTransition(R.animator.slide_in_right, R.animator.slide_out_left);
            }
        });
        sing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(com.example.a1006.login.this, com.example.a1006.sing.class);
                startActivity(intent);
                overridePendingTransition(R.animator.slide_in_right, R.animator.slide_out_left);
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                account=Eaccount.getText().toString();
                password=Epassword.getText().toString();
                Mysqlqrcode mysqlqrcode=new Mysqlqrcode();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        av=mysqlqrcode.getAccount(account);
                        System.out.println(av.account+av.password);

                    }
                }).start();
                while (av==null){//等待搜尋帳號
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if(av.account.equals(account) && av.password.equals(password) && av.state.equals("OK")){
                    rem_isCheck = remember_key.isChecked();
                    auto_isCheck = automatic_login.isChecked();
                    System.out.println(rem_isCheck+" "+auto_isCheck);
                    if (rem_isCheck) {
                        //記住使用者名稱、密碼、
                        global.editor = global.sp.edit();
                        global.editor.putString("USER_NAME", account);
                        global.editor.putString("PASSWORD", password);
                        global.editor.putBoolean("rem_isCheck", rem_isCheck);
                        global.editor.putBoolean("auto_isCheck", auto_isCheck);
                        Log.e("選中儲存密碼", "賬號：" + account +
                                "\n" + "密碼：" + password +
                                "\n" + "是否記住密碼：" + rem_isCheck +
                                "\n" + "是否自動登陸：" + auto_isCheck);
                        global.editor.commit();
                    }
                    global.setAccountname(account);
                    global.setPass(password);
                    Intent intent = new Intent(com.example.a1006.login.this, MainActivity.class);
                    startActivity(intent);
                }
                else if(!av.account.equals(account)){
                    Toast toast = Toast.makeText(com.example.a1006.login.this, "無此帳號", Toast.LENGTH_LONG);
                    toast.show();
                }
                else if(!av.password.equals(password)){
                    Toast toast = Toast.makeText(com.example.a1006.login.this, "密碼錯誤", Toast.LENGTH_LONG);
                    toast.show();
                }
                else if(!av.state.equals("OK")){
                    Toast toast = Toast.makeText(com.example.a1006.login.this, "帳號未開通", Toast.LENGTH_LONG);
                    toast.show();
                }
                overridePendingTransition(R.animator.slide_in_right, R.animator.slide_out_left);
                finish();
            }
        });
    }
}
