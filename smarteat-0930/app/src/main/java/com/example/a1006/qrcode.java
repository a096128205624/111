package com.example.a1006;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.zxing.Result;

import java.io.IOException;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.os.SystemClock.sleep;

public class qrcode extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    ZXingScannerView zXingScannerView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qrcode);

        //找到介面
        zXingScannerView = findViewById(R.id.ZXingScannerView_QRCode);
        //取得相機權限
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                ActivityCompat.checkSelfPermission(this
                        , Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    100);
        }else{
            //若先前已取得權限，則直接開啟
            openQRCamera();

        }
    }
    /**開啟QRCode相機*/
    private void openQRCamera(){
        zXingScannerView.setResultHandler(this);
        zXingScannerView.startCamera();
    }
    /**取得權限回傳*/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 100 && grantResults[0] ==0){
            openQRCamera();
        }else{
            Toast.makeText(this, "權限勒？", Toast.LENGTH_SHORT).show();
        }
    }
    /**關閉QRCode相機*/
    @Override
    protected void onStop() {
        zXingScannerView.stopCamera();
        super.onStop();
    }


    @Override
    public void handleResult(Result rawResult) {

        Global global=(Global)getApplicationContext();
        String strings,result,strings1 ="",code,editer,date,randomcode,strings2=null,date1,strings3="",Uniformnumbers;
        strings=rawResult.getText();
        strings.split("");
        result=strings.substring(0,1);
        boolean result1 = result.matches("[a-zA-Z]+");
        if(result1){
            strings1=strings;
            code=strings1.substring(0,10);
            date=strings1.substring(10,17);
            date1=date.substring(0,3)+"/"+date.substring(3,5)+"/"+date.substring(5,7);
            randomcode=strings1.substring(17,21);
            Uniformnumbers=strings1.substring(45,53);
            String[] strs=strings1.split(":");
            strings2="發票號碼:"+code+"\n日期:"+date1+"\n隨機碼"+randomcode+"\n統編"+Uniformnumbers;
            int sum=0;
            for(int i=0;i<strs.length;i++){
                strs[i]=strs[i].replace(" ","");
            }
            if(code.equals("44953265")){
                for(int i=5;i<strs.length-1;i=i+3){
                    strs[i]=unicode2big5(strs[i]);
                    strings3+="\n品名:"+strs[i]+"\t"+strs[i+1]+"個\t"+strs[i+2]+"元";
                    strings2+="\n品名:"+strs[i]+"\t"+strs[i+1]+"個\t"+strs[i+2]+"元";
                    if(strs.length-1==i){
                        strs[i]=unicode2big5(strs[i]);
                        strings3+="\n品名:"+strs[i]+"\t"+strs[i+1]+"個\t"+strs[i+2]+"元";
                        strings2+="\n品名:"+strs[i]+"\t"+strs[i+1]+"個\t"+strs[i+2]+"元";
                    }
                }
            }else{
                for(int i=5;i<strs.length-1;i=i+3){
                    if(strs[i-2].equals("1")&&i==5){
                        sum+=Integer.parseInt(strs[i+2]);
                        System.out.println("----*"+strs[i+2]+" "+strs[i]+"  "+strs[i+1]);
                        strings3+="\n品名:"+strs[i]+"\t"+strs[i+1]+"個\t單價"+String.valueOf(Integer.parseInt(strs[i+2])/Integer.parseInt(strs[i+1]))+"元";
                        if(strs.length-1==i){
                            strings3+="\n品名:"+strs[i]+"\t"+strs[i+1]+"個\t單價"+String.valueOf(Integer.parseInt(strs[i+2])/Integer.parseInt(strs[i+1]))+"元";
                        }
                    }else {
                        sum+=Integer.parseInt(strs[i-2])*Integer.parseInt(strs[i+2]);
                        System.out.println("----*"+strs[i-2]+"  "+strs[i+2]);
                        strings3+="\n品名:"+strs[i]+"\t"+strs[i-2]+"個\t單價"+strs[i+2]+"元";
                        if(strs.length-1==i){
                            strings3+="\n品名:"+strs[i]+"\t"+strs[i-2]+"個\t單價"+strs[i+2]+"元";
                        }
                    }
                }
            }

            System.out.println(strings1);
            //System.out.println(strings2);
            //System.out.println(strings3);
           // System.out.println(sum);
            if(strings2!=null){
                global.setCode(code);
                global.setMoney(sum);
                global.setDate(date1);
                global.setUniformnumbers(Uniformnumbers);
                global.setDetail(strings3);
                Intent intent = new Intent(qrcode.this, tab.class);
                intent.putExtra("postion",1);
                final data data=new data();
                data.setdata(code);
                startActivity(intent);
            }
        }else{
            openQRCamera();
        }
    }
    public String unicode2big5(String strUTF8)
    {
        String strReturn="";
        try
        {
            strReturn = new String(strUTF8.getBytes("UTF-8"), "big5");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return strReturn;
    }
}
