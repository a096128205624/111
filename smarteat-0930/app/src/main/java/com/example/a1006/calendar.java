package com.example.a1006;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class calendar extends AppCompatActivity {
    RecyclerView account_RV;
    ArrayList<historydata> historydataArrayList=new ArrayList<historydata>();
    ArrayList<historydata> historydatabuffer=new ArrayList<historydata>();
    TextView timeTextview,mainSubCategory,moneyExTextview;
    ListView listViewhistroy;
    boolean end=false;
    Calendar c = Calendar.getInstance();
    int nyear = c.get(Calendar.YEAR);
    int nmonth = c.get(Calendar.MONTH)+1;
    int nday = c.get(Calendar.DAY_OF_MONTH);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendarlayout);
        account_RV = (RecyclerView) findViewById(R.id.account_RV);
        List<EventDay> events = new ArrayList<>();
        Global global=(Global)getApplicationContext();
        String accountname=global.getAccountname();
        Calendar c=Calendar.getInstance();
        ArrayList<historymark> historymarks=new ArrayList<historymark>();
        ArrayList<historydata> historydataArrayList=global.getHistorydata();

        //有資料的時間---------------------------------------------------------------
        for (int i=0;i<historydataArrayList.size();i++){
            if (historymarks.size()==0){
                historymarks.add(new historymark(historydataArrayList.get(i).date,0,0));

            }
            else{
                boolean check=true;
                for(int j=0;j<historymarks.size();j++){
                    if(historymarks.get(j).date.equals(historydataArrayList.get(i).date)){
                        check=false;
                    }
                }
                if (check){
                    historymarks.add(new historymark(historydataArrayList.get(i).date,0,0));
                }
            }
        }

        for (int i=0;i<historymarks.size();i++){
            System.out.println(String.valueOf(historymarks.get(i).expend));
            System.out.println(String.valueOf(historymarks.get(i).income));
            for(int j=0;j<historydataArrayList.size();j++){
                if(historymarks.get(i).date.equals(historydataArrayList.get(j).date)){
                    if(historydataArrayList.get(j).type.equals("income")){
                        historymarks.get(i).income=1;
                    }
                    if(historydataArrayList.get(j).type.equals("expend")){
                        historymarks.get(i).expend=1;
                    }
                }
            }
        }

        for(int i=0;i<historymarks.size();i++){
            Calendar calendar=Calendar.getInstance();
            String nowday[]=historymarks.get(i).date.split("\\/");
            System.out.println(nowday[0]+nowday[1]+nowday[2]);
            if(historymarks.get(i).income==1 && historymarks.get(i).expend==1){
                System.out.println("3");
                calendar.set(Integer.valueOf(nowday[0]),Integer.valueOf(nowday[1])-1,Integer.valueOf(nowday[2]));
                events.add(new EventDay(calendar, R.drawable.bothmark));

            }
            if(historymarks.get(i).income==0 && historymarks.get(i).expend==1) {
                System.out.println("1");
                calendar.set(Integer.valueOf(nowday[0]),Integer.valueOf(nowday[1])-1,Integer.valueOf(nowday[2]));
                events.add(new EventDay(calendar, R.drawable.incomemark));

            }
            if(historymarks.get(i).income==1 && historymarks.get(i).expend==0){
                System.out.println("2");
                calendar.set(Integer.valueOf(nowday[0]),Integer.valueOf(nowday[1])-1,Integer.valueOf(nowday[2]));

                events.add(new EventDay(calendar, R.drawable.expendmark));

            }
        }
        //-----------------------------------------------------------------------------------
        listViewhistroy=findViewById(R.id.history);

        initRV_data(historydataArrayList,nyear,nmonth,nday);
        Collections.sort(historydatabuffer, new historysort());
        CalendarView calendarView = (CalendarView) findViewById(R.id.calendarView);
        calendarView.setEvents(events);
        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                historydatabuffer.clear();
                Calendar clickedDayCalendar = eventDay.getCalendar();
                nyear = clickedDayCalendar.get(Calendar.YEAR);
                nmonth = clickedDayCalendar.get(Calendar.MONTH) + 1;
                nday = clickedDayCalendar.get(Calendar.DATE);
                initRV_data(historydataArrayList,nyear,nmonth,nday);

            }
        });


    }
    private void initRV_data(ArrayList<historydata> historydataArrayList,int nyear,int nmonth,int nday){

        for(int i=0;i<historydataArrayList.size();i++){
            String nowday[]=historydataArrayList.get(i).date.split("\\/");
            if(nowday[0].equals(String.valueOf(nyear)) && nowday[1].equals(String.valueOf(nmonth))&& nowday[2].equals(String.valueOf(nday))){
                historydatabuffer.add(historydataArrayList.get(i));
            }
        }
        Collections.sort(historydatabuffer, new historysort());
        calender_recycleview adapter = new calender_recycleview(this, historydatabuffer);
        account_RV.setLayoutManager(new LinearLayoutManager(this));
        account_RV.setAdapter(adapter);
    }
}
